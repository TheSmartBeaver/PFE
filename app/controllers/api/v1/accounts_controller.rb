class Api::V1::AccountsController < Devise::SessionsController
  respond_to :html, :json

  #   POST /api/v1/login
  def create
    super
    session[:account_id] = current_account.id
  end

  def destroy
    super
    session.delete(:account_id)
  end

  private
    def account_params
      params.require(:account).permit(:email, :password)
    end

    def authorized?
      account.account == current_account
    end
end
