class Api::V1::ApiController < ApplicationController
    before_action :authenticate_account!
    # To exempt a controller or an action from needing to be authenticated, add in controller:
    #       skip_before_action :authenticate_user!                  (for whole controller)
    #       skip_before_action :authenticate_user!, only [:index]   (for index action of controller)




    protected
        def can_reply_survey
            begin
                @replier = Replier.find(session["replier_id"])
                @survey = Survey.find(session["reply_survey_id"])
            rescue Exception => e
                render json: { error: 'You can\'t reply on this survey.'}, status: 400
                return
            end
            unless helpers.can_reply_survey(@survey, @replier)
                render json: { error: 'You can\'t reply on this survey.'}, status: 400
                return
            end
        end
        def can_edit_survey
            begin
                @account = Account.find(session["account_id"])
                @survey = Survey.find(session["edit_survey_id"])
            rescue Exception => e
                render json: { error: 'You can\'t edit this survey.*', exeption: e, id: session["edit_survey_id"]}, status: 400
                return
            end
            unless helpers.can_edit_survey(@survey, @account.id)
                render json: { error: 'You can\'t edit this survey.**'}, status: 400
                return
            end
        end
end
