class Api::V1::Condition::BasicConditionController < Api::V1::ApiController
    
    before_action :can_edit_survey

    #   GET /basicCondition
    # def index   
    #     basicConditions = BasicCondition.all
    #     render json: basicConditions
    # end

    #   GET /basicCondition
    def show    
        basicCondition = BasicCondition.find(params[:id])
        render json: basicCondition.as_json(show_params)
    end

    #   POST /basicCondition
    def create
        basicCondition = BasicCondition.new(basicCondition_params)
        condition = Condition.new(:conditionable => basicCondition)
        if basicCondition.save && condition.save
            render json: basicCondition.to_json(include: [:element])
        else 
            #print basicCondition.errors.full_messages
            render json: { error: 'Unable to create BasicCondition'}, status: 400
        end
    end

    #   PUT /basicCondition/:id
    def update
        basicCondition = BasicCondition.find(params[:id])
        if basicCondition
            basicCondition.update(basicCondition_params)
            render json: { message: 'BasicCondition successfully update.'}, status: 200
        else
            render json: { error: 'Unable to update BasicCondition.'}, status: 400
        end 
    end

    #   DELETE /basicCondition/:id
    def destroy
        basicCondition = BasicCondition.find(params[:id])
        if basicCondition
            basicCondition.target1.destroy
            basicCondition.target2.destroy
            basicCondition.destroy
            render json: { message: 'BasicCondition successfully deleted.'}, status: 200
        else 
            render json: { error: 'Unable to delete BasicCondition.'}, status: 400
        end
    end
    
    #   PUT /basicCondition/joinComplexCondition/:id
    def joinComplexCondition
        basicCondition = BasicCondition.find(params[:id])
        complexCondition = ComplexCondition.find(link_to_complex_cond_params[:complex_condition_id])
        if basicCondition && complexCondition
            begin
                helpers.joinComplexCondition(basicCondition.condition, complexCondition)
            rescue Exception => e
                render json: { error: 'Unable to join ComplexCondition.', exeption: e}, status: 400
                return
            end
            render json: { message: 'BasicCondition successfully joined ComplexCondition.'}, status: 200
        else
            render json: { error: 'Unable to join ComplexCondition.'}, status: 400
        end 
    end 

    #   PUT /basicCondition/leaveComplexCondition/:id
    def leaveComplexCondition
        basicCondition = BasicCondition.find(params[:id])
        if basicCondition
            begin
                helpers.leaveComplexCondition(basicCondition.condition)
            rescue Exception => e
                render json: { error: 'Unable to leave ComplexCondition.', exeption: e}, status: 400
                return
            end
            render json: { message: 'BasicCondition successfully left ComplexCondition.'}, status: 200
        else
            render json: { error: 'Unable to leave ComplexCondition.'}, status: 400
        end 
    end

    #   PUT /basicCondition/setAsRoot/:id
    def setAsRoot
        basicCondition = BasicCondition.find(params[:id])
        element = Element.find(root_to_element[:id])
        if basicCondition && element
            begin
                helpers.setAsRoot(basicCondition.condition, element)
            rescue Exception => e
                render json: { error: 'Unable to set up BasicCondition as root.'}, status: 400
                return
            end
            render json: { message: 'BasicCondition successfully set up as root.'}, status: 200
        else
            render json: { error: 'Unable to set up BasicCondition as root.'}, status: 400
        end 
    end 

    #   PUT /basicCondition/unroot/:id
    def unroot
        basicCondition = BasicCondition.find(params[:id])
        element = Element.find(root_to_element[:id])
        if basicCondition && element
            begin
                helpers.unroot(basicCondition.condition, element)
            rescue Exception => e
                render json: { error: 'Unable to unroot BasicCondition.'}, status: 400
                return
            end
            render json: { message: 'BasicCondition successfully unrooted.'}, status: 200
        else
            render json: { error: 'Unable to unroot BasicCondition.'}, status: 400
        end 
    end

    private
        def basicCondition_params
            params.require(:basic_condition).permit(:target1_id, :target2_id, :comparisonType)
        end
        def link_to_complex_cond_params
            params.require(:complex_parent).permit(:complex_condition_id)
        end
        def root_to_element
            params.require(:element_attributes).permit(:id)
        end
        def show_params
            return {"full" => helpers.to_bool(request.query_parameters["full"]), "expendable" =>  helpers.to_bool(request.query_parameters["expendable"])}
        end
end