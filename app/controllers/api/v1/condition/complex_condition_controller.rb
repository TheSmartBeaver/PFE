class Api::V1::Condition::ComplexConditionController < Api::V1::ApiController

    before_action :can_edit_survey

    #   GET /complexCondition
    # def index   
    #     complexConditions = ComplexCondition.all
    #     render json: complexConditions
    # end

    #   GET /complexCondition
    def show
        complexCondition = ComplexCondition.find(params[:id])
        render json: complexCondition.as_json(show_params)
    end

    #   POST /complexCondition
    def create
        complexCondition = ComplexCondition.new(complexCondition_params)
        condition = Condition.new(:conditionable => complexCondition)
        if complexCondition.save && condition.save
            render json: complexCondition.to_json(include: [:element])
        else 
            #print complexCondition.errors.full_messages
            render json: { error: 'Unable to create ComplexCondition'}, status: 400
        end
    end

    #   PUT /complexCondition/:id
    def update
        complexCondition = ComplexCondition.find(params[:id])
        if complexCondition
            complexCondition.update(complexCondition_params)
            render json: { message: 'ComplexCondition successfully update.'}, status: 200
        else
            render json: { error: 'Unable to update ComplexCondition.'}, status: 400
        end 
    end

    #   DELETE /complexCondition/:id
    def destroy
        complexCondition = ComplexCondition.find(params[:id])
        if complexCondition
            begin
                complexCondition.conditions.collect { |n| helpers.leaveComplexCondition(n.condition)}
                complexCondition.destroy
            rescue Exception => e
                render json: { error: 'Unable to delete ComplexCondition.'}, status: 400
                return
            end
            render json: { message: 'ComplexCondition successfully deleted.'}, status: 200
        else 
            render json: { error: 'Unable to delete ComplexCondition.'}, status: 400
        end
    end

    #   PUT /complexCondition/joinComplexCondition/:id
    def joinComplexCondition
        complexConditionToLink = ComplexCondition.find(params[:id])
        complexCondition = ComplexCondition.find(link_to_complex_cond_params[:complex_condition_id])
        if complexConditionToLink && complexCondition
            begin
                helpers.joinComplexCondition(complexConditionToLink.condition, complexCondition)
            rescue Exception => e
                render json: { error: 'Unable to join ComplexCondition.', exeption: e}, status: 400
                return
            end
            render json: { message: 'ComplexCondition successfully joined ComplexCondition.'}, status: 200
        else
            render json: { error: 'Unable to join ComplexCondition.'}, status: 400
        end 
    end 

    #   PUT /complexCondition/leaveComplexCondition/:id
    def leaveComplexCondition
        complexCondition = ComplexCondition.find(params[:id])
        if complexCondition
            begin
                helpers.leaveComplexCondition(complexCondition.condition)
            rescue Exception => e
                render json: { error: 'Unable to update ComplexCondition.'}, status: 400
                return
            end
            render json: { message: 'ComplexCondition successfully update.'}, status: 200
        else
            render json: { error: 'Unable to update ComplexCondition.'}, status: 400
        end 
    end

    #   PUT /complexCondition/setAsRoot/:id
    def setAsRoot
        complexCondition = ComplexCondition.find(params[:id])
        element = Element.find(root_to_element[:id])
        if complexCondition && element
            begin
                helpers.setAsRoot(complexCondition.condition, element)
            rescue Exception => e
                render json: { error: 'Unable to set up ComplexCondition as root.'}, status: 400
                return
            end
            render json: { message: 'ComplexCondition successfully set up as root.'}, status: 200
        else
            render json: { error: 'Unable to set up ComplexCondition as root.'}, status: 400
        end 
    end 

    #   PUT /complexCondition/unroot/:id
    def unroot
        complexCondition = ComplexCondition.find(params[:id])
        element = Element.find(root_to_element[:id])
        if complexCondition && element
        begin
            helpers.unroot(complexCondition.condition, element)
        rescue Exception => e
            render json: { error: 'Unable to unroot ComplexCondition.'}, status: 400
            return
        end
            render json: { message: 'ComplexCondition successfully unrooted.'}, status: 200
        else
            render json: { error: 'Unable to unroot ComplexCondition.'}, status: 400
        end 
    end

    private
        def complexCondition_params
            params.require(:complex_condition).permit(:complexType)
        end
        def link_to_complex_cond_params
            params.require(:complex_parent).permit(:complex_condition_id)
        end
        def root_to_element
            params.require(:element_attributes).permit(:id)
        end
        def show_params
            return {"full" => helpers.to_bool(request.query_parameters["full"]), "expendable" =>  helpers.to_bool(request.query_parameters["expendable"])}
        end
end
