class Api::V1::Condition::Target::ConstTargetController < Api::V1::ApiController

    before_action :can_edit_survey

    #   GET /constTarget
    # def index   
    #     constTargets = ConstTarget.all
    #     render json: constTargets
    # end

    #   GET /constTarget
    def show    
        constTarget = ConstTarget.find(params[:id])
        render json: constTarget.as_json(show_params)
    end

    #   POST /constTarget
    def create
        #print constTarget_params
        constTarget = ConstTarget.new(constTarget_params)
        target = Target.new(:targetable => constTarget)
            if constTarget.save && target.save
                render json: constTarget.as_json
            else 
                #print constTarget.errors.full_messages
                render json: { error: 'Unable to create ConstTarget'}, status: 400
            end
    end

    #   PUT /constTarget/:id
    def update
        constTarget = ConstTarget.find(params[:id])
        if constTarget
            constTarget.update(constTarget_params)
            render json: { message: 'ConstTarget successfully update.'}, status: 200
        else
            render json: { error: 'Unable to update ConstTarget.'}, status: 400
        end 
    end

    #   DELETE /constTarget/:id
    def destroy
        constTarget = ConstTarget.find(params[:id])
        if constTarget
            constTarget.destroy
            render json: { message: 'ConstTarget successfully deleted.'}, status: 200
        else 
            render json: { error: 'Unable to delete ConstTarget.'}, status: 400
        end
    end

    private
        def constTarget_params
            params.require(:const_target).permit(:const)
        end
        def show_params
            return {"full" => helpers.to_bool(request.query_parameters["full"]), "expendable" =>  helpers.to_bool(request.query_parameters["expendable"])}
        end
end
