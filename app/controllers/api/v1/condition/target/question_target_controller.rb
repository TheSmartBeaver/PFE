class Api::V1::Condition::Target::QuestionTargetController < Api::V1::ApiController

    before_action :can_edit_survey

    #   GET /questionTarget
    # def index   
    #     questionTargets = QuestionTarget.all
    #     render json: questionTargets
    # end

    #   GET /questionTarget
    def show    
        questionTarget = QuestionTarget.find(params[:id])
        render json: questionTarget.as_json(show_params)
    end

    #   POST /questionTarget
    def create
        questionTarget = QuestionTarget.new(questionTarget_params)
        target = Target.new(:targetable => questionTarget)
        if questionTarget.save && target.save
            render json: questionTarget.as_json
        else 
            render json: { error: 'Unable to create QuestionTarget'}, status: 400
        end
    end

    #   PUT /questionTarget/:id
    def update
        questionTarget = QuestionTarget.find(params[:id])
        if questionTarget
            questionTarget.update(questionTarget_params)
            render json: { message: 'QuestionTarget successfully update.'}, status: 200
        else
            render json: { error: 'Unable to update QuestionTarget.'}, status: 400
        end 
    end

    #   DELETE /questionTarget/:id
    def destroy
        questionTarget = QuestionTarget.find(params[:id])
        if questionTarget
            questionTarget.destroy
            render json: { message: 'QuestionTarget successfully deleted.'}, status: 200
        else 
            render json: { error: 'Unable to delete QuestionTarget.'}, status: 400
        end
    end

    private
        def questionTarget_params
            params.require(:question_target).permit(:question_id)
        end
        def show_params
            return {"full" => helpers.to_bool(request.query_parameters["full"]), "expendable" =>  helpers.to_bool(request.query_parameters["expendable"])}
        end
end
