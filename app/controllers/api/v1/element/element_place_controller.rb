class Api::V1::Element::ElementPlaceController < Api::V1::ApiController

    before_action :can_edit_survey

    # OLD
    #   GET /elementPlace
    # def index   
    #     elementPlace = ElementPlace.all
    #     render json: elementPlace, status: 200
    # end

    #   GET /elementPlace/:id
    def show
        begin
            element = Element.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Element to get ElementPlace', exeption: e}, status: 400
            return
        end
        if element.elementPlace.nil?
            render json: { error: 'Unable to get ElementPlace from Element'}, status: 400
            return
        else
            render json: element.elementPlace.as_json(show_params), status: 200
            return
        end
    end

    #   PUT /elementPlace/:id
    def update
        begin
            element = Element.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Element to get ElementPlace', exeption: e}, status: 400
            return
        end
        if element.elementPlace.nil?
            render json: { error: 'Unable to get ElementPlace from Element for update'}, status: 400
            return
        else
            if element.elementPlace.update(elementPlace_params)
                render json: { message: 'ElementPlace successfully update.'}, status: 200
                return
            else
                render json: { error: 'Unable to update ElementPlace', exception: element.elementPlace.errors.to_json}, status: 400
                return
            end
        end 
    end

    private
        def elementPlace_params
            params.require(:element_place).permit(:order, :showWithGroup)
        end
        def show_params
            return {"full" => helpers.to_bool(request.query_parameters["full"]), "expendable" =>  helpers.to_bool(request.query_parameters["expendable"])}
        end
end