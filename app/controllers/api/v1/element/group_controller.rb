class Api::V1::Element::GroupController < Api::V1::ApiController

    before_action :can_edit_survey

    #   GET /elementGroups
    # def index   
    #     elementGroups = ElementGroup.all
    #     render json: elementGroups
    # end

    #   GET /elementGroups
    def show
        begin
            elementGroup = ElementGroup.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get ElementGroup', exeption: e}, status: 400
            return
        end
        render json: elementGroup.as_json(show_params)
        return
    end

    #   POST /elementGroups
    def create
        elementGroup = ElementGroup.new(elementGroup_params)
        place = ElementPlace.new({"showWithGroup" => "false", "order" => 0})
        if place.save
            unless elementGroup.element.update({"elementPlace_id" => place.id})
                render json: { error: 'Unable to setup ElementPlace while creating ElementGroup'}, status: 400
            end
            if elementGroup.save
                render json: elementGroup.as_json, status: 200
                return
            else
                render json: { error: 'Unable to create ElementGroup'}, status: 400
                return
            end
        else
            render json: { error: 'Unable to create ElementPlace while creating ElementGroup'}, status: 400
            return
        end
    end

    #   PUT /elementGroups/:id
    def update
        begin
            elementGroup = ElementGroup.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get ElementGroup', exeption: e}, status: 400
            return
        end
        if elementGroup.update(elementGroup_params)
            render json: { message: 'ElementGroup successfully update.'}, status: 200
            return
        else
            render json: { error: 'Unable to update ElementGroup.', exception: elementGroup.errors.to_json}, status: 400
            return
        end 
    end

    #   DELETE /elementGroups/:id
    def destroy
        begin
            elementGroup = ElementGroup.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get ElementGroup', exeption: e}, status: 400
            return
        end
        unless elementGroup.elements.update_all(:element_group_id => nil)
            render json: { error: 'Unable to destroy ElementGroup.'}, status: 400
            return
        end
        if elementGroup.destroy
            render json: { message: 'ElementGroup successfully deleted.'}, status: 200
            return
        else
            render json: { error: 'Unable to destroy ElementGroup.', exception: elementGroup.errors.to_json}, status: 400
            return
        end
    end

    #   PUT /group/joinGroup/:id
    def joinGroup
        begin
            elementGroup = ElementGroup.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get ElementGroup', exeption: e}, status: 400
            return
        end
        unless elementGroup.isPage
            if elementGroup.element.update(link_to_group_params)
                render json: { message: 'ElementGroup successfully joined group.'}, status: 200
                return
            else
                render json: { error: 'Unable to join group for ElementGroup.', exception: elementGroup.errors.to_json}, status: 400
                return
            end
        else
            render json: { error: 'Unable to join group for ElementGroup.'}, status: 400
            return
        end 
    end 

    #   PUT /group/leaveGroup/:id
    def leaveGroup
        begin
            elementGroup = ElementGroup.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get ElementGroup', exeption: e}, status: 400
            return
        end
        unless elementGroup.isPage
            if elementGroup.element.update(:element_group_id => nil)
                render json: { message: 'ElementGroup successfully left group.'}, status: 200
            else
                render json: { error: 'Unable to leave group for ElementGroup.', exception: elementGroup.errors.to_json}, status: 400
            end
        else
            render json: { error: 'Unable to leave group for ElementGroup.'}, status: 400
        end 
    end

    private
        def elementGroup_params
            params.require(:group).permit(:isPage, :survey_id, element_attributes: [:title])
        end
        def link_to_group_params
            params.require(:element).permit(:element_group_id)
        end
        def show_params
            return {"full" => helpers.to_bool(request.query_parameters["full"]), "expendable" =>  helpers.to_bool(request.query_parameters["expendable"])}
        end
end
