class Api::V1::Element::ImageController < Api::V1::ApiController

    before_action :can_edit_survey

    #   GET /images
    #def index   
    #    images = Image.all
    #    render json: images
    #end

    #   GET /images
    def show
        begin
            image = Image.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Image', exeption: e}, status: 400
            return
        end
        render json: image.as_json(show_params), status: 200
        return
    end

    #   POST /images
    def create
        image = Image.new(image_params)
        place = ElementPlace.new({"showWithGroup" => "false", "order" => 0})
        if place.save
            unless image.element.update({"elementPlace_id" => place.id})
                render json: { error: 'Unable to setup ElementPlace while creating Image'}, status: 400
                return
            end
            if image.save
                render json: image.as_json, status: 200
                return
            else 
                render json: { error: 'Unable to create Image'}, status: 400
                return
            end
        else
            render json: { error: 'Unable to create ElementPlace while creating Image'}, status: 400
            return
        end
    end

    #   PUT /images/:id
    def update
        begin
            image = Image.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Image', exeption: e}, status: 400
            return
        end
        if image.update(image_params)
            render json: { message: 'Image successfully update.'}, status: 200
            return
        else
            render json: { error: 'Unable to update Image.', exception: image.errors.to_json}, status: 400
            return
        end
    end

    #   DELETE /images/:id
    def destroy
        begin
            image = Image.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Image', exeption: e}, status: 400
            return
        end
        if image.destroy
            render json: { message: 'Image successfully deleted.'}, status: 200
        else 
            render json: { error: 'Unable to delete Image.', exception: image.errors.to_json}, status: 400
        end
    end

    #   PUT /image/joinGroup/:id
    def joinGroup
        begin
            image = Image.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Image', exeption: e}, status: 400
            return
        end
        if image.element.update(link_to_group_params)
            render json: { message: 'Image successfully joined group.'}, status: 200
            return
        else
            render json: { error: 'Unable to join group for Image.', exception: image.element.errors.to_json}, status: 400
            return
        end
    end

    #   PUT /image/leaveGroup/:id
    def leaveGroup
        begin
            image = Image.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Image', exeption: e}, status: 400
            return
        end
        if image.element.update(:element_group_id => nil)
            render json: { message: 'Image successfully left group.'}, status: 200
            return
        else
            render json: { error: 'Unable to leave group for Image.', exception: image.element.errors.to_json}, status: 400
            return
        end
    end

    private
        def image_params
            params.require(:image).permit(:url, :alt, element_attributes: [:title])
        end
        def link_to_group_params
            params.require(:element).permit(:element_group_id)
        end
        def show_params
            return {"full" => helpers.to_bool(request.query_parameters["full"]), "expendable" =>  helpers.to_bool(request.query_parameters["expendable"])}
        end
end
