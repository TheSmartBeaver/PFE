class Api::V1::Element::Question::PropertiesController < Api::V1::ApiController

    before_action :can_edit_survey

    #   GET /properties
    def show
        question = Question.find(params[:id])
        if !question.blank?
            properties = question.getQuestionData
            render json: properties, status: 200
        else
            render json: { error: 'Unable to get properties of Question'}, status: 400
        end
    end

    def editTitle
        question = Question.find(params[:id])
        if !question.blank?
            properties = question.getQuestionData
            properties.title = params[:title]
            properties.save_to_base
            render json: properties, status: 200
        else
            render json: { error: 'Unable to get properties of Question'}, status: 400
        end
    end

    def editDescription
        question = Question.find(params[:id])
        if !question.blank?
            properties = question.getQuestionData
            properties.description = params[:description]
            properties.save_to_base
            render json: properties, status: 200
        else
            render json: { error: 'Unable to get properties of Question'}, status: 400
        end
    end

    def editMaxCheck
        question = Question.find(params[:id])
        if !question.blank?
            properties = question.getQuestionData
            begin
                properties.setMaxCheck(params[:maxCheck].to_i)
            rescue Exception => e
                render json: { error: 'Can\'t change maxCheck params Question', exeption: e}, status: 400
                return
            end
            render json: properties, status: 200
        else
            render json: { error: 'Unable to get properties of Question'}, status: 400
        end
    end

    def editMinCheck
        question = Question.find(params[:id])
        if !question.blank?
            properties = question.getQuestionData
            begin
                properties.setMinCheck(params[:minCheck].to_i)
            rescue Exception => e
                render json: { error: 'Can\'t change minCheck params Question', exeption: e}, status: 400
                return
            end
            render json: properties, status: 200
        else
            render json: { error: 'Unable to get properties of Question'}, status: 400
        end
    end

    def addChoice
        question = Question.find(params[:id])
        if !question.blank?
            properties = question.getQuestionData
            begin
                properties.addChoice(params[:choice])
            rescue Exception => e
                render json: { error: 'Can\'t change choices params Question', exeption: e}, status: 400
                return
            end
            render json: properties, status: 200
        else
            render json: { error: 'Unable to get properties of Question'}, status: 400
        end
    end

    def removeChoice
        question = Question.find(params[:id])
        if !question.blank?
            properties = question.getQuestionData
            begin
                properties.removeChoice(params[:choiceId].to_i)
            rescue Exception => e
                render json: { error: 'Can\'t change choices params Question', exeption: e}, status: 400
                return
            end
            render json: properties, status: 200    
        else
            render json: { error: 'Unable to get properties of Question'}, status: 400
        end
    end
    
end
