class Api::V1::Element::QuestionController < Api::V1::ApiController

    before_action :can_edit_survey

    #   GET /questions
    # def index   
    #     questions = Question.all
    #     render json: questions
    # end

    #   GET /questions
    def show
        begin
            question = Question.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Question', exeption: e}, status: 400
            return
        end
        render json: question.as_json(show_params), status: 200
        return
    end

    #   POST /questions
    def create
        question = Question.new(question_params)
        place = ElementPlace.new({"showWithGroup" => "false", "order" => 0})
        if place.save
            unless question.element.update({"elementPlace_id" => place.id})
                render json: { error: 'Unable to setup ElementPlace while creating Question'}, status: 400
                return
            end
            if question.save
                render json: question.as_json, status: 200
                return
            else 
                #print question.errors.full_messages
                render json: { error: 'Unable to create Question'}, status: 400
                return
            end
        else
            render json: { error: 'Unable to create ElementPlace while creating Question'}, status: 400
            return
        end
    end

    #   PUT /questions/:id
    def update
        begin
            question = Question.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Question', exeption: e}, status: 400
            return
        end
        if question.update(question_params)
            render json: { message: 'Question successfully update.'}, status: 200
            return
        else
            render json: { error: 'Unable to update Question.', exception: question.errors.to_json}, status: 400
            return
        end 
    end

    #   DELETE /question/:id
    def destroy
        begin
            question = Question.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Question', exeption: e}, status: 400
            return
        end
        begin
            properties = question.getQuestionData
            properties.remove_from_base
        rescue Exception => e
            render json: { error: 'Unable to delete QuestionData.', exeption: e}, status: 400
            return
        end

        if question.destroy
            render json: { message: 'Question successfully deleted.'}, status: 200
            return
        else 
            render json: { error: 'Unable to delete Question.', exception: question.errors.to_json}, status: 400
            return
        end
    end

    #   PUT /question/joinGroup/:id
    def joinGroup
        begin
            question = Question.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Question', exeption: e}, status: 400
            return
        end
        if question.element.update(link_to_group_params)
            render json: { message: 'Question successfully joined group.'}, status: 200
            return
        else
            render json: { error: 'Unable to join group for Question.', exception: question.errors.to_json}, status: 400
            return
        end 
    end

    #   PUT /question/leaveGroup/:id
    def leaveGroup
        begin
            question = Question.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Question', exeption: e}, status: 400
            return
        end
        if question.element.update(:element_group_id => nil)
            render json: { message: 'Question successfully left group.'}, status: 200
            return
        else
            render json: { error: 'Unable to leave group for Question.', exception: question.errors.to_json}, status: 400
            return
        end
    end

    private
        def question_params
            params.require(:question).permit(:questionType, element_attributes: [:title])
        end
        def link_to_group_params
            params.require(:element).permit(:element_group_id)
        end
        def show_params
            return {"full" => helpers.to_bool(request.query_parameters["full"]), "expendable" =>  helpers.to_bool(request.query_parameters["expendable"])}
        end
end
