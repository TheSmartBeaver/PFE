class Api::V1::Element::TextController < Api::V1::ApiController
    
    before_action :can_edit_survey
    
    #   GET /texts
    #def index
    #    texts = Text.all
    #    render json: texts
    #end

    #   GET /text
    def show
        begin
            text = Text.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Text', exeption: e}, status: 400
            return
        end
        render json: text.as_json(show_params), status: 200
        return
    end

    #   POST /text
    def create
        text = Text.new(text_params)
        place = ElementPlace.new({"showWithGroup" => "false", "order" => 0})
        if place.save
            unless text.element.update({"elementPlace_id" => place.id})
                render json: { error: 'Unable to setup ElementPlace while creating Text'}, status: 400
                return
            end
            if text.save
                render json: text.as_json, status: 200
                return
            else
                render json: { error: 'Unable to create Text'}, status: 400
                return
            end
        else
            render json: { error: 'Unable to create ElementPlace while creating Text'}, status: 400
            return
        end
    end
    
    #   PUT /text/:id
    def update
        begin
            text = Text.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Text', exeption: e}, status: 400
            return
        end
        if text.update(text_params)
            render json: { message: 'Text successfully update.'}, status: 200
            return
        else
            render json: { error: 'Unable to update Text.', exception: text.errors.to_json}, status: 400
            return
        end 
    end

    #   DELETE /text/:id
    def destroy
        begin
            text = Text.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Text', exeption: e}, status: 400
            return
        end
        if text.destroy
            render json: { message: 'Text successfully deleted.'}, status: 200
        else 
            render json: { error: 'Unable to delete Text.', exception: text.errors.to_json}, status: 400
        end
    end

    #   PUT /text/updateGroupLink/:id
    def joinGroup
        begin
            text = Text.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Text', exeption: e}, status: 400
            return
        end
        if text.element.update(link_to_group_params)
            render json: { message: 'Text successfully joined group.'}, status: 200
            return
        else
            render json: { error: 'Unable to join group for Text.', exception: text.element.errors.to_json}, status: 400
            return
        end 
    end

    #   PUT /text/leaveGroup/:id
    def leaveGroup
        begin
            text = Text.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Text', exeption: e}, status: 400
            return
        end
        if text.element.update(:element_group_id => nil)
            render json: { message: 'Text successfully left group.'}, status: 200
            return
        else
            render json: { error: 'Unable to leave group for Text.', exception: text.element.errors.to_json}, status: 400
            return
        end
    end

    private
        def text_params
            params.require(:text).permit(:data, element_attributes: [:title])
        end
        def link_to_group_params
            params.require(:element).permit(:element_group_id)
        end
        def show_params
            return {"full" => helpers.to_bool(request.query_parameters["full"]), "expendable" =>  helpers.to_bool(request.query_parameters["expendable"])}
        end
end