class Api::V1::Reply::ReplierController < Api::V1::ApiController

    before_action :can_edit_survey

    #   GET /repliers
    # def index   
    #     repliers = Replier.all
    #     render json: repliers
    # end

    #   GET /repliers
    def show    
        replier = Replier.find(params[:id])
        render json: replier.as_json(show_params), status: 200
    end

    #   POST /repliers
    def create
        begin
            survey = Survey.find(replier_params["survey_id"])
        rescue Exception => e
            render json: { error: 'Survey not finded to create Replier'}, status: 400
            return
        end
        begin
            account = Account.find(replier_params["account_id"])
        rescue Exception => e
            account = nil
        end
        begin
            replier = helpers.create_replier_for_survey(survey, account)
            render json: replier.as_json, status: 200
        rescue Exception => e
            render json: { error: 'Unable to create Replier'}, status: 400
            return
        end
    end

    #   PUT /repliers/:id
    def update
        begin
            replier = Replier.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to update Replier, Replier not found.'}, status: 400
            return
        end
        if replier.update(replier_params)
            render json: { message: 'Replier successfully update.'}, status: 200
            return
        else
            render json: { error: 'Unable to update Replier.'}, status: 400
            return
        end 
    end

    #   DELETE /repliers/:id
    def destroy
        begin
            replier = Replier.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to update Replier, Replier not found.'}, status: 400
            return
        end
        if replier.destroy
            render json: { message: 'Replier successfully deleted.'}, status: 200
            return
        else 
            render json: { error: 'Unable to delete Replier.'}, status: 400
            return
        end
    end

    private
        def replier_params
            params.require(:replier).permit(:account_id, :survey_id)
        end
        def show_params
            return {"full" => helpers.to_bool(request.query_parameters["full"]), "expendable" =>  helpers.to_bool(request.query_parameters["expendable"])}
        end
end
