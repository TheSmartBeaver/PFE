class Api::V1::Reply::ReplyingController < ApplicationController

    before_action :can_reply_survey
    
    #GET /showElement/:id
    def showElement
        begin
            element = Element.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Element', exeption: e}, status: 400
            return
        end
        if helpers.is_element_in_survey(element, @survey)
            render json: element.as_json(show_params)
            return
        else
            render json: { error: 'This element is not in this survey.'}, status: 400
            return
        end
    end

    #GET /canShowElement/:id
    def canShowElement
        begin
            element = Element.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Element', exeption: e}, status: 400
            return
        end
        if helpers.is_element_in_survey(element, @survey)
            render json: helpers.can_show_element(element, @replier)
            return
        else
            render json: { error: 'This element is not in this survey.'}, status: 400
            return
        end
    end

    #GET /canGoNextPage/:id
    def canGoNextPage
        begin
            page = ElementGroup.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Group', exeption: e}, status: 400
            return
        end
        unless page.isPage
            render json: { error: 'This is not a page'}, status: 400
            return
        end
        if helpers.is_element_in_survey(page.element, @survey)
            render json: helpers.can_go_next_page(page, @replier)
            return
        else
            render json: { error: 'This element is not in this survey.'}, status: 400
            return
        end
    end

    #GET /nextPageOrder/:id
    def nextPageOrder
        begin
            page = ElementGroup.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Group', exeption: e}, status: 400
            return
        end
        unless page.isPage
            render json: { error: 'This is not a page'}, status: 400
            return
        end
        if helpers.is_element_in_survey(page.element, @survey)
            render json: helpers.next_page_order(@survey, page, @replier)
            return
        else
            render json: { error: 'This element is not in this survey.'}, status: 400
            return
        end
    end

    #GET /getPages
    def show
        render json: @survey.as_json(show_params), status: 200
        return
    end

    #GET /getPercentage
    def getPercentage
        begin
            page = ElementGroup.find(params[:id])
            unless page.isPage
                render json: { error: 'This is not a page'}, status: 400
                return
            end
        rescue
            render json: { error: 'Page doesn\'t exist.'}, status: 400
            return
        end
        render json: helpers.survey_percentage_completed(@survey, page)
        return
    end

    #POST /replyToQuestion/:id
    def replyToQuestion
        question_id = params[:id]
        response = @replier.responses.find_by(:question_id => question_id)
        #If no response find for question, create new one
        if response.nil?
            begin
                response = Response.new(:question_id => question_id, :replier_id => @replier.id)
                unless response.save
                    render json: { error: 'Unable to create Response'}, status: 400
                    return
                end
            rescue Exception => e
                render json: { error: 'Unable to create Response', exeption: e}, status: 400
                return
            end
        end
        unless helpers.is_element_in_survey(response.question.element, @survey)
            render json: { error: 'This targeted question is not in allowed survey'}, status: 400
            return
        end
        #Edit response data
        begin
            properties = response.getResponseData
            puts "aaaaaa"
            puts properties
            properties.add_response(response_data_params)
        rescue Exception => e
            render json: { error: 'Response created but can\'t change response data', exeption: e}, status: 400
            return
        end
        render json: properties, status: 200
        return
    end

    #POST /removeReplyToQuestion/:id
    def removeReplyToQuestion
        question_id = params[:id]
        response = @replier.responses.find_by(:question_id => question_id)
        if response.nil?
            render json: { error: 'Can\'t change response data, response doesn\'t exist'}, status: 400
            return
        else
            unless helpers.is_element_in_survey(response.question.element, @survey)
                render json: { error: 'This targeted question is not in allowed survey'}, status: 400
                return
            end
            begin
                properties = response.getResponseData
                if response.question.getQuestionData.can_link_with_response
                    properties.remove_response(response_data_params.to_i)
                else
                    properties.remove_response
                end
            rescue Exception => e
                render json: { error: 'Can\'t change response data', exeption: e}, status: 400
                return
            end
            #TODO: TEST , ce qui permet de supprimer totalement la réponse si elle est vide
            if properties.response_is_empty
                response.destroy
            end
            render json: properties, status: 200
            return
        end
    end

    private
        def can_reply_survey
            begin
                @replier = Replier.find(session["replier_id"])
                @survey = Survey.find(session["reply_survey_id"])
            rescue Exception => e
                render json: { error: 'You can\'t reply on this survey.'}, status: 400
                return
            end
            unless helpers.can_reply_survey(@survey, @replier)
                render json: { error: 'You can\'t reply on this survey.'}, status: 400
                return
            end
        end
        def response_params
            params.require(:response).permit(:question_id, :replier_id)
        end
        def response_data_params
            params.require(:response_data)
        end
        def show_params
            return {"full" => helpers.to_bool(request.query_parameters["full"]), "expendable" =>  helpers.to_bool(request.query_parameters["expendable"])}
        end
end
