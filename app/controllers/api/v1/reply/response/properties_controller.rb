class Api::V1::Reply::Response::PropertiesController < Api::V1::ApiController

    skip_before_action :authenticate_account!
    before_action :can_reply_survey

    #   GET /properties
    def show
        begin
            response = Response.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Response', exeption: e}, status: 400
            return
        end
        begin
            properties = response.getResponseData
            render json: properties, status: 200
        rescue Exception => e
            render json: { error: 'Unable to get ResponseData of Response', exeption: e}, status: 400
        end
    end

    #GET /properties/addResponse/:id
    def addResponse
        begin
            response = Response.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Response', exeption: e}, status: 400
            return
        end
        unless helpers.is_element_in_survey(response.question.element, @survey)
            render json: { error: 'This targeted question is not in allowed survey'}, status: 400
            return
        end
        begin
            properties = response.getResponseData
            properties.add_response(params[:response])
        rescue Exception => e
            render json: { error: 'Unable to get properties of Response, can\'t change response data', exeption: e}, status: 400
            return
        end
        render json: properties, status: 200
    end

    #GET /properties/removeResponse/:id
    def removeResponse
        begin
            response = Response.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Response', exeption: e}, status: 400
            return
        end
        unless helpers.is_element_in_survey(response.question.element, @survey)
            render json: { error: 'This targeted question is not in allowed survey'}, status: 400
            return
        end
        begin
            properties = response.getResponseData
            properties.remove_response(params[:response_index].to_i)
        rescue Exception => e
            render json: { error: 'Unable to get properties of Response, can\'t change response data', exeption: e}, status: 400
            return
        end
        render json: properties, status: 200
    end
end
