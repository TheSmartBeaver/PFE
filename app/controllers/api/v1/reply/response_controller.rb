class Api::V1::Reply::ResponseController < Api::V1::ApiController

    skip_before_action :authenticate_account!
    before_action :can_reply_survey, except: [:show]

    # OLD
    #   GET /responses
    # def index   
    #     responses = Response.all
    #     render json: responses
    # end

    #   GET /responses
    def show
        begin
            response = Response.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Response', exeption: e}, status: 400
            return
        end
        render json: response.as_json(show_params)
        return
    end

    #   POST /responses
    def create
        begin
            if(@replier.id != response_params["replier_id"].to_i)
                render json: { error: 'Unable to create Response, bad replier'}, status: 400
                return
            end
        rescue Exception => e
            render json: { error: 'Unable to create Respons', exception: e}, status: 400
            return
        end

        response = Response.new(response_params)
        if response.save
            render json: response.as_json
            return
        else
            render json: { error: 'Unable to create Response'}, status: 400
            return
        end
    end

    #   PUT /responses/:id
    def update
        begin
            response = Response.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Response', exeption: e}, status: 400
            return
        end
        if(response.replier.survey.id != @survey.id)
            render json: { error: 'Can\'t update Response from another Survey', exeption: e}, status: 400
            return
        end
        if response.update(response_params)
            render json: { message: 'Response successfully update.'}, status: 200
            return
        else
            render json: { error: 'Unable to update Response'}, status: 400
            return
        end
    end

    #   DELETE /responses/:id
    def destroy
        begin
            response = Response.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Response', exeption: e}, status: 400
            return
        end
        if(response.replier.survey.id != @survey.id)
            render json: { error: 'Can\'t delete Response from another Survey', exeption: e}, status: 400
            return
        end
        response.destroy
        render json: { message: 'Response successfully deleted.'}, status: 200
        return
    end

    private
        def response_params
            params.require(:response).permit(:question_id, :replier_id)
        end
        def show_params
            return {"full" => helpers.to_bool(request.query_parameters["full"]), "expendable" =>  helpers.to_bool(request.query_parameters["expendable"])}
        end
end
