class Api::V1::SignupController < Devise::RegistrationsController

    respond_to :json

    def create
        super
        unless current_account == nil
            session[:account_id] = current_account.id
        end
    end
end
