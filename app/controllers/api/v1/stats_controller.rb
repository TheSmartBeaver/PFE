class Api::V1::StatsController < Api::V1::ApiController

    before_action :can_edit_survey

    # GET /getReplierOnQuestion/:id
    def getReplierOnQuestion
        begin
            question = Question.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Question', exeption: e}, status: 400
            return
        end
        render json: helpers.response_question_count(question), status: 200
        return
    end

    # GET /getReponseOccOnQuestion/:id
    def getReponseOccOnQuestion
        begin
            question = Question.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Question', exeption: e}, status: 400
            return
        end
        render json: helpers.occurence_of_responses(question), status: 200
        return
    end

    def getReplierCount
        render json: helper.replier_survey_count(@survey)
        return
    end

    def getEndedReplierCount
        render json: helper.replier_ended_survey_count(@survey)
        return
    end

end
