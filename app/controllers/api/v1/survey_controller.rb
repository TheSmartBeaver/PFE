class Api::V1::SurveyController < Api::V1::ApiController

    skip_before_action :authenticate_account!, only: [:showReplySurvey, :startReplySurvey, :endReplySurvey]
    before_action :can_reply_survey, only: [:pauseReplySurvey, :endReplySurvey]

    #   GET /survey
    # def index   
    #     surveys = Survey.all
    #     render json: surveys
    # end

    #   GET /editables
    def editableSurveys
        if session["account_id"].nil?
            render json: { error: 'Can\'t have editable survey'}, status: 400
            return
        end
        surveys = Survey.where(:account_id => session["account_id"])
        render json: surveys.as_json, status: 200
        return
    end

    #   GET /repliyable
    def replyableSurveys   
        #TODO: imple It
        render json: { error: 'Not implemented yet'}, status: 400
        return
    end

    #   GET /showEditSurvey
    def showEditSurvey
        begin
            survey = Survey.find(session["edit_survey_id"])
        rescue Exception => e
            render json: { error: 'Unable to get Survey', exeption: e}, status: 400
            return
        end
        render json: survey.as_json(show_params), status: 200
        return
    end

    #   POST /survey
    def create
        survey = Survey.new(survey_params)
        survey.account_id = session["account_id"]
        if survey.save
            render json: survey, status: 200
            return
        else 
            render json: { error: 'Unable to create Survey'}, status: 400
            return
        end
    end

    #   PUT /survey/:id
    def update
        begin
            survey = Survey.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Survey', exeption: e}, status: 400
            return
        end
        if survey.update(survey_params_update)
            render json: { message: 'Survey successfully update.'}, status: 200
            return
        else
            render json: { error: 'Unable to update Survey.'}, status: 400
            return
        end 
    end

    #   DELETE /survey/:id
    def destroy
        begin
            survey = Survey.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Survey', exeption: e}, status: 400
            return
        end
        if survey.destroy
            render json: { message: 'Survey successfully deleted.'}, status: 200
            return
        else 
            render json: { error: 'Unable to delete Survey.'}, status: 400
            return
        end
    end

    #POST /startEdit/:id
    def startEditSurvey
        begin
            survey = Survey.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Survey', exeption: e}, status: 400
            return
        end
        survey_id = survey.id
        if helpers.can_edit_survey(survey, session["account_id"])
            session["edit_survey_id"] = survey.id
            render json: { message: 'You can now edit this survey.', survey: survey}, status: 200
            #puts survey.id
            return
        else
            render json: { error: 'You can\'t edit this survey.'}, status: 400
            return
        end
    end

    #POST /startReply/:id
    def startReplySurvey
        begin
            survey = Survey.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Survey', exeption: e}, status: 400
            return
        end
        begin
            if(replier_params[:replier_id].present?)
                replier = helpers.search_replier_for_survey(survey, replier_params[:replier_id])
            end
        rescue
        end
        if survey.isPublic
            replier = helpers.create_replier_for_survey(survey, session["account_id"])
        end
        if replier.nil?
            render json: { error: 'You can\'t reply this survey, replier is not accepted.'}, status: 400
            return
        end
        session["reply_survey_id"] = survey.id
        session["replier_id"] = replier.id
        render json: session["replier_id"], status: 200
        return
    end

    #POST /endEdit
    def endEditSurvey
        session["edit_survey_id"] = nil
        render json: { message: 'Edit ended for this survey.'}, status: 200
        return
    end

    #POST /endReply
    def endReplySurvey
        @replier.ended = true
        @replier.save
        session["reply_survey_id"] = nil
        session["replier_id"] = nil
        render json: { message: 'Reply ended for this survey.'}, status: 200
        return
    end
    
    #POST /endReply
    def pauseReplySurvey
        link = helpers.generate_link(@survey, @replier)
        session["reply_survey_id"] = nil
        session["replier_id"] = nil
        render json: link, status: 200
        return
    end

    #POST /setSurveyPublic/:id
    def setPublic
        begin
            survey = Survey.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Survey', exeption: e}, status: 400
            return
        end
        if helpers.can_edit_survey(survey, session["account_id"])
            survey.isPublic = true
            survey.save
            render json: { message: 'Survey is now public.'}, status: 200
            return
        else
            render json: { error: 'You can\'t edit this survey.'}, status: 400
            return
        end
    end

    #POST /setSurveyPrivate/:id
    def setSurveyPrivate
        begin
            survey = Survey.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Survey', exeption: e}, status: 400
            return
        end
        if helpers.can_edit_survey(survey, session["account_id"])
            survey.isPublic = false
            survey.save
            render json: { message: 'Survey is now private.'}, status: 200
            return
        else
            render json: { error: 'You can\'t edit this survey.'}, status: 400
            return
        end
    end

    #GET /generateLink/:id
    def generateLink
        begin
            survey = Survey.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Survey', exeption: e}, status: 400
            return
        end
        if helpers.can_edit_survey(survey, session["account_id"])
            render json: helpers.generate_link(survey), status: 200
        else
            render json: { error: 'You can\'t generate link for this survey, survey doesn\'t allow you.'}, status: 400
        end

    end

       
    #POST /setSurveyPublic/:id
    def setSurveyPublic
        begin
            survey = Survey.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Survey', exeption: e}, status: 400
            return
        end
        survey.isPublic = true
        render json:{}, status: 200
    end

    #POST /setSurveyPrivate/:id
    def setSurveyPrivate
        begin
            survey = Survey.find(params[:id])
        rescue Exception => e
            render json: { error: 'Unable to get Survey', exeption: e}, status: 400
            return
        end
        survey.isPublic = false
        render json:{}, status: 200
    end


    private
        def survey_params
            params.require(:survey).permit(:name)
        end
        def survey_params_update
            params.require(:survey).permit(:name, :editable, :isPublic)
        end
        def replier_params
            params.require(:survey).permit(:replier_id)
        end
        def show_params
            return {"full" => helpers.to_bool(request.query_parameters["full"]), "expendable" =>  helpers.to_bool(request.query_parameters["expendable"])}
        end
end

