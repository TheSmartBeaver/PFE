class Page::V1::SessionController < ApplicationController
    before_action :set_token

    def set_token
        if session[:token]==nil
            #puts "Creating token..."
            token = SecureRandom.hex(10)
            session[:token] = token
        end
        #puts "Token:"
        #puts session[:token]
    end
end
