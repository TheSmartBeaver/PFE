module ConditionHelper

    def joinComplexCondition(conditionToLink, complexCondition)
        if conditionToLink.isRoot
            element = conditionToLink.element
            if element != nil
                begin
                    unroot(conditionToLink, element)
                    setAsRoot(complexCondition.condition, element)
                rescue
                    raise StandardError, "can't give the root for this new tree conditional parent"
                end
            end
        end
        conditionToLink.update(:complex_condition_id => complexCondition.id)
    end 

    def leaveComplexCondition(conditionToUnlink)
        if conditionToUnlink.isRoot
            element = conditionToUnlink.element
            if element != nil
                begin
                    unroot(conditionToUnlink, element)
                rescue
                    raise StandardError, "can't unroot this dirty condition"
                end
            end
            conditionToUnlink.update(:complex_condition_id => nil)
        end
        conditionToUnlink.update(:complex_condition_id => nil)
    end

    def setAsRoot(condition, element)
        if element.condition_id == nil && condition.complex_condition_id == nil
            condition.update(:isRoot => true)
            element.update(:condition_id => condition.id)
        else
            unroot(element.condition, element)
            condition.update(:isRoot => true)
            element.update(:condition_id => condition.id)
        end 
    end 

    def unroot(condition, element)
        if element.condition_id == condition.id
            condition.update(:isRoot => false)
            element.update(:condition_id => nil)
        else
            raise ArgumentError, "condition is not conditional root of element"
        end 
    end

    def testCondition(condition, replierId)
        if condition.nil?
            return true
        end
        return condition.testCondition(replierId)
    end
end
