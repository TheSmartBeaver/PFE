module QuestionDataHelper
    def getQuestionData(dataKey, questionType)
        if dataKey != nil
            case questionType
            when "text"
                return QuestionDataText.new(dataKey)
            when "number"
                return QuestionDataNumber.new(dataKey)
            when "checkbox"
                return QuestionDataCheckbox.new(dataKey)
            when "radio"
                return QuestionDataRadio.new(dataKey)
            else
                raise "@type '#{questionType}' is not supported"
            end
        else
            raise "dataKey is null"
        end
    end
end