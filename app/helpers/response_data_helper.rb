module ResponseDataHelper
    def getResponseData(dataKey, questionType)
        if dataKey != nil
            case questionType
            when "text"
                return ResponseDataText.new(dataKey)
            when "number"
                return ResponseDataNumber.new(dataKey)
            when "checkbox"
                return ResponseDataCheckbox.new(dataKey)
            when "radio"
                return ResponseDataRadio.new(dataKey)
            else
                raise "@type '#{questionType}' is not supported"
            end
        else
            raise "dataKey is null"
        end
    end
end