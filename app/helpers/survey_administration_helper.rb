module SurveyAdministrationHelper

    def can_edit_survey(survey, account_id)
        if survey.account_id == account_id
            return survey.editable
        else
            return false
        end
    end

    def can_reply_survey(survey, replier)
        if replier.ended || replier.survey.id != survey.id
            return false
        end
        return true
    end

    def is_element_in_survey(element, survey)
        if element.expendableElement_type == "ElementGroup"
            return element.expendableElement.survey.id == survey.id
        end
        if element.element_group.nil?
            return false
        else
            return is_element_in_survey(element.element_group.element, survey)
        end
    end

    def is_replier_in_survey(replier, survey)
        return replier.survey.id == survey.id
    end

    def generate_link(survey, replier = nil)
        unless(replier.nil?)
            return "/page/v1/survey/show?survey_id="+survey.id.to_s+"&replier_id="+replier.id.to_s
        end
        if survey.isPublic
            return "/page/v1/survey/show?survey_id="+survey.id.to_s
        else
            replier = create_replier_for_survey(survey)
            if replier.nil?
                raise Exception, "Replier can't be create"
            end
            return "/page/v1/survey/show?survey_id="+survey.id.to_s+"&replier_id="+replier.id.to_s
        end
    end



end