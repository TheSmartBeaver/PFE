module SurveyReplierHelper

    def can_show_element(element, replier, notShowedAreTrue = false)
        if element.element_group.nil? && element.condition.nil?
            return true
        elsif element.element_group.present? && element.condition.nil?
            return can_show_element(element.element_group.element, replier, notShowedAreTrue)
        elsif element.element_group.nil? && element.condition.present?
            return element.condition.conditionable.testCondition(replier.id, notShowedAreTrue)
        else
            return element.condition.conditionable.testCondition(replier.id, notShowedAreTrue) && can_show_element(element.element_group.element, replier, notShowedAreTrue) 
        end
    end

    #check if page is completed
    def can_go_next_page(page, replier)
        elementPage = page.element
        unless elementPage.expendableElement_type == "ElementGroup" && elementPage.expendableElement.isPage
            raise ArgumentError, "This is not a page"
        end
        return can_go_next_page_loop(elementPage, replier)
    end

    def next_page_order(survey, page, replier)
        elementPage = page.element
        unless elementPage.expendableElement_type == "ElementGroup" && elementPage.expendableElement.isPage
            raise ArgumentError, "This is not a page"
        end
        unless (can_go_next_page(page, replier))
            return page.id
        end
        # -1 cause begin to Zero
        pageCount = survey.elementGroups.count -1
        currentOrder = elementPage.elementPlace.order
        if(pageCount > currentOrder)
            tmpOrder = currentOrder+1
            while tmpOrder <= pageCount  do
                newPage = give_page_by_order(survey, tmpOrder)
                if !newPage.nil? && can_show_element(newPage.element, replier)
                    return newPage.id
                end
                tmpOrder += 1
            end
        end
        return page.id
    end

    def total_question_count(survey)
        return in_group_question_count(survey.elementGroups)
    end

    def in_group_question_count(group)
        count = 0
        for element in group do
            if element.expendableElement_type == "ElementGroup"
                count += in_group_question_count(element.expendableElement)
            elsif element.expendableElement_type == "Question"
                count += 1
            end
        end
        return count
    end

    def survey_percentage_completed(survey, flagPage)
        unless flagPage.isPage
            raise ArgumentError, "Element is not a page"
        end
        total = total_question_count(survey)
        if total == 0
            return 0
        end
        pending = 0
        for page in survey.elementGroups do
            if flagPage.element.elementPlace.order <= page.element.elementPlace.order
                pending += in_group_question_count(page)
            end
        end
        return pending*100/total
    end

    def create_replier_for_survey(survey, account_id = nil)
        if account_id.nil?
            newReplier = Replier.new(:survey_id => survey.id)
        else
            newReplier = Replier.new(:survey_id => survey.id, :account_id => account_id)
        end
        if newReplier.save
            if survey.editable
                survey.editable = false
                survey.save
            end
            return newReplier
        else
            raise ArgumentError, "Bad params for replier"
        end
    end

    def search_replier_for_survey(survey, replier_id)
        begin
            replier = Replier.find(replier_id)
        rescue
            return nil
        end
        if replier.survey.id == survey.id
            return replier
        end
        return nil
    end

    private
    #loop to check responses of questions in page 
        def can_go_next_page_loop(element, replier)
            for childElement in element.expendableElement.elements do
                if(childElement.expendableElement_type == "ElementGroup")
                    unless can_go_next_page_loop(childElement, replier)
                        return false
                    end
                elsif(childElement.expendableElement_type == "Question")
                    question = childElement.expendableElement
                    if(question.get_response_of_replier(replier).nil? && can_show_element(childElement, replier))
                        return false
                    end
                end
            end
            return true
        end

        def give_page_by_order(survey, order)
            for page in survey.elementGroups do
                if(page.element.elementPlace.order == order)
                    return page
                end
            end
            return nil
        end
end
