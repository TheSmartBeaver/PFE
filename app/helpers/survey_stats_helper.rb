module SurveyStatsHelper

    def response_question_count(question)
        return question.responses.count
    end

    def occurence_of_responses(question)
        occ = Hash.new
        occ.default = 0
        for response in question.responses
            data = response.getResponseData
            if(question.getQuestionData.can_link_with_response)
                data = response.getResponseData.linkWithQuestion(response.question.getQuestionData)
                if data.kind_of?(Array)
                    for val in data do
                        occ[val] += 1
                    end
                else
                    occ[data.response] += 1
                end
            else
                occ[data.response] += 1
            end
        end
        return occ
    end


    def replier_survey_count(survey)
        return survey.repliers.count
    end

    def replier_ended_survey_count(survey)
        return survey.repliers.where(:ended => true).count
    end

end
