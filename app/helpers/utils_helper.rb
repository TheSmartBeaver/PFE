module UtilsHelper
    def to_bool(string)
        return true if string.downcase == "true"
        return false if string.downcase == "false"
        return false
    end
end
