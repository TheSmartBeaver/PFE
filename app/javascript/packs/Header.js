import React from "react";
import InscriptionForm from "./components/account/InscriptionForm";
import ConnectionForm from "./components/account/ConnectionForm";

class Header extends React.Component {
  constructor(props) {
    super(props);
    if (this.props.currentAccount == null) {
      this.state = {
        page: "login",
      };
    } else {
      this.state = {
        page: "delete",
      };
    }
    this.changePage = this.changePage.bind(this);
  }

  changePage(newPage) {
    this.setState({
      page: newPage,
    });
  }

  render() {
    switch (this.state.page) {
      case "signup":
        return (
          <InscriptionForm
            changePage={this.changePage}
            updateCurrentAccount={this.props.updateCurrentAccount}
          />
        );
      case "login":
        return (
          <ConnectionForm
            changePage={this.changePage}
            updateCurrentAccount={this.props.updateCurrentAccount}
          />
        );
      case "delete":
        return (
          <Logout
            changePage={this.changePage}
            updateCurrentAccount={this.props.updateCurrentAccount}
          />
        );
    }
  }
}

export default Header;