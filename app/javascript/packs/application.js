import React from 'react';
import Rails from "@rails/ujs";
import Turbolinks from "turbolinks";
import * as ActiveStorage from "@rails/activestorage";
import "channels";
import axios from 'axios';
import Header from './Header';

require("./components/answering/AnsweringPanel");
require("./components/elementEditor/EditorPanel");
require("./components/dashboard/DashBoardPanel");
require("./components/account/ConnectionForm");
require("./components/account/InscriptionForm");

Rails.start();
Turbolinks.start();
ActiveStorage.start();

class Application extends React.Component {

    constructor(){
        super();
        this.state = {
          currentUser: null
        }
        this.updateCurrentUser = this.updateCurrentUser.bind(this);
      }
    componentDidMount(){
        let that = this
        axios.get('/users/check_for_user',{
        })
        .then(function(response){
          if(response.data.email){
            that.setState({
              currentUser: response.data.email
            })
          } else {
            that.setState({
              currentUser: null
            })
          }
        })
        .catch(function(error){
          console.log(error);
        })
      }
    updateCurrentUser(email) {
        this.setState({
          currentUser: email
        })
      }
    render(){
        return (
          <div>
            <Header updateCurrentUser={this.updateCurrentUser}/>
          </div>
        )
      }    
}

export default Application;