import React from "react";
import ReactDOM from "react-dom";
import Button from "@material-ui/core/Button";
import Link from "@material-ui/core/Link";
import TextField from "@material-ui/core/TextField";
import { withStyles } from "@material-ui/core";
import axios from "axios";

import NavbarForm from "../layout/NavbarForm";
import "../../../../assets/stylesheets/form.css";

const MarginButton = withStyles({
  root: {
    marginTop: 15,
    marginBottom: 10,
  },
})(Button);

class ConnectionForm extends React.Component {
  constructor(props) {
    super(props);
    this.handleSignin = this.handleSignin.bind(this);
  }

  render() {
    return (
      <>
        <NavbarForm
          title="Connexion"
          subtitle="Connectez-vous pour accéder à plus de fonctionnalités"
        />
        <form className="form-container">
          {this.connectionError}
          <TextField label="Identifiant" variant="outlined" id="email" />
          <TextField
            label="Mot de passe"
            variant="outlined"
            type="password"
            margin="normal"
            id="password"
          />
          <MarginButton
            onClick={this.handleSignin}
            variant="contained"
            color="primary"
            type="submit"
          >
            Se connecter
          </MarginButton>
          <Link href="/page/v1/account/signup">S'inscrire</Link>
        </form>
      </>
    );
  }

  handleSignin(e) {
    e.preventDefault();
    let that = this;
    axios 
      .post("/api/v1/login", {  //send request to connect with usernames
        account: {
          email: document.getElementById("email").value,
          password: document.getElementById("password").value,
        },
      })
      .then(function (response) {
        window.location.reload();
      })
      .catch(function (error) {
        if(error.response.status == 401)
          alert("Combinaison email/mot de passe incorrecte")
        else alert ("Erreur: veuillez réessayer")
      });
  }
}

document.addEventListener("turbolinks:load", () => {
  const app = document.getElementById("form-connection");
  app && ReactDOM.render(<ConnectionForm />, app);
});

export default ConnectionForm;
