import React from "react";
import ReactDOM from "react-dom";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { withStyles } from "@material-ui/core";
import axios from "axios";

import NavbarForm from "../layout/NavbarForm";
import "../../../../assets/stylesheets/form.css";

const MarginButton = withStyles({
  root: {
    marginTop: 15,
  },
})(Button);

class InscriptionForm extends React.Component {
  constructor(props) {
    super(props);
    this.handleSignup = this.handleSignup.bind(this);
  }

  render() {
    return (
      <>
        <NavbarForm
          title="Inscription"
          subtitle="Inscrivez-vous pour créer des questionnaires"
        />

        <form className="form-container">
          <TextField
            id="first-name"
            label="Prénom"
            variant="outlined"
            margin="normal"
          />
          <TextField
            id="last-name"
            label="Nom"
            variant="outlined"
            margin="normal"
          />
          <TextField
            id="email"
            label="Adresse mail"
            variant="outlined"
            margin="normal"
            required
          />
          <TextField
            id="password"
            label="Mot de passe"
            variant="outlined"
            type="password"
            min-length="6"
            margin="normal"
            required
          />
          <TextField
            id="password-confirm"
            label="Confirmez le mot de passe"
            type="password"
            min-length="6"
            variant="outlined"
            margin="normal"
            required
          />
          <MarginButton
            variant="contained"
            color="primary"
            type="submit"
            onClick={this.handleSignup}
          >
            S'inscrire
          </MarginButton>
        </form>
      </>
    );
  }



  handleSignup(e) {
    e.preventDefault();
    let that = this;
    if(validateEmail() && validatePassword()){ 
      //if the email and password are syntactically correct (and the email is not already in use), then we register the user
      axios
      .post("/api/v1/signup", {
        account: {
          email: document.getElementById("email").value,
          password: document.getElementById("password").value,
          password_confirmation: document.getElementById("password-confirm")
            .value
        },
      })
      .then(function (response) {
        window.location.reload();
      })
      .catch(function (error) {
        if(error.response.status == 422)
          alert("Un compte à cette adresse mail existe déjà")
        else
          alert("Erreur: Veuillez réessayer")
      });
    }
  }
}

document.addEventListener("turbolinks:load", () => {
  const app = document.getElementById("form-subscribe");
  app && ReactDOM.render(<InscriptionForm />, app);
});

function validateEmail(){

  //Check email formatting
  let regex= /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if(!regex.test(document.getElementById("email").value)){
    alert("Veuillez entrer une adresse mail correcte")
    return false;
  } else return true;
}

//check if the password and password confirmation match
function validatePassword(){
  if(document.getElementById("password").value.length < 6){
    alert("Veuillez entrer un mot de passe d'au moins 6 caractères");
    return false;
  }
  else if(document.getElementById("password-confirm").value != document.getElementById("password").value){
    alert("Les deux mots de passe ne correspondent pas");
    return false;
  } else return true;
}

export default InscriptionForm;
