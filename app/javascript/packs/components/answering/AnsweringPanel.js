import React, { useContext, useEffect } from "react";
import ReactDOM from "react-dom";
import CheckboxesGroup from "../surveyElement/CheckboxesGroup";
import FormPropsTextFields from "../surveyElement/FormPropsTextFields";
import NavbarAnswering from "../layout/NavbarAnswering";
import RadioButtonsGroup from "../surveyElement/RadioButtonsGroup";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import axios from "axios";
import queryString from 'query-string';

import IconButton from "@material-ui/core/IconButton";
import ArrowDropUpIcon from "@material-ui/icons/ArrowDropUp";

import ReplyStore from "../../reducers/editor/ReplyStore";
import { ReplyContext } from "../../reducers/editor/ReplyStore";
import Sort from "../../utils/Sort";

import * as actions from "../../utils/Action";
import * as actionTypes from "../../constants/ActionTypes";

export default function AnsweringPanel(props) {

  return(<ReplyStore><Answering/></ReplyStore>)
}

export function Answering(props) {
  const [showAnswers, setShowAnswers] = React.useState(false);
  const [state, dispatch] = useContext(ReplyContext);
  //console.log("dispatch existe ??" + dispatch)
  const [current_page, setCurrent_page] = React.useState(0);
  const [surveyId, setSurveyId] = React.useState("");

  let url = location.search;
  let params = queryString.parse(url);

  if (params.id == undefined) {
    alert("alert missing id param")
    return;
  }

  if (surveyId == "")
    setSurveyId(params.id)

  //const [surveyState, setSurveyState] = React.useState({"id":5,"name":"Questionnaire TEST","pages":[{"isPage":true,"element":{"id":94,"inGroup":null,"condition":null,"title":"PAGE","elementPlace":{"id":94,"order":0,"showWithGroup":false},"childType":"ElementGroup","childId":45},"elements":[{"id":96,"inGroup":45,"condition":null,"elementPlace":{"id":96,"order":0,"showWithGroup":false},"title":"TEXT","childType":"Text","childId":28,"childElement":{"data":"Questionnaire sur les chats"}},{"id":97,"inGroup":45,"condition":null,"elementPlace":{"id":97,"order":0,"showWithGroup":false},"title":"Element title","childType":"Question","childId":23,"childElement":{"questionType":"text","properties":{"title":null,"description":"Comment vous vous appelez ?","parameters":{}}}},{"id":98,"inGroup":45,"condition":null,"elementPlace":{"id":98,"order":0,"showWithGroup":false},"title":"TITLE","childType":"ElementGroup","childId":47,"childElement":{"isPage":false,"elements":[{"id":99,"inGroup":47,"condition":null,"elementPlace":{"id":99,"order":0,"showWithGroup":false},"title":"Element title","childType":"Question","childId":24,"childElement":{"questionType":"radio","properties":{"title":null,"description":"Combien de chats avez-vous ?","parameters":{"minCheck":0,"maxCheck":1,"choices":["1","2","3"]}}}}]}}]},{"isPage":true,"element":{"id":95,"inGroup":null,"condition":null,"title":"PAGE","elementPlace":{"id":95,"order":0,"showWithGroup":false},"childType":"ElementGroup","childId":46},"elements":[{"id":100,"inGroup":46,"condition":null,"elementPlace":{"id":100,"order":0,"showWithGroup":false},"title":"TEXT","childType":"Text","childId":29,"childElement":{"data":"A propos d'autres animaux"}},{"id":101,"inGroup":46,"condition":null,"elementPlace":{"id":101,"order":0,"showWithGroup":false},"title":"Element title","childType":"Question","childId":25,"childElement":{"questionType":"text","properties":{"title":null,"description":"Quels autres animaux aimez vous ?","parameters":{}}}}]}]});
  const [surveyState, setSurveyState] = React.useState("empty");
  const [surveyChildren, setSurveyChildren] = React.useState("loading");

  //TODO:Récupérer le questionnaire en entier depuis l'API pour construire les éléments répondables

  function displayAnswers() {
    let children = [];
    //parcours pages
    console.log("reply state : "+ JSON.stringify(state, null, 1))
    for (let index = 0; index < state.answers.length; index++) {
      //parcours réponses d'une page
      children.push(<li>Page {index+1}</li>);
      console.log("reply state d'une page : "+ JSON.stringify(state.answers[index], null, 1))
      for (let i = 0; i < state.answers[index].length; i++) {
        console.log("On push la réponse : "+JSON.stringify(state.answers[index][i]))
        children.push(<li>{JSON.stringify(state.answers[index][i])}</li>);
      }
    }
    console.log("Reply Children : "+children)
    return (
      <div>
        <p>Réponses aux questions</p>
        <ul>{children}</ul>
      </div>
    );
  }

  const handleEdit = () => {
    setShowAnswers(!showAnswers);
    console.log("showAnswer ???" +showAnswers)
  };

  function generateQuestion(element, pageIndex, counter) {
    let type = element.childElement.questionType;
    console.log("typeQuestion : " + type);
    switch (type) {
      case "text":
        //TODO: MODIFIER position plus tard pour réponses aux questions !!!
        return (
          <FormPropsTextFields
            pos={element.elementPlace.order}
            description={element.childElement.properties.description}
            id_question={element.childId}
            position={[pageIndex, counter]}
            reply
          />
        );
      case "radio":
        //TODO: MODIFIER position plus tard pour réponses aux questions !!!
        return (
          <RadioButtonsGroup
            pos={element.elementPlace.order}
            description={element.childElement.properties.description}
            id_question={element.childId}
            choices={element.childElement.properties.parameters.choices}
            position={[pageIndex, counter]}
            reply
          />
        );
      case "checkbox":
        //TODO: MODIFIER position plus tard pour réponses aux questions !!!
        return (
          <CheckboxesGroup
            pos={element.elementPlace.order}
            description={element.childElement.properties.description}
            id_question={element.childId}
            choices={element.childElement.properties.parameters.choices}
            position={[pageIndex, counter]}
            reply
          />
        );
      default:
        console.log("Type Question INCONNU !!!");
        break;
    }
  }

  async function handleReplyRequest() {
    //let survey_id = 25;
    let survey_id = surveyId;
    //TODO: 


    //await actions.postStartEditSurvey(survey_id)
    await actions.postSetSurveyPublic(survey_id);
    let replier_id = await actions.startReply(survey_id);
    dispatch({type: actionTypes.SET_REPLYER, replyer_id: replier_id})
    setSurveyState(await actions.getSurveyReply());
    //setSurveyChildren(await generatePageOfSurvey(surveyState, current_page));
  }

  async function iterate(tab, children, pageIndex, counter) {
    for (let index = 0; index < tab.length; index++) {
      let element = tab[index];
      //TODO: On teste si on peut afficher élément
      if(await actions.askCanShowElement(element.id) == false) continue;
      console.log("l'élément API est placé à " + element.elementPlace.order);
      let type = element.childType;
      switch (type) {
        case "Text":
          if(await actions.askCanShowElement(element.id) != true) continue;
          console.log("on push un text");
          children.push(
            <p pos={element.elementPlace.order}>{element.childElement.data}</p>
          );
          break;
        case "ElementGroup":
          console.log("on push un element");
          iterate(element.elements, children, pageIndex, counter);
          break;
        case "Question":
          if(await actions.askCanShowElement(element.id) != true) continue;
          console.log("on push une question");
          children.push(generateQuestion(element, pageIndex, counter++));
          console.log("counter " + counter);
        default:
          break;
      }
    }
  }

  async function generatePageOfSurvey(survey, pageIndex) {
    let children = [];
    let counter = [0];
    if (pageIndex > survey.pages.length - 1) {
      //TODO: activer cette route
      actions.postEndReply();
      return (
        <div>
          <h1>QUESTIONNAIRE TERMINE</h1>
          <Button variant="outlined" color="primary" onClick={handleEdit}>
            Print answers
          </Button>
        </div>
      );
    }
    let page = survey.pages[pageIndex];
    console.log("Page à afficher : " + JSON.stringify(page));
    for (let index = 0; index < page.elements.length; index++) {
      //await actions.nextPageOrder(page.element.id);

      let element = page.elements[index];
      let type = element.childType;

      switch (type) {
        case "Text":
          //TODO: On teste si on peut afficher élément
          if(await actions.askCanShowElement(element.id) != true) continue;
          console.log("on push un text");
          children.push(
            <p pos={element.elementPlace.order}>{element.childElement.data}</p>
          );
          break;
        case "ElementGroup":
          console.log("on push un element");
          //ITERATE INSIDE
          iterate(element.childElement.elements, children, pageIndex, counter);
          break;
        case "Question":
          //TODO: On teste si on peut afficher élément
          if(await actions.askCanShowElement(element.id) != true) continue;
          console.log("on push une question");
          children.push(generateQuestion(element, pageIndex, counter++));
          break;
        default:
          break;
      }
    }
    console.log(children);
    return children;
  }

  useEffect(() => {
    async function build() {
      if (surveyChildren == "loading" && surveyState != "empty") {
        let children = await generatePageOfSurvey(surveyState, current_page);
        setSurveyChildren(children);
      }
    }
    build();
  });

  /**<CheckboxesGroup position={[0, 0]} />
        <FormPropsTextFields position={[0, 1]} />
        <RadioButtonsGroup position={[0, 2]} /> */
  return (
    <>
      <NavbarAnswering
        title="Répondez aux questions + ajouter possibilité de scroller..." />
      <div style={{ height: '84%', width: '100%' }}>
        
          {surveyState == "empty" && (
            <Button
              variant="outlined"
              color="primary"
              onClick={() => {
                handleReplyRequest();
              }}
            >
              Répondre au questionnaire
            </Button>
          )}
          {surveyState != "empty" && (
            <div>
              <Sort by="pos">{surveyChildren}</Sort>
              {current_page < surveyState.pages.length && (
                <Button
                  variant="outlined"
                  color="primary"
                  onClick={() => {
                    let i = current_page + 1;
                    setSurveyChildren("loading");
                    setCurrent_page(i);
                  }}
                >
                  Go Next
                </Button>
              )}
            </div>
          )}
          {showAnswers && displayAnswers()}
      </div>

      
    </>
  );
}

document.addEventListener("turbolinks:load", () => {
  const app = document.getElementById("panel-reply");
  app && ReactDOM.render(<AnsweringPanel />, app);
});
