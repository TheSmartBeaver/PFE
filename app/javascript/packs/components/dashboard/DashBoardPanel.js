import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import axios from "axios";
import NavbarDashboard from "../layout/NavbarDashboard";
import QuestionChart from "./QuestionChart";
import QuestionStats from "./QuestionStats";
import SurveyList from "./SurveyList";
import SurveyStore from "../../reducers/editor/SurveyStore";
import '../../../../assets/stylesheets/dashboard.css';

/*Panel contenant les composants principaux de l'éditeur de questionnaire*/
class DashBoardPanel extends React.Component {
  constructor(props) {
    super(props);
    this.handleSelectedIndex = this.handleSelectedIndex.bind(this);
    this.state = {
      selectedIndex: -1,
      surveys: []
    }
  }

  //retrieve surveys of user and display them
  componentDidMount() {
    axios
      .get("http://localhost:3000/api/v1/survey/edit/editables"+"?full=true&expendable=true")
      .then(response => {
        this.setState({ surveys: response.data });
        console.log("hiaaaaaaaaaa "+JSON.stringify(response.data))
      })
      .catch(error => {
        console.log(error);
      });
  }

  //change the actual survey selected in SurveyList
  handleSelectedIndex(index) {
    this.setState({selectedIndex: index});
  }

  render() {
    return (
      <SurveyStore>
        <NavbarDashboard
          title="Panel Administrateur"
          subtitle="Retrouvez ici les statistiques de vos questionnaires"
        />
        <div className="dashboard-container">
          <SurveyList surveyList={this.state} onIndexChanged={this.handleSelectedIndex} />
          <QuestionStats surveyList={this.state} />
        </div>
      </SurveyStore>
    );
  }
}

document.addEventListener("turbolinks:load", () => {
  const app = document.getElementById("dashboard-panel");
  app && ReactDOM.render(<DashBoardPanel />, app);
});