import * as React from 'react';
import Paper from '@material-ui/core/Paper';
import {
  Chart,
  PieSeries,
  Title,
  Legend
} from '@devexpress/dx-react-chart-material-ui';

import { Animation } from '@devexpress/dx-react-chart';

const data = [
  { prog: 'C/C++', value: 12 },
  { prog: 'Java', value: 7 },
  { prog: 'Ruby', value: 14 },
  { prog: 'Python', value: 7 },
  { prog: 'Javascript', value: 6 },
];
export default class QuestionCharts extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      data,
    };
  }

  render() {
    const { data: chartData } = this.state;

    return (
      <Paper>
        <Chart
          data={chartData}
        >
          <PieSeries
            valueField="value"
            argumentField="prog"
          />
          <Title
            text="Langage préféré des français"
          />
          <Animation />
          <Legend/>
        </Chart>
        
      </Paper>
    );
  }
}