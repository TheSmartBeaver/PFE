import React, { useState } from "react";
import QuestionChart from "./QuestionChart";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListSubheader from "@material-ui/core/ListSubheader";

import "../../../../assets/stylesheets/questionstats.css";

const useStyles = makeStyles({
  subheader: {
    backgroundColor: "inherit",
  },
});

function QuestionStats(props) {
  const [selectedQuestion, setSelectedQuestion] = useState(-1);
  const classes = useStyles();

  // recursive function for DFS
  function exploreDFS(questionGroup, items) {
    var counter = 0;

    questionGroup.forEach(element => {
      // if it's an element group, then we explore this branch
      if (element.type !== undefined && !element.type.localeCompare("groupe")) {
        counter += exploreDFS(element, items);
      }
      //else we display the question
      else if (questionGroup[index].elementPlace === undefined) {
        items.push(
          <ListItem
            button
            key={currentQuestion[index].id}
            selected={currentQuestion[index].id === selectedQuestion} //if true, the button got selected styling
            onClick={(event, index) => {
              setSelectedQuestion(currentQuestion[index].id);         //change the selected question when we click on it
            }}>
            <ListItemText primary={element.content.text} />
          </ListItem>
        );
        counter++;
      }
    });
    return counter;
  }

  function depthFirstSearch(index) {
    var counter = 0;
    var items = [];
    var selectedIndex = props.surveyList.selectedIndex;

    if (selectedIndex != -1) {
      var currentQuestion = props.surveyList.surveys[selectedIndex].pages;

      currentQuestion.forEach((element, index) => {
        // if it's an element group, then we explore this branch
        if (currentQuestion[index].type !== undefined && !currentQuestion[index].type.localeCompare("ElementGroup")) {
          counter += exploreDFS(currentQuestion[index].elements);
        }
        else {  // else this is a question and we display it
          items.push(
            <ListItem
              button
              key={currentQuestion[index].id}
              selected={currentQuestion[index].id === selectedQuestion} //if true, the button got selected styling
              onClick={(event, index) => {
                setSelectedQuestion(currentQuestion[index].id);         //change the selected question when we click on it
              }}>
              <ListItemText primary={currentQuestion[index].title} />
            </ListItem>
          );
          counter++;
        }
      });
    }
    return items;
  }

  return (
    <>
      <div id="question-stats" className="question-stats-container">
        <h2>Statistiques par question</h2>

        <div className="question-stats-subcontainer">
          <div className="question-stats-left-subcontainer">
            <List classes={{ subheader: classes.subheader }} subheader={<ListSubheader>Questions</ListSubheader>}>
              {depthFirstSearch(props.surveyList)}
            </List>
          </div>

          <div className="question-stats-right-subcontainer">
            <QuestionChart surveyList={props.surveyList} selectedQuestionId={selectedQuestion} />
          </div>
        </div>
      </div>
    </>
  );
}

export default QuestionStats;