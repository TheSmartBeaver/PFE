import React, { useContext } from "react";
import axios from "axios";
import { SurveyContext } from "../../reducers/editor/SurveyStore";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemText from "@material-ui/core/ListItemText";
import IconButton from "@material-ui/core/IconButton";
import { makeStyles } from "@material-ui/core";
import { CSVLink, CSVDownload } from "react-csv";
import ShareIcon from '@material-ui/icons/Share';

import "../../../../assets/stylesheets/surveylist.css";

// Import icons from material-ui
import AddBoxOutlinedIcon from "@material-ui/icons/AddBoxOutlined"; //add
import DeleteOutlinedIcon from "@material-ui/icons/DeleteOutlined"; //delete
import EditOutlinedIcon from "@material-ui/icons/EditOutlined"; //edit
import ImportExportOutlinedIcon from "@material-ui/icons/ImportExportOutlined"; //import/export

import * as actionTypes from "../../constants/ActionTypes";
import * as actions from "../../utils/Action";

const useStyles = makeStyles(() => ({
  titleContainer: {
    color: "white",
    fontSize: 1 + "vw",
  },

  subtitle: {
    opacity: 0.54,
  },

  iconColor: {
    color: "white",
    backgroundColor: "#EB8258",
  },
}));

function SurveyList(props) {
  const classes = useStyles();
  const [state, dispatch] = useContext(SurveyContext); //state global
  const csvLinkRef = React.useRef(null);
  const headers = [
    { label: "Question ID", key: "questionID" },
    { label: "Question text", key: "questionText" },
    { label: "Answer", key: "questionAnswer" },
  ];

  // console.log("State?");
  // console.log(state);
  var csvData = [
    ["Test ?", "Test"],
    ["Test?", "Test"],
  ];

  function displaySurveys() {
    var selectedIndex = props.surveyList.selectedIndex;
    var surveys = props.surveyList.surveys;
    var items = [];
    surveys.forEach((element, index) => { //for each survey, we display it
      items.push(
        <div className="survey-item">
          <List>
            <ListItem
              button
              selected={index === selectedIndex}
              onClick={(event) => handleListItemClick(event, index)}
            >
              <ListItemText primary={element.name} />
              <ListItemAvatar>
                <IconButton
                  className={classes.iconColor}
                  selected={index === selectedIndex}
                  onClick={(event) =>
                    onClickExportToCsv(event, element.id)
                  }
                >
                  <ImportExportOutlinedIcon />
                </IconButton>
              </ListItemAvatar>
            </ListItem>
          </List>

          <List>
            <ListItem>
              <ListItemText
                classes={{
                  primary: classes.subtitle,
                }}
                primary={depthFirstSearch() + " question(s)"}
                secondary="Complété par x utilisateur(s)"
              />
              <ListItemAvatar>
                <IconButton
                  className={classes.iconColor}
                  onClick={async (event) =>{
                    await onClickDeleteSurvey(event, element.id)
                    window.location.reload();
                  }
                  }
                >
                  <DeleteOutlinedIcon />
                </IconButton>
              </ListItemAvatar>
              <ListItemAvatar>
                <IconButton
                  className={classes.iconColor}
                  onClick={async (event) => {
                    let url = await actions.getShareLink(index)
                    alert("On génère un lien : "+url)
                  }
                  }
                >
                  <ShareIcon />
                </IconButton>
              </ListItemAvatar>
              <ListItemAvatar>
                <IconButton
                  className={classes.iconColor}
                  onClick={(event) =>
                    onClickEditSurvey(event, element.id)
                  }
                >
                  <EditOutlinedIcon />
                </IconButton>
              </ListItemAvatar>
            </ListItem>
          </List>
        </div>
      );
    });
    return items;
  }

  // recursive function for DFS
  function exploreDFS(questionGroup) {
    var counter = 0;

    questionGroup.forEach(element => {
      // if it's an element group, then we explore this branch
      if (element.type !== undefined && !element.type.localeCompare("groupe")) {
        counter += exploreDFS(element, items);
      }
      else if (questionGroup[index].elementPlace === undefined) {
        counter++;
      }
    });
    return counter;
  }

  // return the number of questions of a survey
  function depthFirstSearch() {
    var counter = 0;
    const selectedIndex = props.surveyList.selectedIndex;

    if (selectedIndex != -1) {
      var currentQuestion = props.surveyList.surveys[selectedIndex].pages;

      currentQuestion.forEach((element, index) => {
        // if it's an element group, then we explore this branch
        if (currentQuestion[index].type !== undefined && !currentQuestion[index].type.localeCompare("ElementGroup")) {
          counter += exploreDFS(currentQuestion[index].elements);
        }
        else {  // else there is a question
          counter++;
        }
      });
    }
    return counter;
  }

  /* Select the list item clicked */
  const handleListItemClick = (event, index) => {
    props.onIndexChanged(index);
  };

  // edit the survey when we click on it
  const onClickEditSurvey = (event, index) => {
    const id = props.surveyList.surveys[index].id;
    console.log("Trying to edit survey " + id);
    axios
      .post("http://localhost:3000/api/v1/survey/edit/startEdit/" + id)
      .then((response) => {
        if (response.status == 200) {
          //TODO : redirect to edit page
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // delete the survey when we click on it
  const onClickDeleteSurvey = (event, index) => {
    console.log("Trying to delete survey " + index);
    axios
      .delete("http://localhost:3000/api/v1/survey/edit/survey/" + index)
      .then((response) => {
        handleListItemClick(null, -1);
        console.log(response.data + " deleted");
        
      })
      .catch((error) => {
        console.log(error);
        console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
      console.log(error.request);
      console.log('Error', error.message);
      });
  };

  function explore(questionGroup, items) {
    var counter = 0;

    questionGroup.forEach((element) => {
      // if it's an element group, then we explore this branch
      if (!element.type.localeCompare("groupe")) {
        counter += explore(element, items);
      } else {
        items.push(
          { text: element.content.text, id: element.id }
        );
        counter++;
      }
    });
    return counter;
  }

  function buildCsv(index) {
    var counter = 0;
    var items = [];
    const selectedIndex = props.surveyList.selectedIndex;

    if (selectedIndex != -1) {
      
      var currentQuestion = props.surveyList.surveys[selectedIndex].pages;
      console.error("currentQuestion : "+JSON.stringify(currentQuestion))
      actions.postStartEditSurvey(currentQuestion)
      actions.getPropertiesOfQuestion(currentQuestion)
      currentQuestion.forEach((element, index) => {
        
        // if it's an element group, then we explore this branch
        if (currentQuestion[index].type !== undefined && !currentQuestion[index].type.localeCompare("ElementGroup")) {
          counter += explore(currentQuestion[index].elements, items);
        }
        else {
          items.push({  // else there is a question
            text: currentQuestion[index].content.text,
            id: currentQuestion[index].id,
          });
          counter++;
        }
        console.log("CSV built");
      });
    }

    return items;
  }

  // export the survey to csv when we click on it
  const onClickExportToCsv = (event, index) => {
    dispatch({ type: actionTypes.MODIFY_SELECTED_SURVEY, id_selected: index });
    csvData = [];
    console.log("Survey id:" + index);
    console.log("Building CSV");
    csvData = buildCsv(index);
    console.log("CSV built");
    downloadCsv();
  };

  function downloadCsv() {
    console.log("Attempting csv download");
    console.log("Data to DL");
    console.log(csvData);
    useCsvLink();
    if (csvLinkRef.current) {
      csvLinkRef.current.link.click();
    }
  }

  function useCsvLink() {
    return (
      <div>
        <CSVLink data={buildCsv(state.selected_survey_id)} ref={csvLinkRef} />
      </div>
    );
  }

  return (
    <div className="survey-list-container">
      <h2>Vos questionnaires </h2>
      <div className="survey-add-button">
        <IconButton className={classes.iconColor} href="/page/v1/survey/edit">
          <AddBoxOutlinedIcon />
        </IconButton>
      </div>
      {displaySurveys()}
      {useCsvLink()}
    </div>
  );
}

export default SurveyList;
