import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListSubheader from "@material-ui/core/ListSubheader";

import "../../../../assets/stylesheets/userstats.css";

const useStyles = makeStyles({
  subheader: {
    backgroundColor: "inherit",
  },
});

function GenerateQuestions() {
  const items = [];
  for (var i = 0; i < 20; i++) {
    items.push(
      <ListItem button key={i}>
        <ListItemText primary="Alex" />
      </ListItem>
    );
  }

  return items;
}

function UserStats() {
  const classes = useStyles();

  return (
    <>
      <div className="user-stats-container">
        <h2>Statistiques par utilisateur</h2>

        <div className="user-stats-subcontainer">
          <div className="user-stats-left-subcontainer">
            <List classes={{subheader: classes.subheader}} subheader={<ListSubheader>Utilisateurs</ListSubheader>}>
              <GenerateQuestions />
            </List>
          </div>

          <div className="user-stats-right-subcontainer">
              Statistique à voir
          </div>
        </div>
      </div>
    </>
  );
}

export default UserStats;
