import React, { useEffect, useContext, useState } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

import InputLabel from "@material-ui/core/InputLabel";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import NativeSelect from "@material-ui/core/NativeSelect";

import { getElementsOfCurrentPageAndPrevious } from "../../utils/utils";
import { get_object_of_id } from "../../reducers/editor/SurveyReducer";
import * as elementTypes from "../../constants/ElementTypes";
import * as actionTypes from "../../constants/ActionTypes";
import ConditionCombTreeView from "./condition/ConditionCombTreeView";

import * as actions from "../../utils/Action";
import { SurveyContext } from "../../reducers/editor/SurveyStore";

import { ConditionContext } from "../../reducers/editor/ConditionStore";
const transformJS = require("js-to-json-logic");

/** Ce composant permet de créer un bouton qui ouvre une dialogue pour modifier une condition d'un élément */

export default function ConditionsDialog(props) {

  //state de la condition dialog
  const [open, setOpen] = React.useState(false);
  //state du questionnaire
  const [state, dispatch] = useContext(SurveyContext);
  const [stateConditions, dispatchConditions] = useContext(ConditionContext);
  
  //Page à partir de laquelle on ouvre l'éditeur de condition
  const { pageIndex } = props;

  console.log("elementID de la ConditionsDialog " + state.currentSelectedItemId)

  const [combinaisonsTextfield, setCombinaisonsTextfield] = React.useState("");

  const [combinedConditions, setCombinedConditions] = React.useState("");

  //
  const [unitConditionsList, setUnitConditionsList] = React.useState({
    c_list: [],
  });

  const handleCombinaisonFieldChange = (event) => {
    setCombinaisonsTextfield(event.target.value);
  };

  //Fonction pour ouvrir fermer dialog
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  async function addConditionUnit() {
    let c =
      "NAME:C" +
      unitConditionsList.c_list.length +
      " page " +
      state2.page +
      " element " +
      state2.id_element +
      " opérateur " +
      state2.op +
      " value " +
      state2.value;

    let c_elem = { name: "C" + unitConditionsList.c_list.length, value: c };
    let new_state = { ...unitConditionsList };


    let id_target1 = await actions.postQuestionTarget(state2.id_element[1]);
    let id_target2 = await actions.postConstTarget(state2.value);
    console.log(
      "On va add une basic condition avec les targets " +
        id_target1 +
        " " +
        id_target2
    );
    let id_basic = await actions.postBasicCondition(id_target1, id_target2, state2.op, false);

    let condition = {
      id: unitConditionsList.c_list.length,
      id_api: id_basic,
      page: state2.page,
      id_element: state2.id_element,
      op: state2.op,
      value: state2.value,
    };

    dispatchConditions({
      type: actionTypes.ADD_CONDITION,
      condition: condition,
    });
    let formatted_condition = "C"+condition.id+"/ page "+condition.page+"/ elem "+condition.id_element+"/ op "+condition.op+"/ value "+condition.value;
      
    new_state.c_list.push(JSON.stringify(formatted_condition));

    setUnitConditionsList(new_state);
  }

  //2ème state contenant l'état de la condition en cours d'édition
  const [state2, setState2] = React.useState({
    page: -1,
    id_element: [-1, -1],
    op: "",
    value: "",
  });

  //Permet de changer l'état de la condition en cours d'édition
  const handleChange = (event) => {
    const name = event.target.name;
    if (name == "id_element") {
      setState2({
        ...state2,
        [name]: event.target.value.split(','),
      });
    } else
      setState2({
        ...state2,
        [name]: event.target.value,
      });
  };

  //liste des éléments pouvant servir à créer des conditions
  const editableElements = getElementsOfCurrentPageAndPrevious(
    state.surveyStruct,
    pageIndex
  );
  const combineConditions = (event) => {
    let result = transformJS(combinaisonsTextfield);
    console.log("combined ----> " + JSON.stringify(result));
    setCombinedConditions(JSON.stringify(result));
  };

  function generateUnitConditions() {
    let children = [];
    for (let index = 0; index < unitConditionsList.c_list.length; index++) {
      children.push(<li>{unitConditionsList.c_list[index]}</li>);
      console.log("generateUnitConditions " + children);
    }
    return <ul>{children}</ul>;
  }

  //génère les opérations conditionneles possibles
  function generatePossibleOperators(possiblesOperators, possibleOptions) {
    //opérateurs possibles
    let possibleOperators = [];
    //options possibles : pour radio/checkbox
    let possibleOptionsElements = [];

    //renvoie les opérateurs possibles pour la liste déroulantes
    for (let index = 0; index < possiblesOperators.length; index++) {
      possibleOperators.push(
        <option value={possiblesOperators[index]}>
          {possiblesOperators[index]}
        </option>
      );
    }
    //renvoie les options possibles pour la liste déroulante des checkbox et radio
    if (possibleOptions != undefined) {
      for (let index = 0; index < possibleOptions.length; index++) {
        possibleOptionsElements.push(
          <option value={possibleOptions[index]}>
            {possibleOptions[index]}
          </option>
        );
      }
    }

    return (
      <div>
        {/**Select des pages */}
        <FormControl>
          <FormHelperText>Opérateurs conditionnels possibles</FormHelperText>
          <Select
            native
            value={state2.op}
            onChange={handleChange}
            inputProps={{
              name: "op",
              id: "native-simple_3",
            }}
          >
            <option aria-label="None" value="" />
            {possibleOperators}
          </Select>
        </FormControl>

        {/**Select des options possibles si on édite questions checkbox ou radio */}
        {possibleOptions != undefined && (
          <FormControl>
            <FormHelperText>Valeurs possibles</FormHelperText>
            <Select
              native
              value={state2.value}
              onChange={handleChange}
              inputProps={{
                name: "value",
                id: "native-simple_3",
              }}
            >
              <option aria-label="None" value="" />
              {possibleOptionsElements}
            </Select>
          </FormControl>
        )}
        {/**TextField pour questions textuelles */}
        {possibleOptions == undefined && (
          <TextField
            autoFocus
            margin="dense"
            id="valuee"
            label="valuee"
            type="valuee"
            fullWidth
            onChange={handleChange}
            inputProps={{
              name: "value",
              id: "native-simple_3",
            }}
            value={state2.value}
          />
        )}
        <Button /**Ici c'est le bouton pour créer une condition unitaire */
          onClick={(event) => {
            event.stopPropagation();
            event.preventDefault();
            addConditionUnit();
          }}
          variant="outlined"
          color="secondary"
        >
          Ajouter condition unitaire
        </Button>
      </div>
    );
  }

  /**En fonction du type de l'élément avec lequel on veut créer une condition,
   * on passe les opérateurs possibles, les options pour radio/checkbox*/
  function generatePossibleConditionsOfElement() {
    console.log(
      "On va générer les possibilités de l'élément " + state2.id_element[0]
    );
    let object = get_object_of_id(state.surveyStruct, state2.id_element[0]);
    let type = object.type;
    switch (type) {
      //TODO: Remplacer par opérateur possible, demander à Toto
      case elementTypes.QU_RADIO:
        return generatePossibleOperators(
          ["selected", "not selected"],
          object.content.options
        );
        case elementTypes.QU_CHECKBOX:
        return generatePossibleOperators(
          ["selected", "not selected"],
          object.content.options
        );
      case elementTypes.TEXTE:
        return generatePossibleOperators(["not equal", "equal"]);
      case elementTypes.GROUP:
        return <p>Conditions non dispo</p>;
      case elementTypes.QU_TEXTUELLE:
        return generatePossibleOperators(["equal", "not equal"]);
      default:
        return <p>Conditions inconnus</p>;
    }
  }

  /**On génère les éléments de la page sélectionné qui peuvent composés une conditions */
  function generateElementOfPage() {
    let result = [];
    if (state2.page > -1) {
      let elements = editableElements[state2.page];
      for (let i = 0; elements[i] != undefined; i++) {
        result.push(
          <option value={elements[i].id}> element {elements[i].id[0]}</option>
        );
      }
      return result;
    }
  }

  /**On génère le select des pages où on peut piocher des éléments pour faire une conditions */
  function generatePageSelect() {
    let pages = [];
    for (let i = 0; i < pageIndex + 1; i++) {
      pages.push(<option value={i}>page {i + 1}</option>);
    }
    return pages;
  }


  return (
    <div>
      <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        Edit conditions
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Editeur de conditions</DialogTitle>
        <DialogContent>
          <DialogContentText>Entrer la condition</DialogContentText>
          <div>
            {/**Select des pages */}
            <FormControl>
              <FormHelperText>Numéro de Page</FormHelperText>
              <Select
                native
                value={state2.page}
                onChange={handleChange}
                inputProps={{
                  name: "page",
                  id: "age-native-simple",
                }}
              >
                <option aria-label="None" value="" />
                {generatePageSelect()}
              </Select>
            </FormControl>
          </div>
          {/**Select des éléments de la page sélectionné */}
          {state2.page > -1 && (
            <div>
              <FormControl>
                <FormHelperText>ID de l'élément</FormHelperText>
                <Select
                  native
                  value={state2.id_element}
                  onChange={handleChange}
                  inputProps={{
                    name: "id_element",
                    id: "age-native-simple_2",
                  }}
                >
                  <option aria-label="None" value="" />
                  {generateElementOfPage()}
                </Select>
              </FormControl>
              {/** C'est ici qu'on génère les opérateurs possibles */}
              {state2.id_element[0] > -1 &&
                generatePossibleConditionsOfElement()}
            </div>
          )}
          {/**C'est ici qu'on display les condtions unitaires */}
          <p>Condtions unitaires crées :</p>
          {generateUnitConditions()}
          {/** C'est ICI qu'on combine les conditions */}
          <p>Combineur</p>
          <ConditionCombTreeView />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleClose} color="primary">
            Edit
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
