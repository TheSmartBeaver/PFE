import React, { useEffect, useContext, useState } from "react";
import ReactDOM from "react-dom";
import Grid from "@material-ui/core/Grid";

import ElementEditorPanel from "./ElementEditorPanel";
import NavbarEditor from "../layout/NavbarEditor";
import OrderableElements from "../order/OrderableElement";
import { SurveyContext } from "../../reducers/editor/SurveyStore";
import SurveyStore from "../../reducers/editor/SurveyStore";
import TreeViewPanel from "../../components/surveyTreeView/TreeViewPanel"

import * as actions from "../../utils/Action"
import * as actionTypes from "../../constants/ActionTypes"


/*Panel contenant les composants principaux de l'éditeur de questionnaire*/
export default function EditorPanel() {

  return (
      <SurveyStore>
        <EditorPanelContent />
      </SurveyStore>
  );
}

function EditorPanelContent() {
  const [initialized, setInitialized] = useState(false);
  const [state, dispatch] = useContext(SurveyContext);

  //TODO: Mettre le StartEdit au bon endroit
  useEffect(() => {
    //let id_surv = actions.postCreateSurvey()
    //dispatch({type: actionTypes.CREATE_SURVEY, id_surv: id_surv})
    //actions.postStartEditSurvey(id_surv)
    async function initSUrvey() {
      let n = await actions.countSurvey();
      console.error("Nous avons "+n+" surveys")
      let id_surv = await actions.postCreateSurvey(("Survey "+ n))
      console.log("ID du survey crée sur API " + id_surv)
      dispatch({ type: actionTypes.CREATE_SURVEY, id_surv: id_surv })
      await actions.postStartEditSurvey(id_surv)
      await actions.postSetSurveyPublic(id_surv)
      setInitialized(true);
    }
    if (!initialized) {
      initSUrvey();
    }
  }, [])

  return (
    <>
      <NavbarEditor
        title="Éditer un questionnaire"
        subtitle="Créez et éditez vos questionnaires" />
      <div style={{height: '84%', width: '100%'}}>
        <Grid
          container
          direction="row"
          justify="space-between"
          alignItems="center"
        >
          <Grid item>
            <TreeViewPanel />
          </Grid>
          <Grid item>
            <OrderableElements />
          </Grid>
          <Grid item>
            <ElementEditorPanel />
          </Grid>
        </Grid>
      </div>
    </>
  );
}

document.addEventListener("turbolinks:load", () => {
  const app = document.getElementById("panel-editor");
  app && ReactDOM.render(<EditorPanel />, app);
});
