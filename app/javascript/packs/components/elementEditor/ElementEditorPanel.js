import React, { useEffect, useContext } from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

import { SurveyContext } from "../../reducers/editor/SurveyStore";
import { get_object_of_id } from "../../reducers/editor/SurveyReducer";
import { getPageOfElementOfId } from "../../utils/utils";
import ConditionsDialog from "./ConditionsDialog";
import ConditionStore from "../../reducers/editor/ConditionStore";

import * as elementTypes from "../../constants/ElementTypes";
import * as actionTypes from "../../constants/ActionTypes";

export default function ElementEditorPanel() {
  //state du questionnaire
  const [state, dispatch] = useContext(SurveyContext);
  //state du champs texte de l'élement en cours d'édition
  const [current_element_text, setValueText] = React.useState("TEXT");
  //state du champs options de l'élement en cours d'édition
  const [current_element_options, setValueOptions] = React.useState("");
  //state de l'ID de l'élément en cours d'édition
  const [current_element_id, setValueId] = React.useState("UNKNOWN");

  const axios = require("axios");
  let current_element;

  //Fonction qui s'occupe d'appliquer changement du champs des objets en réaction à l'utilisateur
  const handleTextChange = (event) => {
    setValueText(event.target.value);
  };

  const handleOptionsChange = (event) => {
    setValueOptions(event.target.value);
  };

  /*Update state local si partie du state correspondante différente*/
  const makeTextChange = (value) => {
    //Si on a changé d'élément à éditer, on charge les champs correspondants et on update ID
    if (current_element_id != state.currentSelectedItemId) {
      current_element.content.text = value;
      setValueText(value);
      setValueId(state.currentSelectedItemId);
    }
  };

  const makeOptionsChange = (value) => {
    if (current_element_id != state.currentSelectedItemId) {
      //Conversion du tableau d'options en String
      let options = "";
      for (let i = 0; i < value.length; i++) {
        //Fix Bug, multiplication virgule ??
        if (value[i] == ",") {
          continue;
        }
        options = options.concat(value[i] + ",");
      }
      //On enlève la dernière virgule !
      options = options.slice(0, -1);
      console.error("Options After")
      current_element.content.options = options; //TODO:WTF
      setValueOptions(options);
      setValueId(state.currentSelectedItemId);
    }
  };

  //TODO: Implémenter changement propriétés question
  function changeQuestionDescription(desc, id_question) {
    axios
      .post(
        "/api/v1/survey/edit/element/question/properties/editDescription/" +
          id_question,
        { description: desc }
      )
      .then((response) => {
        console.log(
          "Réussi à modifier description question " +
            JSON.stringify(response.data)
        );
      })
      .catch((error) => {
        console.log(error);
      });
  }

  //TODO: Implémenter changement propriétés question
  async function addChoiceToQuestion(choice, id_question) {
    await axios
      .post(
        "/api/v1/survey/edit/element/question/properties/params/addChoice/" +
          id_question,
        { choice: choice }
      )
      .then((response) => {
        console.log(
          "Réussi à modifier description question " +
            JSON.stringify(response.data)
        );
      })
      .catch((error) => {
        console.log(error);
      });
  }

  async function saveChanges() {
    let action;
    let e_type;
    let payload;
    let tab;

    /**On définit le nouvel élément mis à jour, les actions de changement pour le reducer, et on définit le type
     * "e_type" qui servira à indiquer à l'API quel élément on modifie
     */
    switch (current_element.type) {
      case elementTypes.GROUP:
        break;
      case elementTypes.QU_TEXTUELLE:
        changeQuestionDescription(current_element_text, current_element.id[1]);
        current_element.content.text = current_element_text;
        action = {
          type: actionTypes.MODIFY_OBJECT,
          id: current_element_id,
          payload: current_element,
        };
        dispatch(action);
        break;
      //TEXTE
      case elementTypes.TEXTE:
        //On définit type exact pour l'API
        e_type = "text";
        current_element.content.text = current_element_text;
        action = {
          type: actionTypes.MODIFY_OBJECT,
          id: current_element_id,
          payload: current_element,
        };
        dispatch(action);
        /* On charge le payload pour API */
        payload = { data: current_element.content.text };

        //Mise à jour d'un élément texte via l'API
        axios
          .put(
            "/api/v1/survey/edit/element/" +
              e_type +
              "/" +
              current_element.id[1],
            payload
          )
          .then((response) => {
            console.log(
              "Réussi à modifier élément de type " +
                e_type +
                " " +
                JSON.stringify(response.data)
            );
          })
          .catch((error) => {
            console.log(error);
          });
        break;
      //RADIO
      case elementTypes.QU_RADIO:
        e_type = "radio";
        //Conversion du tableau d'options
        tab = current_element_options.split(",");
        for (let index = 0; index < tab.length; index++) {
          console.log("ADD CHOICE:::: " + tab[index]);
          await addChoiceToQuestion(tab[index], current_element.id[1]);
        }
        current_element.content.options = tab;
        action = {
          type: actionTypes.MODIFY_OBJECT,
          id: current_element_id,
          payload: payload,
        };
        dispatch(action);

        changeQuestionDescription(current_element_text, current_element.id[1]);
        //On change le champs texte de l'objet
        current_element.content.text = current_element_text;
        //On prépare l'action de modif pour le reducer
        action = {
          type: actionTypes.MODIFY_OBJECT,
          id: current_element_id,
          payload: current_element,
        };
        //On dispatch l'action
        dispatch(action);

        break;

      case elementTypes.QU_CHECKBOX:
        e_type = "checkbox";
        //Conversion du tableau d'options
        tab = current_element_options.split(",");
        for (let index = 0; index < tab.length; index++) {
          console.log("ADD CHOICE:::: " + tab[index]);
          await addChoiceToQuestion(tab[index], current_element.id[1]);
        }
        current_element.content.options = tab;
        action = {
          type: actionTypes.MODIFY_OBJECT,
          id: current_element_id,
          payload: payload,
        };
        dispatch(action);

        changeQuestionDescription(current_element_text, current_element.id[1]);
        //On change le champs texte de l'objet
        current_element.content.text = current_element_text;
        //On prépare l'action de modif pour le reducer
        action = {
          type: actionTypes.MODIFY_OBJECT,
          id: current_element_id,
          payload: current_element,
        };
        //On dispatch l'action
        dispatch(action);

        break;
      default:
    }
  }

  /*Génère formulaire d'édition d'un élément du questionnaire*/
  function generate_form() {
    /*On récupère l'objet à éditer*/
    current_element = get_object_of_id(
      state.surveyStruct,
      state.currentSelectedItemId
    );

    /*Si l'objet existe on l'édite*/
    if (current_element != undefined) {
      let type = current_element.type;

      switch (type) {
        //QU TEXTE
        case "question textuelle":
          //On fait changement des champs si on a basculé d'un élément à un autre
          makeTextChange(current_element.content.text);
          return (
            <div>
              <p>Texte Question</p>
              <TextField
                id={"qu text " + state.currentSelectedItemId}
                label="Required"
                value={current_element_text}
                onChange={handleTextChange}
                name={"QUU text " + state.currentSelectedItemId}
              />
              <div>
                <Button
                  variant="outlined"
                  color="primary"
                  onClick={saveChanges}
                >
                  Save Changes
                </Button>
              </div>
            </div>
          );
        //GROUPE
        case "groupe":
          return <div>je suis un groupe</div>;
        //QU RADIO
        case "question radio":
          makeTextChange(current_element.content.text);
          makeOptionsChange(current_element.content.options);
          return (
            <div>
              <p>Texte Question</p>
              <TextField
                id={"qu radio " + state.currentSelectedItemId}
                label="Required"
                value={current_element_text}
                onChange={handleTextChange}
                name={"QUU radio " + state.currentSelectedItemId}
              />
              <p>Options radio à séparer par virgules</p>
              <TextField
                id={"options radio " + state.currentSelectedItemId}
                label="Required"
                value={current_element_options}
                onChange={handleOptionsChange}
                name={"optionSS radio " + state.currentSelectedItemId}
              />
              <div>
                <Button
                  variant="outlined"
                  color="primary"
                  onClick={saveChanges}
                >
                  Save Changes
                </Button>
              </div>
            </div>
          );
        //QU CHECKBOX
        case elementTypes.QU_CHECKBOX:
          makeTextChange(current_element.content.text);
          makeOptionsChange(current_element.content.options);
          return (
            <div>
              <p>Texte Question</p>
              <TextField
                id={"qu checkbox " + state.currentSelectedItemId}
                label="Required"
                value={current_element_text}
                onChange={handleTextChange}
                name={"QUU checkbox " + state.currentSelectedItemId}
              />
              <p>Options Checkbox à séparer par virgules</p>
              <TextField
                id={"options checkbox " + state.currentSelectedItemId}
                label="Required"
                value={current_element_options}
                onChange={handleOptionsChange}
                name={"optionSS checkbox " + state.currentSelectedItemId}
              />
              <div>
                <Button
                  variant="outlined"
                  color="primary"
                  onClick={saveChanges}
                >
                  Save Changes
                </Button>
              </div>
            </div>
          );
        // TEXTE
        case "texte":
          makeTextChange(current_element.content.text);
          return (
            <div>
              <p>Texte Informatif</p>
              <TextField
                id={"text " + state.currentSelectedItemId}
                label="Required"
                value={current_element_text}
                onChange={handleTextChange}
                name={"texttt " + state.currentSelectedItemId}
              />
              <div>
                <Button
                  variant="outlined"
                  color="primary"
                  onClick={saveChanges}
                >
                  Save Changes
                </Button>
              </div>
            </div>
          );
        default:
          console.error("ELEMENT INCONNU");
          return state;
      }
    }

    return <div>Aucun élément sélectionné</div>;
  }

  return (
    <div>
      <p>On peut éditer un élément du questionnaire</p>
      <p>Element en cours d'édition ! : {state.currentSelectedItemId}</p>
      {generate_form()}
      <ConditionStore>
        <ConditionsDialog
          nodeId={state.currentSelectedItemId}
          pageIndex={getPageOfElementOfId(
            state.surveyStruct,
            state.currentSelectedItemId
          )}
        />{" "}
      </ConditionStore>
      {/* TODO: Mettre le CORRECT pageIndex ! */}
    </div>
  );
}
