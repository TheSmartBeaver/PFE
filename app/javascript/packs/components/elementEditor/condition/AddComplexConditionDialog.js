import React, { useEffect, useContext, useState } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

import IconButton from "@material-ui/core/IconButton";
import AddCircleIcon from "@material-ui/icons/AddCircle";

import * as elementTypes from "../../../constants/ElementTypes";
import * as actionTypes from "../../../constants/ActionTypes";

import { ConditionContext } from "../../../reducers/editor/ConditionStore";
const transformJS = require("js-to-json-logic");

/** Ce composant permet de créer un bouton qui ouvre une dialogue pour modifier une condition d'un élément */

export default function AddComplexConditionDialog(props) {
  //state de la condition dialog
  const [open, setOpen] = React.useState(false);
  //state du questionnaire
  //const [state, setState] = useContext(SurveyContext);

  const [stateConditions, dispatchConditions] = useContext(ConditionContext);
  //Page à partir de laquelle on ouvre l'éditeur de condition
  const { nodeId } = props;
  const [selectedCondition, setSelectedCondition] = React.useState("");

  const [rootButton, setRootButton] = React.useState(false);

  const handleChange = (event) => {
    setSelectedCondition(event.target.value);
    let index = event.target.value;
    dispatchConditions({ type: actionTypes.ADD_CONDITION_TO_COMPLEX, type_cond: "cond", cond: stateConditions.unitConditions[index], id: nodeId })
    setOpen(false);
  }

  //Fonction pour ouvrir fermer dialog
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  function addOperator(op){
    //Si il s'agit de la complexe racine
    if( nodeId == 0 ){
      dispatchConditions({ type: actionTypes.ADD_COMPLEX_CONDITION, type_cond: op, cond: [], id: nodeId })
      //TODO: On bloque l'ajout d'une deuxième racine
      setRootButton(true)
    }
    else {
      //Si il s'agit de complexe enfant de la racine
      dispatchConditions({ type: actionTypes.ADD_CONDITION_TO_COMPLEX, type_cond: op, cond: [], id: nodeId })
    }
    setOpen(false);
  }

  function generateAvailableConditions(){
    let children = [];
    for(let i = 0; i < stateConditions.unitConditions.length; i++){
      children.push(<option value={stateConditions.unitConditions[i].id}>{stateConditions.unitConditions[i].id}</option>);
    }
    return children;
  }

  if(stateConditions.conditions.length == 0 && rootButton == true){
    setRootButton(false)
  }

  return (
    <span>
      <IconButton
                  disabled={rootButton}
                  onClick={(event) => {
                    event.stopPropagation();
                    event.preventDefault();
                    setOpen(true)
                  }}
                >
                  <AddCircleIcon />
                </IconButton>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Editeur de conditions</DialogTitle>
        <DialogContent>
          <DialogContentText>Sélectionnez des conditions à ajouter</DialogContentText>
          
          <Button onClick={() => {addOperator("and")}} color="primary">
            AND
          </Button>
          <Button onClick={() => {addOperator("or")}} color="primary">
            OR
          </Button>
          <div>
            {/**Select des pages */}
            {nodeId != 0 && <FormControl>
              <FormHelperText>Condition à insérer</FormHelperText>
              <Select
                native
                value={selectedCondition}
                onChange={handleChange}
              >
                <option aria-label="None" value="" />
                {generateAvailableConditions()}
              </Select>
            </FormControl>}
          </div>
          
        </DialogContent>
        <DialogActions>
            {/**Actions pour cette dialogue ??? */}
        </DialogActions>
      </Dialog>
    </span>
  );
}