import React, { useEffect, useContext } from "react";
import PropTypes from "prop-types";
import TreeView from "@material-ui/lab/TreeView";
import TreeItem from "@material-ui/lab/TreeItem";
import Typography from "@material-ui/core/Typography";
import InfoIcon from "@material-ui/icons/Info";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import IndeterminateCheckBoxIcon from "@material-ui/icons/IndeterminateCheckBox";
import QuestionAnswerIcon from "@material-ui/icons/QuestionAnswer";
import PagesIcon from "@material-ui/icons/Pages";
import Button from "@material-ui/core/Button";

import { get_object_of_id } from "../../../reducers/editor/SurveyReducer";

import IconButton from "@material-ui/core/IconButton";
import AddCircleIcon from "@material-ui/icons/AddCircle";

import {
  SurveyContext,
  NewSurveyContext,
} from "../../../reducers/editor/SurveyStore";

import * as elementTypes from "../../../constants/ElementTypes";
import * as actionTypes from "../../../constants/ActionTypes";

import SvgIcon from "@material-ui/core/SvgIcon";
import { fade, makeStyles, withStyles } from "@material-ui/core/styles";
import Collapse from "@material-ui/core/Collapse";
import { useSpring, animated } from "react-spring/web.cjs"; // web.cjs is required for IE 11 support
import AddComplexConditionDialog from "./AddComplexConditionDialog";
import { ConditionContext } from "../../../reducers/editor/ConditionStore";

import * as actions from "../../../utils/Action";

function MinusSquare(props) {
  return (
    <SvgIcon fontSize="inherit" style={{ width: 14, height: 14 }} {...props}>
      {/* tslint:disable-next-line: max-line-length */}
      <path d="M22.047 22.074v0 0-20.147 0h-20.12v0 20.147 0h20.12zM22.047 24h-20.12q-.803 0-1.365-.562t-.562-1.365v-20.147q0-.776.562-1.351t1.365-.575h20.147q.776 0 1.351.575t.575 1.351v20.147q0 .803-.575 1.365t-1.378.562v0zM17.873 11.023h-11.826q-.375 0-.669.281t-.294.682v0q0 .401.294 .682t.669.281h11.826q.375 0 .669-.281t.294-.682v0q0-.401-.294-.682t-.669-.281z" />
    </SvgIcon>
  );
}

function PlusSquare(props) {
  return (
    <SvgIcon fontSize="inherit" style={{ width: 14, height: 14 }} {...props}>
      {/* tslint:disable-next-line: max-line-length */}
      <path d="M22.047 22.074v0 0-20.147 0h-20.12v0 20.147 0h20.12zM22.047 24h-20.12q-.803 0-1.365-.562t-.562-1.365v-20.147q0-.776.562-1.351t1.365-.575h20.147q.776 0 1.351.575t.575 1.351v20.147q0 .803-.575 1.365t-1.378.562v0zM17.873 12.977h-4.923v4.896q0 .401-.281.682t-.682.281v0q-.375 0-.669-.281t-.294-.682v-4.896h-4.923q-.401 0-.682-.294t-.281-.669v0q0-.401.281-.682t.682-.281h4.923v-4.896q0-.401.294-.682t.669-.281v0q.401 0 .682.281t.281.682v4.896h4.923q.401 0 .682.281t.281.682v0q0 .375-.281.669t-.682.294z" />
    </SvgIcon>
  );
}

function CloseSquare(props) {
  return (
    <SvgIcon
      className="close"
      fontSize="inherit"
      style={{ width: 14, height: 14 }}
      {...props}
    >
      {/* tslint:disable-next-line: max-line-length */}
      <path d="M17.485 17.512q-.281.281-.682.281t-.696-.268l-4.12-4.147-4.12 4.147q-.294.268-.696.268t-.682-.281-.281-.682.294-.669l4.12-4.147-4.12-4.147q-.294-.268-.294-.669t.281-.682.682-.281.696 .268l4.12 4.147 4.12-4.147q.294-.268.696-.268t.682.281 .281.669-.294.682l-4.12 4.147 4.12 4.147q.294.268 .294.669t-.281.682zM22.047 22.074v0 0-20.147 0h-20.12v0 20.147 0h20.12zM22.047 24h-20.12q-.803 0-1.365-.562t-.562-1.365v-20.147q0-.776.562-1.351t1.365-.575h20.147q.776 0 1.351.575t.575 1.351v20.147q0 .803-.575 1.365t-1.378.562v0z" />
    </SvgIcon>
  );
}

function TransitionComponent(props) {
  const style = useSpring({
    from: { opacity: 0, transform: "translate3d(20px,0,0)" },
    to: {
      opacity: props.in ? 1 : 0,
      transform: `translate3d(${props.in ? 0 : 20}px,0,0)`,
    },
  });

  return (
    <animated.div style={style}>
      <Collapse {...props} />
    </animated.div>
  );
}

TransitionComponent.propTypes = {
  /**
   * Show the component; triggers the enter or exit states
   */
  in: PropTypes.bool,
};

const StyledTreeItem = withStyles((theme) => ({
  iconContainer: {
    "& .close": {
      opacity: 0.3,
    },
  },
  group: {
    marginLeft: 7,
    paddingLeft: 18,
    borderLeft: `1px dashed ${fade(theme.palette.text.primary, 0.4)}`,
  },
}))((props) => (
  <TreeItem {...props} TransitionComponent={TransitionComponent} />
));

const useStyles = makeStyles({
  root: {
    height: 264,
    flexGrow: 1,
    maxWidth: 400,
  },
});

export default function ConditionCombTreeView() {
  const classes = useStyles();
  const [current_cond, setCurrentCond] = React.useContext(ConditionContext);
  const [state, setState] = React.useContext(SurveyContext);

  function iterate(tab, counter) {
    let children = [];
    for (let i = 0; i < tab.length; i++) {
      
      if (tab[i].type == "and" || tab[i].type == "or") {
        
        children.push(
          <TreeItem
            nodeId={tab[i].id.toString()}
            label={
              <span>
                {tab[i].id + " - " + tab[i].type}
                <AddComplexConditionDialog nodeId={tab[i].id.toString()} />
              </span>
            }
          >
            {iterate(tab[i].cond, counter++)}
          </TreeItem>
        );
      } else {
        children.push(
          <TreeItem
            nodeId={tab[i].id.toString()}
            label={"C"+tab[i].cond.id}
          />
        );
      }
    }
    return children;
  }

  function generateTree() {
    let children = [];
    let counter = [0];
    for (let i = 0; i < current_cond.conditions.length; i++) {
      let formatted_condition = "C"+current_cond.conditions[i].id+"/ page "+current_cond.conditions[i].page+"/ elem "+current_cond.conditions[i].id_element+"/ op "+current_cond.conditions[i].op+"/ value "+current_cond.conditions[i].value;
      if (
        current_cond.conditions[i].type == "and" ||
        current_cond.conditions[i].type == "or"
      ) {
        children.push(
          <TreeItem
            nodeId={current_cond.conditions[i].id.toString()}
            label={
              <span>
                {current_cond.conditions[i].id.toString() +
                  " - " +
                  current_cond.conditions[i].type}
                <AddComplexConditionDialog
                  nodeId={current_cond.conditions[i].id.toString()}
                />
              </span>
            }
          >
            {iterate(
              current_cond.conditions[i].cond,
              current_cond.conditions[i].id.toString()
            )}
          </TreeItem>
        );
      } else {
        children.push(
          <TreeItem
            nodeId={current_cond.conditions[i].id.toString()}
            label={
              formatted_condition
            }
          />
        );
      }
    }
    return children;
  }

  /**Fonctions pour sauvegarder combinaison de condition */
  async function iterate_for_save(cond_list, id_complex_parent) {
    for (let i = 0; i < cond_list.length; i++) {
      if (cond_list[i].type == "and" || cond_list[i].type == "or") {
        //On veut récup l'ID de la complex condition crée
        let id_complex = await actions.postComplexCondition(cond_list[i].cond);
        console.log(
          "--------------- id_complex " +
            cond_list[i].type +
            " " +
            id_complex +
            "rejoins complex " +
            id_complex_parent
        );
        //On lie la complexe condition crée à la complex parent
        await actions.putComplexJoinComplexCondition(
          id_complex[1],
          id_complex_parent[1]
        );
        //On va parcourir les conditions de la complex
        iterate_for_save(cond_list[i].cond, id_complex);
      } else {
        //La condition basique rejoins la complex suivante
        console.log(
          "--------------- id_basic " +
            cond_list[i].type +
            " " +
            cond_list[i].cond.id_api +
            " " +
            "rejoins la complex " +
            id_complex_parent
        );
        await actions.putBasicJoinComplexCondition(
          cond_list[i].cond.id_api[1],
          id_complex_parent[1]
        );
      }
    }
  }

  function resetCombinedCondition(){

  }

  function modifyConditionOfElement(id_element, condition) {
    let element = get_object_of_id(id_element, state.currentSelectedItemId);
    element.condition = condition;
    dispatch({
      type: actionTypes.MODIFY_OBJECT,
      id: id_element,
      payload: element,
    });
  }

  async function handleSave() {
    console.log(
      "Element à partir duquel on veut faire une sauvegarde " +
        state.currentSelectedItemId
    );
    //let children = [];
    //for (let i = 0; i < current_cond.conditions.length; i++) {
    if (
      current_cond.conditions[0].type == "and" ||
      current_cond.conditions[0].type == "or"
    ) {
      //On veut récup l'ID de la complex condition crée
      let id_complex = await actions.postComplexCondition(
        current_cond.conditions[0].cond
      );
      console.log(
        "--------------- id_complexxxx RACINE " +
          current_cond.conditions[0].type +
          " " +
          id_complex
      );
      //On va parcourir les conditions basic de la complex
      iterate_for_save(current_cond.conditions[0].cond, id_complex);

      //TODO: il y'a un not found 404 ???
      //On veut lier la complex condition racine à l'élément appelant
      console.log(
        "---------------- On veut lier cond complex " +
          id_complex +
          " à élément " +
          state.currentSelectedItemId
      );
      actions.putSetAsRoot(id_complex[1], state.currentSelectedItemId);

      //TODO: Cette situation est impossible
      /*} else {
        current_cond.conditions[i]
      }*/
      //}
      //return children;

      //TODO:On modifie condition dans SurveyStore
      
      /*console.log("On modifie condition dans le surveyStore")
      modifyConditionOfElement(
        state.currentSelectedItemId,
        current_cond.conditions[0]
      );*/
      console.log("On reset combined condition")
      setCurrentCond({type:actionTypes.RESET_COMBINED_CONDITION})
    }

  }

  return (
    <span>
      <div>
        <Button
          onClick={() => {
            handleSave();
          }}
        >
          Combiner
        </Button>
      </div>
      <AddComplexConditionDialog nodeId={"0"} />
      <TreeView
        className={classes.root}
        defaultExpanded={["1"]}
        defaultCollapseIcon={<MinusSquare />}
        defaultExpandIcon={<PlusSquare />}
        defaultEndIcon={<CloseSquare />}
      >
        {generateTree()}
      </TreeView>
    </span>
  );

  /*return (
    <TreeView
      className={classes.root}
      defaultExpanded={["1"]}
      defaultCollapseIcon={<MinusSquare />}
      defaultExpandIcon={<PlusSquare />}
      defaultEndIcon={<CloseSquare />}
    >
      <StyledTreeItem nodeId="1" label="Main">
        <StyledTreeItem nodeId="2" label="Hello" />
        <StyledTreeItem nodeId="3" label="Subtree with children">
          <StyledTreeItem nodeId="6" label="Hello" />
          <StyledTreeItem nodeId="7" label="Sub-subtree with children">
            <StyledTreeItem nodeId="9" label="Child 1" />
            <StyledTreeItem nodeId="10" label="Child 2" />
            <StyledTreeItem nodeId="11" label="Child 3" />
          </StyledTreeItem>
          <StyledTreeItem nodeId="8" label="Hello" />
        </StyledTreeItem>
        <StyledTreeItem nodeId="4" label="World" />
        <StyledTreeItem nodeId="5" label="Something something" />
      </StyledTreeItem>
    </TreeView>
  );*/
}
