import React from 'react';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import { withStyles } from '@material-ui/core';

import bgImage from '../../../../assets/images/background_header.jpg';
import logo from '../../../../assets/images/survey.png';
import '../../../../assets/stylesheets/navbar.css';

const DefaultButton = withStyles({
  root: {
    backgroundColor: '#C4C4C4'
  },

  label: {
    color: 'white'
  }
})(Button);

const ActiveButton = withStyles({
  root: {
    backgroundColor: '#72AD26'
  },

  label: {
    color: 'black'
  }
})(Button);

class NavbarForm extends React.Component {
  constructor(props){
    super(props);
    if (this.props.currentUser == null){
      this.state = {
        page:"login"
      }
    } else{
      this.state = {
        page: "delete"
      }
    }
    this.changePage = this.changePage.bind(this);
  }

  changePage(newPage) {
    this.setState({
      page: newPage
    })
  }

  render() {
    return (
      <div className="header-container">
        <img
          className="image-background"
          src={bgImage}
          alt="background header"
        />
        <div className="background-content">
          <a href="/page/v1/account/dashboard">
            <img className="logo" src={logo} alt="logo" />  {/* Logo button to dashboard */}
          </a>
          <div className="header-title">
            <p>{this.props.title}</p>
            <p>{this.props.subtitle}</p>
          </div>
          <div className="header-buttons">
            <ButtonGroup variant="contained">
              {/* <DefaultButton href="#">Aide</DefaultButton> */}
              <ActiveButton href="/page/v1/account/login">Connexion</ActiveButton>
            </ButtonGroup>
          </div>
        </div>
      </div>
    );
  }
}

export default NavbarForm;