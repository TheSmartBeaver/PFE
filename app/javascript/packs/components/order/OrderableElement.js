import React, { useEffect, useContext } from "react";
import { SurveyContext } from "../../reducers/editor/SurveyStore";

import IconButton from "@material-ui/core/IconButton";
import ArrowDropUpIcon from "@material-ui/icons/ArrowDropUp";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import { makeStyles } from "@material-ui/core/styles";
import CheckboxesGroup from "../surveyElement/CheckboxesGroup";
import FormPropsTextFields from "../surveyElement/FormPropsTextFields";
import RadioButtonsGroup from "../surveyElement/RadioButtonsGroup";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import axios from "axios";
import Sort from "../../utils/Sort";

import * as elementTypes from "../../constants/ElementTypes";
import * as actionTypes from "../../constants/ActionTypes";
import MoveButtons from "../surveyTreeView/MoveButtons";
import { getPageOfElementOfId } from "../../utils/utils";
import ReplyStore from "../../reducers/editor/ReplyStore";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    margin: "auto",
    maxWidth: 500,
  },
  moveButton: {
    color: "red",
    border: "solid blue",
  },
}));

/** Composant qui permet de faire baisser/monter un élément dans l'arbre */
export default function OrderableElements(props) {
  const [open, setOpen] = React.useState(false);
  const [state, dispatch] = useContext(SurveyContext);
  const { nodeId } = props;
  const classes = useStyles();

  function iterate(tab, children) {
    for (let i = 0; i < tab.length; i++) {
      if (tab[i].type == elementTypes.GROUP) {
        iterate(tab[i].content, children);
        //Si élément != groupe, on l'insère directement
      } else {
        children.push(tab[i]);
      }
    }

    return children;
  }

  function generateElements(pageIndex) {
    let children = [];
    let children_obj = [];
    if (state.surveyStruct[pageIndex] != undefined)
      iterate(state.surveyStruct[pageIndex].content, children);
    for (let i = 0; i < children.length; i++) {
      let obj = children[i];
      let type = obj.type;
      switch (type) {
        case elementTypes.QU_RADIO:
          children_obj.push(
            <Paper pos={obj.pos} variant="outlined">
              <MoveButtons nodeId={obj.id[0]} />
              <RadioButtonsGroup
                description={obj.content.text}
                choices={obj.content.options}
                position={[0, 2]}
              />
            </Paper>
          );
          break;

        case elementTypes.QU_CHECKBOX:
          children_obj.push(
            <Paper pos={obj.pos} variant="outlined">
              <MoveButtons nodeId={obj.id[0]} />
              <CheckboxesGroup
                description={obj.content.text}
                choices={obj.content.options}
                position={[0, 2]}
              />
            </Paper>
          );
          break;

        case elementTypes.QU_TEXTUELLE:
          children_obj.push(
            <Paper pos={obj.pos} variant="outlined">
              <MoveButtons nodeId={obj.id[0]} />
              <FormPropsTextFields
                description={obj.content.text}
                position={[0, 1]}
              />
            </Paper>
          );
          break;

        case elementTypes.TEXTE:
          //il faut insérer position dans survey si inexistant !

          children_obj.push(
            <Paper pos={obj.pos} variant="outlined">
              <MoveButtons nodeId={obj.id[0]} />
              <p>{obj.content.text}</p>
            </Paper>
          );
          break;

        default:
          break;
      }
    }
    return children_obj;
  }

  //TODO: modifier avec page de l'élément sélectionné !
  return (
    <div>
      <ReplyStore>
      <Sort by="pos">
        {generateElements(
          getPageOfElementOfId(state.surveyStruct, state.currentSelectedItemId)
        )}
      </Sort>
      </ReplyStore>
    </div>
  );
}
