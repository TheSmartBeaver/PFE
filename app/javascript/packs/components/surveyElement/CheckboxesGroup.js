import React, { useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import FormLabel from "@material-ui/core/FormLabel";
import FormControl from "@material-ui/core/FormControl";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

import { ReplyContext } from "../../reducers/editor/ReplyStore"
import * as actionTypes from "../../constants/ActionTypes";

import * as actions from "../../utils/Action"

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  formControl: {
    margin: theme.spacing(3),
  },
}));

/*Créee un composant Checkbox auquel on puisse répondre*/
export default function CheckboxesGroup(props) {
  const { position, reply, id_question, description, choices } = props;
  const classes = useStyles();

  //TODO: Le content de la question est rempli en fonction de ce qu'on récupère sur l'API
  //let options = ["Choix 1", "Choix 2", "Choix 3"];
  let options = choices;

  //State avec les options du checkbox
  let optionsState = {};
  for (let index = 0; index < options.length; index++) {
    optionsState = {
      ...optionsState,
      [options[index]]: false,
    };
  }
  const [state, setState] = React.useState(optionsState);
  const [answers, dispatchAnswer] = useContext(ReplyContext);

  //Permet d'attendre que le Reducer ait fini de modif la réponse  
  const [answerUpdated, setanswerUpdated] = React.useState(true);

  function updateResponse() {
    if (answerUpdated == false) {
    dispatchAnswer({ type: actionTypes.ADD_REPLY_TO_PAGE, answer: state, position: position })
    //TODO: envoyer bon format de réponse sur l'API
    actions.postReply(id_question, state)
    setanswerUpdated(true);
    }
  }

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
    setanswerUpdated(false);
    //Remplit résumé des réponses

  };


  function generateOptions() {
    let children = [];
    for (let index = 0; index < options.length; index++) {
      if(options[index] == ',') continue;
      children.push(<FormControlLabel
        control={
          <Checkbox
            checked={state[options[index]]}
            onChange={handleChange}
            name={options[index]}
          />
        }
        label={options[index]}
      />)
    }
    return children;
  }
  
  if (reply) {
    updateResponse();
  }

  return (
    <div>
      <p>{description}</p>
      <div className={classes.root}>
        <FormControl component="fieldset" className={classes.formControl}>
          <FormLabel component="legend">Options</FormLabel>
          <FormGroup>
            {generateOptions()}
          </FormGroup>
        </FormControl>
      </div>
    </div>
  );
}
