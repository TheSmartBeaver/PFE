import React, { useContext } from "react";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";

import { ReplyContext } from "../../reducers/editor/ReplyStore"
import * as actionTypes from "../../constants/ActionTypes";

import * as actions from "../../utils/Action"

const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: "25ch",
    },
  },
}));

/*Créee un composant TextField auquel on puisse répondre*/
export default function FormPropsTextFields(props) {
  const { position, description, reply, id_question } = props;
  const classes = useStyles();

  const [state, setState] = React.useState({
    monInput: "",
  });

  const [answerUpdated, setanswerUpdated] = React.useState(true);

  function updateResponse() {
    if (answerUpdated == false) {
      dispatchAnswer({ type: actionTypes.ADD_REPLY_TO_PAGE, answer: (description+" : "+state.monInput), position: position })
       //TODO: envoyer bon format de réponse sur l'API
    actions.postReply(id_question, (state.monInput))
      setanswerUpdated(true);
    }
  }
  const [answerssss, dispatchAnswer] = useContext(ReplyContext);
  const {answers, dispatchAnswerrrr} = useContext(ReplyContext);

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.value });
    //Remplit résumé des réponses
    setanswerUpdated(false);
  };

  if(reply)
    updateResponse();

  return (
    <form className={classes.root} noValidate autoComplete="off">
      <div>
        <p>{description}</p>
        <TextField
          required
          id="outlined-required"
          label="Required"
          defaultValue=""
          name="monInput"
          onChange={handleChange}
        />
      </div>
    </form>
  );
}
