import React, { useContext } from "react";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";

import { ReplyContext } from "../../reducers/editor/ReplyStore"
import * as actionTypes from "../../constants/ActionTypes";

import * as actions from "../../utils/Action"

/*Créee un composant Radio auquel on puisse répondre*/
export default function RadioButtonsGroup(props) {
  const {position, description, choices, reply, id_question} = props;
  const [value, setValue] = React.useState("");

  const [answers, dispatchAnswer] = useContext(ReplyContext);

  //let options = ["Choix 1", "Choix 2", "Choix 3"];
  let options = choices;

  const [answerUpdated, setanswerUpdated] = React.useState(true);

  function updateResponse() {
    if (answerUpdated == false) {
      dispatchAnswer({ type: actionTypes.ADD_REPLY_TO_PAGE, answer: (description+" : "+value), position: position })
      //TODO: envoyer bon format de réponse sur l'API
    actions.postReply(id_question, value)
      setanswerUpdated(true);
    }
  }

  const handleChange = (event) => {
    setValue(event.target.value);
    //Remplit résumé des réponses
    setanswerUpdated(false)
  };



  function generateOptions() {
    let children = [];
    for (let index = 0; index < options.length; index++) {
      if(options[index] == ',') continue;
      children.push(<FormControlLabel value={options[index]} control={<Radio />} label={options[index]} />)
    }
    return children;
  }

  console.log("Value de la radio " + value);

  if(reply)
    updateResponse();

  return (<div>
    <p>{description}</p>
    <FormControl component="fieldset">
      <FormLabel component="legend">Options</FormLabel>
      <RadioGroup
        aria-label="numberrr"
        name="numberrr1"
        value={value}
        onChange={handleChange}
      >
        {generateOptions()}
      </RadioGroup>
    </FormControl>
  </div>
  );
}
