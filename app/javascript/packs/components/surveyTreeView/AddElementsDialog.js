import React, { useEffect, useContext } from "react";
import axios from "axios";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import IconButton from "@material-ui/core/IconButton";
import AddCircleIcon from "@material-ui/icons/AddCircle";

import { SurveyContext } from "../../reducers/editor/SurveyStore";
import { getVisibleElementsInPage } from "../../utils/utils";

import * as elementTypes from "../../constants/ElementTypes";
import * as actionTypes from "../../constants/ActionTypes";
import { putPosOnApi } from "../../utils/Action";

/** Ce composant permet de créer un bouton qui ouvre une dialogue pour insérer élément dans arbre */
export default function AddElementsDialog(props) {
  const axios = require("axios");
  //state ouverture de la dialogue
  const [open, setOpen] = React.useState(false);
  //state du questionnaire en cours d'édition
  const [state, dispatch] = useContext(SurveyContext);

  const {
    labelButton,
    nodeId,
    nodeTypeId,
    IsButtonOfPage /*On informe si bouton est rattaché directement à page*/,
    pageIndex,
  } = props;

  function generatePos(pageIndex){
    let n = getVisibleElementsInPage(state.surveyStruct, pageIndex);
    return n[0].length;
  }

  /**Apelle API en indiquant quel type d'élément on veut crée, pour créer l'élément dans BACKEND et recevoir ID correspondant, et initialiser
   * l'objet crée sur l'API
   */
  async function generateElementIdAndInitEditOnAPI(type) {
    /**TODO: Demander à Thomas le format des params... */
    let created_id;
    let created_element_id;
    let e_type;
    let payload;
    //On définit le type de l'élément à ajouter à l'API
    switch (type) {
      case elementTypes.GROUP:
        e_type = "group";
        payload = {isPage: "false", survey_id:state.survey_id, element_attributes: {title: "TITLE"}}
        break;
      case elementTypes.QU_RADIO:
        e_type = "radio";
        payload = {questionType: e_type, element_attributes: {title: "Element title"}}
        break;
      case elementTypes.QU_TEXTUELLE:
        e_type = "text";
        payload = {questionType: e_type, element_attributes: {title: "Element title"}}
        break;
        case elementTypes.QU_CHECKBOX:
          e_type = "checkbox";
          payload = {questionType: e_type, element_attributes: {title: "Element title"}}
          break;
      case elementTypes.TEXTE:
        //TODO: TEXTE Uniquement pour l'instant, faire plus tard pour les autres types
        e_type = "text";
        //payload d'initialisation pour l'API
        payload = { data: "youpi", element_attributes: { title: "TEXT" } };
        break;

      default:
    }
    /**On crée élément dans API, et on reçoit un ID */
    if(type == elementTypes.TEXTE){
    await axios
      .post(
        "/api/v1/survey/edit/element/" + e_type,
        payload
      )
      .then((response) => {
        console.log(
          "Réussi à créer élément de type " +
            e_type +
            " " +
            JSON.stringify(response.data)
        );
        /**On récupère les IDs crées par l'API */
        created_id = response.data.element.childId;
        created_element_id = response.data.element.id; //TODO: Thomas a changé l'objet retourné ??, c'est plus response.data.element.id ??
        console.log("IDs crées par API : "+created_id+" "+created_element_id)
        //TODO: On reçoit l'ID et on doit ainsi update l'objet sur l'API
      })
      .catch((error) => {
        console.log(error);
      });
    }else if(type == elementTypes.QU_RADIO || type == elementTypes.QU_TEXTUELLE || type == elementTypes.QU_CHECKBOX){
      await axios
      .post(
        "/api/v1/survey/edit/element/question",
        payload
      )
      .then((response) => {
        console.log(
          "Réussi à créer élément de type question " +
            e_type +
            " " +
            JSON.stringify(response.data)
        );
        /**On récupère les IDs crées par l'API */
        created_id = response.data.element.childId;
        created_element_id = response.data.element.id; //TODO: Thomas a changé l'objet retourné ??, c'est plus response.data.element.id ??
        console.log("IDs crées par API : "+created_id+" "+created_element_id)
        //TODO: On reçoit l'ID et on doit ainsi update l'objet sur l'API
      })
      .catch((error) => {
        console.log(error);
      });
    }else if(type == elementTypes.GROUP){
      await axios
      .post(
        "/api/v1/survey/edit/element/"+e_type,
        payload
      )
      .then((response) => {
        console.log(
          "Réussi à créer élément de type " +
            e_type +
            " " +
            JSON.stringify(response.data)
        );
        /**On récupère les IDs crées par l'API */
        created_id = response.data.element.childId;
        created_element_id = response.data.element.id; //TODO: Thomas a changé l'objet retourné ??, c'est plus response.data.element.id ??
        console.log("IDs crées par API : "+created_id+" "+created_element_id)
        //TODO: On reçoit l'ID et on doit ainsi update l'objet sur l'API
      })
      .catch((error) => {
        console.log(error);
      });
    }
      //TODO: A MODIF ?? Je retourne les 2 IDs
      let returned_ids = [created_element_id,created_id];
      console.log("On return le tab d'IDs "+returned_ids);
      return returned_ids;
  }

  /*Permet de rajouter un élément à un groupe ou une page !!*/
  async function addComponent(type) {
    let payload; //payload pour state react
    //TODO: Remplacer 14 par le nodeId (ID du groupe/page) plus tard, POUR ça : Une page doit posséder un ID dans state.surveyStruct !
    let api_payload = {element:{element_group_id: nodeTypeId}} //payload pour modif via API
    let id;
    let e_type;
    let pos;

    console.log("ON ADD UN COMPONENT : " + type);

    /*En fonction du type de la question, on insère tel forme d'objet dans le state du formulaire*/
    switch (type) {
      //GROUPE
      case elementTypes.GROUP:
        e_type = "group";//TODO: Remplacer par vrai type, une fois que Thomas aura fait les routes adéquats
        //TODO: Remplacer ID par ceux renvoyer par API après !
        id = await generateElementIdAndInitEditOnAPI(elementTypes.GROUP);

        payload = { id: [id[0], id[1]], type: elementTypes.GROUP, content: [] };
        break;
        //QU RADIO
      case elementTypes.QU_RADIO:
      case elementTypes.QU_CHECKBOX:
        e_type = "question";//TODO: Remplacer par vrai type, une fois que Thomas aura fait les routes adéquats
        //TODO: Remplacer ID par ceux renvoyer par API après !
        id = await generateElementIdAndInitEditOnAPI(type);
        pos = generatePos(pageIndex)
        payload = {
          id: [id[0], id[1]],
          pos: pos,
          type: type,
          content: {
            text: "",
            options: [],
            answer: null,
          },
        };
        break;
        //QU TEXTE
      case elementTypes.QU_TEXTUELLE:
        e_type = "question";//TODO: Remplacer par vrai type, une fois que Thomas aura fait les routes adéquats
        //TODO: Remplacer ID par ceux renvoyer par API après !
        id = await generateElementIdAndInitEditOnAPI(elementTypes.QU_TEXTUELLE);
        pos = generatePos(pageIndex)
        payload = {
          id: [id[0], id[1]],
          pos: pos,
          type: elementTypes.QU_TEXTUELLE,
          content: { text: "", answer: null },
        };
        break;
        //TEXTE
      case elementTypes.TEXTE:
        e_type = "text";
        id = await generateElementIdAndInitEditOnAPI(elementTypes.TEXTE);
        pos = generatePos(pageIndex)
        payload = {
          id: id, //TODO: utiliser ID element ??? OU les 2 avec ID par rapport à table de l'élément ?
          pos: generatePos(pageIndex),
          type: elementTypes.TEXTE,
          content: { text: "" },
        };
        
        console.log("API_PAYLOAD pour texte "+JSON.stringify(api_payload));
        break;

      default:
    }

    /*On va mettre les modifs sur l'API également, il s'agit juste de l'objet initialisé*/
    //TODO: FAIRE POUR les autres éléments (questions), une fois que schéma update correctement... ---> Installer rails sur MAC
    console.log("API--PAYLOAD : "+JSON.stringify(api_payload))
    if(type == elementTypes.GROUP || type == elementTypes.TEXTE){

    axios
      .put(
        "/api/v1/survey/edit/element/"+e_type+"/joinGroup/"+id[1],
        api_payload
      )
      .then((response) => {
        console.log(
          "Réussi à initialiser élément de type " +
            e_type +
            " " +
            JSON.stringify(response.data)
        );
      })
      .catch((error) => {
        console.log(error);
      });
    }else if(type == elementTypes.QU_RADIO || type == elementTypes.QU_TEXTUELLE || type == elementTypes.QU_CHECKBOX){

      axios
        .put(
          "/api/v1/survey/edit/element/"+e_type+"/joinGroup/"+id[1],
          api_payload
        )
        .then((response) => {
          console.log(
            "Réussi à initialiser élément de type " +
              e_type +
              " " +
              JSON.stringify(response.data)
          );
        })
        .catch((error) => {
          console.log(error);
        });
      }
      if(type == elementTypes.QU_RADIO || type == elementTypes.QU_TEXTUELLE || type == elementTypes.TEXTE || type == elementTypes.QU_CHECKBOX)
      putPosOnApi(id[0], pos);

    let action;
    /*En fonction de si on ajoute dans page ou groupe*/
    if (!IsButtonOfPage) {
      action = {
        type: actionTypes.ADD_OBJECT_TO_GROUP,
        id: nodeId,
        payload: payload,
      };
    } else {
      action = {
        type: actionTypes.ADD_OBJECT_TO_PAGE,
        id: pageIndex,
        payload: payload,
      };
    }
    dispatch(action);
  }

  //Fonction ouverture/fermeture dialog
  const handleClickOpen = (e) => {
    setOpen(true);
    e.stopPropagation();
    e.preventDefault();
  };

  const handleClose = (e) => {
    setOpen(false);
    e.stopPropagation();
    e.preventDefault();
  };

  return (
    <div>
      {/**Bouton d'ajout d'élément */}
      <IconButton
        aria-label="add"
        dummyid="dummy"
        onClick={(event) => {
          event.stopPropagation();
          event.preventDefault();
          handleClickOpen(event);
        }}
      >
        {labelButton && <AddCircleIcon />}
      </IconButton>
      <Dialog
        open={open}
        onClose={(event) => {
          handleClose(event);
        }}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Ajouteur d'éléments</DialogTitle>
        <DialogContent>
          <DialogContentText>Entrer l'élément</DialogContentText>
          <Button
            onClick={(event) => {
              event.stopPropagation();
              event.preventDefault();
              addComponent(elementTypes.TEXTE);
            }}
            variant="outlined"
            color="secondary"
          >
            Texte
          </Button>
          <Button
            onClick={(event) => {
              event.stopPropagation();
              event.preventDefault();
              addComponent(elementTypes.QU_TEXTUELLE);
            }}
            variant="outlined"
            color="secondary"
          >
            Question Textuelle
          </Button>
          <Button
            onClick={(event) => {
              event.stopPropagation();
              event.preventDefault();
              addComponent(elementTypes.QU_RADIO);
            }}
            variant="outlined"
            color="secondary"
          >
            Question Radio
          </Button>
          <Button
            onClick={(event) => {
              event.stopPropagation();
              event.preventDefault();
              addComponent(elementTypes.QU_CHECKBOX);
            }}
            variant="outlined"
            color="secondary"
          >
            Question Checkbox
          </Button>
          <Button
            onClick={(event) => {
              event.stopPropagation();
              event.preventDefault();
              addComponent(elementTypes.GROUP);
            }}
            variant="outlined"
            color="secondary"
          >
            Groupe
          </Button>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={(event) => {
              handleClose(event);
            }}
            color="primary"
          >
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
