import React, { useEffect, useContext } from "react";

import IconButton from "@material-ui/core/IconButton";
import AddCircleIcon from "@material-ui/icons/AddCircle";
//import axios from "axios";
import { SurveyContext } from "../../reducers/editor/SurveyStore";

import * as elementTypes from "../../constants/ElementTypes";
import * as actionTypes from "../../constants/ActionTypes";

import * as actions from "../../utils/Action";

/** Ce composant permet de créer un bouton qui ouvre une dialogue pour insérer élément dans arbre */
export default function AddPageButton() {
  const axios = require("axios");
  const [state, dispatch] = useContext(SurveyContext);

  /**Apelle API pour créer PAGE dans BACKEND et recevoir ID correspondant */
  async function generateElementId() {

    let created_id;
    let created_element_id;
    let payload = {
      isPage: "true",
      survey_id: state.survey_id,
      element_attributes: { title: "PAGE" },
    };

    //Appel API pour création de la page
    await axios
      .post("/api/v1/survey/edit/element/group", payload)
      .then((response) => {
        created_id = response.data.element.childId;
       created_element_id = response.data.element.id;
        console.log("Réussi à créer élément de type PAGE "+created_element_id+" "+created_id);
      })
      .catch((error) => {
        console.log(error);
        console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
      console.log(error.request);
      console.log('Error', error.message);
      });
      return[created_element_id, created_id];
  }

  /*Permet de rajouterune page !!*/
  async function addComponent() {
    let payload;
    let id;

    console.log("ON ADD UNE PAGE : ");

    /*En fonction du type de la question, on insère tel forme d'objet dans le state du formulaire*/

    //TODO: Il faut que la page soit associé à un ID ?????
    id = await generateElementId();

    let action = {
      type: actionTypes.ADD_PAGE,
      id: id,
    };
    dispatch(action);
  }

  return (
    <div>
      <IconButton
        aria-label="add"
        dummyid="dummy"
        onClick={(event) => {
          event.stopPropagation();
          event.preventDefault();
          addComponent();
        }}
      >
        {<AddCircleIcon />}
      </IconButton>
    </div>
  );
}
