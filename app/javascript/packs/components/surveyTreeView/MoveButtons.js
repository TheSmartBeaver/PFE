import React, { useEffect, useContext } from "react";
import { SurveyContext } from "../../reducers/editor/SurveyStore";
import { get_object_of_id } from "../../reducers/editor/SurveyReducer";
import { makeStyles } from "@material-ui/core/styles";

import IconButton from "@material-ui/core/IconButton";
import ArrowDropUpIcon from "@material-ui/icons/ArrowDropUp";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";

import * as elementTypes from "../../constants/ElementTypes";
import * as actionTypes from "../../constants/ActionTypes";

import { putPosOnApi } from "../../utils/Action"

import {
  getPageOfElementOfId,
  get_object_at_pos_in_page,
  getVisibleElementsInPage,
} from "../../utils/utils";

const useStyles = makeStyles((theme) => ({
  moveButton: {
    color: "red",
    border: "solid blue",
  },
}));

/** Composant qui permet de faire baisser/monter un élément dans l'arbre */
export default function MoveButtons(props) {
  const [open, setOpen] = React.useState(false);
  const [state, dispatch] = useContext(SurveyContext);
  const classes = useStyles();
  const { nodeId } = props;

  const handleClickForMove = (way) => {
    //Action de modification de l'ordre d'un objet dans questionnaire
    //dispatch({ type: "MODIFY_ORDER_POSITION", id: nodeId, way: way, pageIndex: getPageOfElementOfId(state.surveyStruct, nodeId) });

    console.log("on veut modif la pos de " + nodeId);
    let target1 = get_object_of_id(state.surveyStruct, nodeId);
    let pageIndex1 = getPageOfElementOfId(state.surveyStruct, nodeId);
    let target2;
    let temp_pos;

    function changePos() {
      temp_pos = target1.pos;
      target1.pos = target2.pos;
      target2.pos = temp_pos;

      putPosOnApi(target1.id[1]);
      putPosOnApi(target2.id[1]);

      dispatch({
        type: actionTypes.MODIFY_OBJECT,
        id: target1.id[0],
        payload: target1,
      });
      dispatch({
        type: actionTypes.MODIFY_OBJECT,
        id: target2.id[0],
        payload: target2,
      });
      console.log("target 1 : " + JSON.stringify(target1));
      console.log("target 2 : " + JSON.stringify(target2));
    }

    console.log("TARGET 1 : " + JSON.stringify(target1));
    if (way == "up" && target1.pos > 0) {
      console.log("target 1 : " + JSON.stringify(target1));
      target2 = get_object_at_pos_in_page(
        state.surveyStruct,
        pageIndex1,
        target1.pos - 1
      );
      console.log("target 2 : " + JSON.stringify(target2));
      changePos();
    }
    if (
      way == "down" &&
      target1.pos <
        getVisibleElementsInPage(state.surveyStruct, pageIndex1).length + 1
    ) {
      console.log("target 1 : " + JSON.stringify(target1));
      target2 = get_object_at_pos_in_page(
        state.surveyStruct,
        pageIndex1,
        target1.pos + 1
      );
      console.log("target 2 : " + JSON.stringify(target2));
      changePos();
    }
  };

  return (
    <div>
      <IconButton /**UP */
        aria-label="add"
        className={classes.moveButton}
        dummyid="dummy"
        onClick={() => {
          handleClickForMove("up");
        }}
      >
        <ArrowDropUpIcon />
      </IconButton>

      <IconButton /**DOWN */
        aria-label="add"
        className={classes.moveButton}
        dummyid="dummy"
        onClick={() => {
          handleClickForMove("down");
        }}
      >
        <ArrowDropDownIcon />
      </IconButton>
    </div>
  );
}
