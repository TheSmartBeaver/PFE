import React, { useEffect, useContext, useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import TreeView from "@material-ui/lab/TreeView";
import TreeItem from "@material-ui/lab/TreeItem";
import Typography from "@material-ui/core/Typography";
import InfoIcon from "@material-ui/icons/Info";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import IndeterminateCheckBoxIcon from "@material-ui/icons/IndeterminateCheckBox";
import QuestionAnswerIcon from "@material-ui/icons/QuestionAnswer";
import PagesIcon from "@material-ui/icons/Pages";
import Button from "@material-ui/core/Button";

import AddElementsDialog from "./AddElementsDialog";
import { SurveyContext, NewSurveyContext } from "../../reducers/editor/SurveyStore";
import MoveButtons from "./MoveButtons";
import AddPageButton from "./AddPageButton";

import * as elementTypes from "../../constants/ElementTypes";
import * as actionTypes from "../../constants/ActionTypes";

import * as actions from "../../utils/Action"

//On stylise l'arbre
const useTreeItemStyles = makeStyles((theme) => ({
  root: {
    color: theme.palette.text.secondary,
    "&:hover > $content": {
      backgroundColor: theme.palette.action.hover,
    },
    "&:focus > $content, &$selected > $content": {
      backgroundColor: `var(--tree-view-bg-color, ${theme.palette.grey[400]})`,
      color: "var(--tree-view-color)",
    },
    "&:focus > $content $label, &:hover > $content $label, &$selected > $content $label": {
      backgroundColor: "transparent",
    },
  },
  content: {
    color: theme.palette.text.secondary,
    borderTopRightRadius: theme.spacing(2),
    borderBottomRightRadius: theme.spacing(2),
    paddingRight: theme.spacing(1),
    fontWeight: theme.typography.fontWeightMedium,
    "$expanded > &": {
      fontWeight: theme.typography.fontWeightRegular,
    },
  },
  group: {
    marginLeft: 20,
    "& $content": {
      paddingLeft: theme.spacing(0),
    },
  },
  expanded: {},
  selected: {},
  label: {
    fontWeight: "inherit",
    color: "inherit",
  },
  labelRoot: {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0.5, 0),
  },
  labelIcon: {
    marginRight: theme.spacing(1),
  },
  labelText: {
    fontWeight: "inherit",
    flexGrow: 1,
  },
}));


/**Container d'un TreeItem, on y passe de nombreuses propriétés */
function StyledTreeItem(props) {
  const classes = useTreeItemStyles();
  console.log("URL utilisé pour accéder à l'éditeur : "+window.location.href);
  const [state, dispatch] = useContext(SurveyContext);

  const {
    labelText,
    labelIcon: LabelIcon,
    labelInfo,
    color,
    bgColor,
    labelButton, //On indique si à partir de cet élément on peut créer des élements enfants (ce sont soit des pages ou groupes)
    nodeId, //ID du node, qui doit être UNIQUE ! ID doit être généré par l'API
    nodeTypeId, //ID du node par rapport au type ! ID doit être généré par l'API
    IsButtonOfPage, //Indique si le bouton d'ajout d'éléments enfants se fait directement à partir d'un élément PAGE
    pageIndex, //Indique la page à laquelle le TreeItem courant appartient
    ...other
  } = props;


  function handleItemClick(e) {
    //A chaque fois qu'on clique sur un élément, on mets à jour le dernier élément sélectionné
    dispatch({
      type: actionTypes.MODIFY_CURRENT_SELECTED_ID,
      payload: { id: nodeId },
    });
  }

  return (
    <TreeItem
      nodeId={nodeId}
      label={
        <div
          className={classes.labelRoot}
          onClick={(event) => {
            handleItemClick(event);
          }}
        >
          <LabelIcon color="inherit" className={classes.labelIcon} />
          <Typography variant="body2" className={classes.labelText}>
            {labelText}
          </Typography>
          {/**Bouton d'ajout d'éléments s'affiche ou pas, et aura des comportement différents
           * en fonction des props qui lui auront été passé
           */}
          <AddElementsDialog
            labelButton={labelButton}
            nodeId={nodeId}
            nodeTypeId={nodeTypeId}
            IsButtonOfPage={IsButtonOfPage}
            pageIndex={pageIndex}
          />
          {/** On peut modifier la position de tout élément à l'exception des pages */}
        </div>
      }
      style={{
        "--tree-view-color": color,
        "--tree-view-bg-color": bgColor,
      }}
      classes={{
        root: classes.root,
        content: classes.content,
        expanded: classes.expanded,
        selected: classes.selected,
        group: classes.group,
        label: classes.label,
      }}
      {...other}
    />
  );
}

/** On spécifie le type des props qu'on peut passé aux StyledTreeItem */
StyledTreeItem.propTypes = {
  bgColor: PropTypes.string,
  color: PropTypes.string,
  labelIcon: PropTypes.elementType.isRequired,
  labelButton: PropTypes.bool,
  IsButtonOfPage: PropTypes.bool,
  labelInfo: PropTypes.string,
  labelText: PropTypes.string.isRequired,
};

const useStyles = makeStyles({
  root: {
    height: 264,
    flexGrow: 1,
    maxWidth: 400,
  },
});

/**Fonction principale pour la génération de la TreeView */
export default function SurveyTreeView() {
  const classes = useStyles();
  //state du questionnaire en cours d'édition
  const [state, dispatch] = useContext(SurveyContext);
  

  /* Permet d'itérer tant qu'il reste des feuilles à render dans l'arbre*/
  function iterate(tab, pageIndex) {
    let children = [];

    for (let i = 0; i < tab.length; i++) {
      //Si il s'agit d'un groupe, on explore les éléments qu'il contient
      console.log("TreeItem "+tab[i].id)
      if (tab[i].type == elementTypes.GROUP) {
        let result = iterate(tab[i].content, pageIndex);
        //On push les éléments au sein du groupe
        children.push(
          <StyledTreeItem
            nodeId={tab[i].id[0].toString()}
            nodeTypeId={tab[i].id[1].toString()}
            labelText={tab[i].id[0].toString() + " - " + tab[i].type}
            labelIcon={IndeterminateCheckBoxIcon}
            pageIndex={pageIndex}
            labelButton={true}
            pageIndex={pageIndex}
          >
            {result}
          </StyledTreeItem>
        );
        //Si élément != groupe, on l'insère directement
      } else {
        children.push(
          <StyledTreeItem
            nodeId={tab[i].id[0].toString()}
            pageIndex={pageIndex}
            nodeTypeId={tab[i].id[1].toString()}
            labelText={tab[i].id[0].toString() + " - " + tab[i].type}
            labelIcon={//Si objet texte, icone différente
              (tab[i].type == elementTypes.TEXTE && InfoIcon) ||
              QuestionAnswerIcon
            }
            labelButton={false}
          />
        );
      }
    }

    return children;
  }

  function resetSurvey(){
    dispatch({type:actionTypes.RESET_SURVEY});
  }

  /*Fonction pour générer l'arbre*/
  function generateTree() {
    let children = [];
    /*On itére sur les pages*/
    for (let i = 0; i < state.surveyStruct.length; i++) {
      children.push(
        <StyledTreeItem
          nodeId={state.surveyStruct[i].id[0]}
          nodeTypeId={state.surveyStruct[i].id[1]}
          labelText={"Page " + (i+1)}
          labelIcon={PagesIcon}
          labelButton={true}
          IsButtonOfPage={true}
          pageIndex={i}
        >
          {iterate(state.surveyStruct[i].content, i)}
        </StyledTreeItem>
      );
    }
    /*On retourne l'arbre d'élément entier*/
    return children;
  }

  return (
    <div>
      <p>ID du questionnaire : {state.survey_id}</p>
      <p>Ajouter une page</p>
      {/**Bouton d'ajout d'une page */}
      <AddPageButton />
      {/*<Button
            onClick={(event) => {
              event.stopPropagation();
              event.preventDefault();
              resetSurvey();
            }}
            variant="outlined"
            color="secondary"
          >
            Reset Survey
          </Button>*/}
      <TreeView
        className={classes.root}
        defaultExpanded={["3"]}
        defaultCollapseIcon={<ArrowDropDownIcon />}
        defaultExpandIcon={<ArrowRightIcon />}
        defaultEndIcon={<div style={{ width: 24 }} />}
      >
        {generateTree()}
      </TreeView>
    </div>
  );
}
