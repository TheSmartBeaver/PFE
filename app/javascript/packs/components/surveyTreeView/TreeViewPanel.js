import React from "react";
import ReactDOM from "react-dom";
import SurveyTreeView from "./SurveyTreeView";

export default class TreeViewPanel extends React.Component {
  render() {
    return (
      <div>
        <h4>On peut controller composition du questionnaire</h4>
        <SurveyTreeView />
      </div>
    );
  }
}

document.addEventListener("turbolinks:load", () => {
  const app = document.getElementById("panel-treeview");
  app && ReactDOM.render(<TreeViewPanel />, app);
});
