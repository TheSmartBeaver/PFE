export const GROUP = "groupe";
export const QU_RADIO = "question radio";
export const QU_CHECKBOX = "question checkbox";
export const QU_TEXTUELLE = "question textuelle";
export const TEXTE = "texte";
export const PAGE = "page";
