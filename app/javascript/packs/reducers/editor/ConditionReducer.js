import * as elementTypes from "../../constants/ElementTypes";
import * as actionTypes from "../../constants/ActionTypes";

/**const conditionsState = [{
  nbElements: 6,
  conditions: [
    { type: "cond", cond: "cond 1", id:"1" },
    {
      type: "and", id:"2",
      cond: [
        { type: "cond", cond: "cond 2" , id:"3"},
        {
          type: "or", id:"4",
          cond: [
            { type: "cond", cond: "cond 3" , id:"5"},
            { type: "cond", cond: "cond 4" , id:"6"},
          ],
        },
      ],
    },
  ],
}]; */
function find_complex_condition(tab, id) {
  function iterate(tab, id) {
    for (let i = 0; i < tab.length; i++) {
      if (tab[i].id == id) return tab[i];
      if (tab[i].type == "and" || tab[i].type == "or") {
        let result = iterate(tab[i].cond, id);
        if (result != undefined) return result;
      }
    }
  }

  for (let i = 0; i < tab.conditions.length; i++) {
    if (tab.conditions[i].id == id) return tab.conditions[i];
    if (tab.conditions[i].type == "and" || tab.conditions[i].type == "or") {
      let result = iterate(tab.conditions[i].cond, id);
      if (result != undefined) return result;
    }
  }
}

const ConditionReducer = (state, action) => {
  let new_state;
  let condition;
  let newNb_elements;

  switch (action.type) {
    case actionTypes.ADD_CONDITION_TO_COMPLEX:
      console.log("ADD_CONDITION_TO_COMPLEX")
      //{ type: actionTypes.ADD_CONDITION_TO_COMPLEX, type_cond: op, cond: [] }
      //On ajoute un élément et donc on incrémente l'ID dans tous les cas
      newNb_elements = ++state.nbElements;
      new_state = { ...state };
      new_state.nbElements = newNb_elements;

      //On va ajouter cette condition à la complexe d'ID 
      condition = {
        type: action.type_cond,
        cond: action.cond,
        id: newNb_elements,
      };
      console.log("L'ID de la condition à partir de laquelle on ajoute : "+ action.id)

      //TODO: Cette situation est censé être impossible
      /*if (action.id == 0) {
        new_state.conditions.push(condition);
      } else {*/

        //On cherche la complexe condition dans laquelle on va insérer
        let target = find_complex_condition(state, action.id);
        console.log(
          "On a trouvé la complex condition où ajouté la cond " +
            JSON.stringify(target)
        );
        //On réalise l'insertion dans la complexe
        target.cond.push(condition);
      //}
      console.log(
        "On va ajouter cond " +
          JSON.stringify(condition) +
          " à la complex " +
          target
      );

      console.log("endState "+ JSON.stringify(new_state));
      return new_state;

    //ADD condition unitaire
    case actionTypes.ADD_CONDITION:
      console.log("ADD_CONDITION")
      //On ajoute un élément et donc on incrémente l'ID dans tous les cas
      newNb_elements = ++state.nbElements;
      new_state = { ...state };
      new_state.nbElements = newNb_elements;

      console.log(
        "On va add une condition " +
          JSON.stringify(action.condition) +
          " dans " +
          JSON.stringify(new_state)
      );
      //on push la condition unitaire dans la liste des conditions unitaires
      new_state.unitConditions.push(action.condition);
      console.log("endState "+ JSON.stringify(new_state));
      return new_state;

      //TODO: fonction pour fabriquer la RACINE !!!
    case actionTypes.ADD_COMPLEX_CONDITION:
      console.log("ADD_COMPLEX_CONDITION")
      //On ajoute un élément et donc on incrémente l'ID dans tous les cas
      new_state = { ...state };
      newNb_elements = ++new_state.nbElements;
      console.log("Le nouvel élément condition aura l'ID " + newNb_elements +" et non " + state.nbElements)
      new_state.nbElements = newNb_elements;

      //Il s'agit de la condition qu'on va placer à la racine !!!
      condition = {
        type: action.type_cond,
        cond: [],
        id: newNb_elements,
      };

      if (action.id == 0) new_state.conditions.push(condition);
      //TODO: Cette situation est impossible ??
      return new_state;

      case actionTypes.RESET_COMBINED_CONDITION:
      console.log("RESET_COMBINED_CONDITION")
      new_state = { ...state }
      new_state.conditions = []
      return new_state;

    default:
      console.error("ACTION INCONNU");
      return state;
  }
};

export default ConditionReducer;
