import React, { createContext, useReducer } from "react";
import ConditionReducer from "./ConditionReducer";

const conditionsState2 = {
  nbElements: 6,
  unitConditions : [],
  conditions: [
    { type: "cond", cond: "cond 1", id:"1" },
    {
      type: "and", id:"2",
      cond: [
        { type: "cond", cond: "cond 2" , id:"3"},
        {
          type: "or", id:"4",
          cond: [
            { type: "cond", cond: "cond 3" , id:"5"},
            { type: "cond", cond: "cond 4" , id:"6"},
          ],
        },
      ],
    },
  ],
};

const conditionsState = {
  nbElements: 0,
  unitConditions : [],
  conditions: [],
};

/* Permet de partager un même state aux enfants*/
const ConditionStore = ({ children }) => {
  const [state, dispatch] = useReducer(ConditionReducer, conditionsState); //CHANGER ICI !!!!!!!!!!!!!!!!!
  return (
    <ConditionContext.Provider value={[state, dispatch]}>
      {children}
    </ConditionContext.Provider>
  );
};

export const ConditionContext = createContext(conditionsState); // CHANGER ICI AUSSI !!!!!!!!!!!!!!!!!!
export default ConditionStore;
