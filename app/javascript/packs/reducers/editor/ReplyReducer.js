import * as elementTypes from "../../constants/ElementTypes";
import * as actionTypes from "../../constants/ActionTypes";

const ReplyReducer = (state, action) => {
  let new_state;

  switch (action.type) {
    case actionTypes.ADD_REPLY_TO_PAGE:
        new_state = {...state};
        if(new_state.answers[action.position[0]] == undefined){
          new_state.answers[action.position[0]] = [];
        }
        new_state.answers[action.position[0]][action.position[1]] = action.answer;
        return new_state;

      case actionTypes.SET_REPLYER:
        console.log("On set le replyer !!!")
        new_state = {...state};
        new_state.replyer_id = action.replyer_id;
        return new_state;

    default:
      console.error("ACTION INCONNU");
      return state;
  }
};

export default ReplyReducer;
