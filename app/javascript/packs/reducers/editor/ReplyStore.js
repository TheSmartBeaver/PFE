import React, { createContext, useReducer } from "react";
import ReplyReducer from "./ReplyReducer";

const replyState = {survey_id: "xxx", replyer_id: "xxx", answers:[]}; //Contiendra un tableau de tableau : les réponses par page

/* Permet de partager un même state aux enfants*/
const ReplyStore = ({ children }) => {
  const [state, dispatch] = useReducer(ReplyReducer, replyState); //CHANGER ICI !!!!!!!!!!!!!!!!!
  return (
    <ReplyContext.Provider value={[state, dispatch]}>
      {children}
    </ReplyContext.Provider>
  );
};

export const ReplyContext = createContext(replyState);// CHANGER ICI AUSSI !!!!!!!!!!!!!!!!!!
export default ReplyStore;
