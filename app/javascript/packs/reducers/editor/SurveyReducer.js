import * as elementTypes from "../../constants/ElementTypes";
import * as actionTypes from "../../constants/ActionTypes";

/* Permet d'itérer jusqu'à trouver l'élément cherché */
function iterate(tab, searched) {
  for (let i = 0; i < tab.length; i++) {
    //élément recherché trouvé = on retourne son index pour construire le chemin récursivement
    if (tab[i].id[0] == searched) {
      let indexSearched = [];
      indexSearched.unshift(i);
      return indexSearched;
    }
    //Si groupe, on itère à l'intérieur
    if (tab[i].type == elementTypes.GROUP) {
      let indexSearched = iterate(tab[i].content, searched);
      //Si on a trouvé l'élément, on construit récursivement le chemin
      if (indexSearched != undefined) {
        indexSearched.unshift(i);
        return indexSearched;
      }
    }
  }
}

/*Permet de chercher chemin d'un objet du questionnaire en utilisant son id*/
function pathOfElement(surveyTab, object_id) {
  //On parcours les pages
  for (let i = 0; i < surveyTab.length; i++) {
    let indexSearched = iterate(surveyTab[i].content, object_id);
    //Si élément trouvé, on retourne le chemin entier
    if (indexSearched != undefined) {
      indexSearched.unshift(i);
      return indexSearched;
    }
  }
}

/*Permet de récupérer l'objet à l'id passé en param*/
export function get_object_of_id(tab, id_searched) {
  //On parcours les pages
  for (let i = 0; i < tab.length; i++) {
    let result = iter2(tab[i].content, id_searched);
    if (result != undefined) {
      return result;
    }
  }
}

/*Permet de itérer jusqu'à trouver l'objet à l'id passé en param*/
function iter2(tab, searched) {
  for (let i = 0; i < tab.length; i++) {
    if (tab[i].id[0] == searched) {
      return tab[i];
    }
    if (tab[i].type == elementTypes.GROUP) {
      let indexSearched = iter2(tab[i].content, searched);
      if (indexSearched != undefined) {
        return indexSearched;
      }
    }
  }
}

/*va appliquer l'action "action" dans "tab" (le questionnaire) */
function apply_action(tab, action) {
  //Action d'ajout d'élément directement à une page, aucune récursion nécessaire
  if (action.type == actionTypes.ADD_OBJECT_TO_PAGE) {
    tab[action.id].content.push(action.payload);
    return;
  }

  /*On recherche le chemin menant à l'élément où on veut faire l'action*/
  let path = pathOfElement(tab, action.id);
  console.log("ENTIRE PATH : " + path);
  console.log(JSON.stringify(tab, null, 1));

  /*Je fais cette ligne car j'ai besoin des premiers niveaux du path pour modif la pos order*/
  if (action.type == actionTypes.MODIFY_ORDER_POSITION && path.length == 2) {
    let path_piece = path.shift();
    action.pos = path.shift();
    make_action(tab[path_piece].content, action);
    /**On passe ici pour les autres types d'action */
  } else {
    let path_piece = path.shift();
    iter(tab[path_piece].content, path, action);
  }
}

/*Fonction qui itère jusqu'à trouver l'endroit où éxécuter l'action*/
function iter(tab, path, action) {
  let path_piece = path.shift();

  /*POUR CHANGER l'ordre des éléments, on a besoin du parent... Ne pas descendre trop bas*/
  if (path.length == 1 && action.type == actionTypes.MODIFY_ORDER_POSITION) {
    action.pos = path.shift();
    make_action(tab[path_piece].content, action);
    return tab;
  }

  //Une fois position de l'élément atteinte, on réalise l'action 
  if (path.length == 0) {
    make_action(tab[path_piece], action);
    return tab;
  }
  //On récurse tant que position dans l'arbre souhaité non atteinte
  iter(tab[path_piece].content, path, action);
}

/*Permet de réaliser l'action passé en param à l'objet passé en param*/
function make_action(object, action) {
  switch (action.type) {

    case actionTypes.MODIFY_OBJECT:
      object = action.payload;
      return object;

    case actionTypes.ADD_OBJECT_TO_GROUP:
      object.content.push(action.payload);
      return object;
      

    case actionTypes.MODIFY_ORDER_POSITION:
      //dispatch({ type: "MODIFY_ORDER_POSITION", id: nodeId, way: way });
      let target;
      
      //On cherche l'objet dont on veut changer l'attribut pos
      for(let i = 0; i<object.length; i++){
        if(object[i].id[0] == action.id)
          target = object[i];
      }
      
      if (action.way == "up" && action.pos > 0) {
        action.pageIndex;
      }
      if (action.way == "down" && action.pos < object.length - 1) {
      }
      return object;

    default:
      console.error("\n ERROR ! Action inconnu \n");
      return undefined;
  }
}

const SurveyReducer = (state, action) => {
  let new_state;

  switch (action.type) {
    /*Tous ces cases utilisent la même action*/
    case actionTypes.ADD_OBJECT_TO_GROUP:
    case actionTypes.ADD_OBJECT_TO_PAGE:
    case actionTypes.MODIFY_ORDER_POSITION:
    case actionTypes.MODIFY_OBJECT:
      console.log("BBBBBBBBBBBBBBB");
      apply_action(state.surveyStruct, action);
      return state;
      
    case actionTypes.ADD_PAGE:
      new_state = { ...state };
      console.log("ADD_PAGE ACTION "+JSON.stringify(action))
      new_state.surveyStruct.push({id: [action.id[0],action.id[1]],content: []}); //TODO: Remplacer par ID page renvoyé par API !!!
      
      return new_state;

    case actionTypes.MODIFY_CURRENT_SELECTED_ID:
      new_state = { ...state };
      new_state.currentSelectedItemId = action.payload.id;
      return new_state;

    case actionTypes.RESET_SURVEY:
      new_state = { currentSelectedItemId: "666",
          survey_id:"xxxxxx", //TODO:MODIF par ID d'un survey API
          surveyStruct: [
        ]};
        console.log("SURVEY RESET");
        return new_state;
      
    case actionTypes.MODIFY_SELECTED_SURVEY:
      new_state = { ...state };
      new_state.selected_survey_id = action.id_selected;
      return new_state;

    case actionTypes.CREATE_SURVEY:
      console.log("CREATE_SURVEY REDUCER waaaaaaaaa")
      new_state = { ...state };
      new_state.survey_id = action.id_surv;
      /*new_state = {
        currentSelectedItemId: "666",
        survey_id: action.id_surv, 
        surveyStruct:[],
      }*/
      console.log("CREATE_SURVEY REDUCER "+action.id_surv)
      //new_state.survey_id = action.survey_id;
      console.log("CREATE_SURVEY REDUCER new-state "+ JSON.stringify(new_state))
      return new_state

    default:
      console.error("ACTION INCONNU : "+action.type);
      return state;
  }

};

export default SurveyReducer;
