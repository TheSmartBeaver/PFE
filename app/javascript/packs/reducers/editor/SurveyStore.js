import React, { createContext, useReducer } from "react";
import SurveyReducer from "./SurveyReducer";

const initialStateNewSurvey = {
  currentSelectedItemId: "666",
  survey_id: "47", //TODO: Remplacer l'ID par celui renvoyé par l'API

  /* Tableaux d'elements des pages */
  surveyStruct: [
    {
      id: ["page 0", "xxxx"],
      content: [
        {
          id: ["1", "xxxx"],
          type: "texte",
          content: { text: "Questionnaire sur les chats" },
        },
        {
          id: ["2", "xxxx"],
          type: "question textuelle",
          content: { text: "Votre nom ?", answer: null },
        },
        {
          id: ["3", "xxxx"],
          type: "groupe",
          content: [
            /*Tableau des éléments du groupe*/
            {
              id: ["4", "xxxx"],
              type: "question radio",
              content: {
                text: "Combien de chats ?",
                options: [0, 1, 2, 3],
                answer: null,
              },
            },
          ],
        },
      ],
    },
    {
      id: ["page 1", "xxxx"],
      content: [
        {
          id: ["5", "xxxx"],
          type: "question textuelle",
          content: { text: "Couleur de vos chats ?", answer: null },
        },
      ],
    },
  ],
};

//POUR CREATION d'un nouveau questionnaire, le state ressemble à ça
const initialState = {
  currentSelectedItemId: "Rien",
  survey_id: "47", //TODO: Remplacer l'ID par celui renvoyé par l'API
  surveyStruct: /* Tableaux d'elements des pages */[],
}

const testCovid = {
  selected_survey_id: 0,
  surveys: [
    {
      //Questionnaire 1
      survey_id: "1",
      survey_title: "Questionnaire sur les chats",
      number_of_questions: 4,
      survey_questions: [
        //questions
        {
          id: "1",
          type: "question textuelle",
          content: { text: "Votre nom ?", answer: null },
        },

        {
          id: "2",
          type: "groupe",
          content: [
            //groupe de questions
            {
              id: "3",
              type: "question radio",
              content: {
                text: "Combien avez-vous de chats ?",
                options: ["0", "1", "2", "3+"],
                answer: null,
              },
            },

            {
              id: "4",
              type: "question textuelle",
              content: {
                text:
                  "Quelle(s) est(sont) la(les) couleur(s) de votre(vos) chat(s) ?",
                answer: null,
              },
            },
          ],
        },

        {
          id: "5",
          type: "question radio",
          content: {
            text: "Combien avez-vous de chiens ?",
            options: ["0", "1", "2", "3+"],
            answer: null,
          },
        },
      ],
    },

    {
      //Questionnaire 2
      survey_id: "2",
      survey_title: "Questionnaire sur les habitudes alimentaires",
      number_of_questions: 2,
      survey_questions: [
        //questions
        {
          id: "6",
          type: "question textuelle",
          content: { text: "Votre nom ?", answer: null },
        },
        {
          id: "7",
          type: "groupe",
          content: [
            {
              id: "8",
              type: "question radio",
              content: {
                text: "Buvez-vous du café ?",
                options: ["Oui", "Non"],
                answer: null,
              },
            },

            {
              id: "9",
              type: "question radio",
              content: {
                text: "Combien mettez-vous de sucre ?",
                options: ["0", "0,5", "1", "1,5"],
              },
            },
          ],
        },
      ],
    },
  ],
};

const initialState2 = {
  currentSelectedItemId: "666",
  survey_id: "1", //TODO: Remplacer l'ID par celui renvoyé par l'API
  surveyStruct: /* Tableaux d'elements des pages */[
    {
      id: "page 0", //ID de la PAGE
      content: [
        {
          id: "1",
          type: "texte",
          content: { text: "Questionnaire sur les chats" },
        },
        {
          id: "2",
          type: "question textuelle",
          content: { text: "Votre nom ?", answer: null },
        },
        {
          id: "3",
          type: "groupe",
          content: [
            /*Tableau des éléments du groupe*/
            {
              id: "4",
              type: "question radio",
              content: {
                text: "Combien de chats ?",
                options: [0, 1, 2, 3],
                answer: null,
              },
            },
            {
              id: "5",
              type: "question textuelle",
              content: { text: "Couleur de vos chats ?", answer: null },
            },
            {
              id: "11",
              type: "groupe",
              content: [
                /*Tableau des éléments du sous-groupes*/
                {
                  id: "12",
                  type: "question radio",
                  content: {
                    text: "Combien de chameaux ?",
                    options: [0, 1, 2, 3],
                    answer: null,
                  },
                },
                {
                  id: "13",
                  type: "question textuelle",
                  content: { text: "Couleur de vos chameaux ?", answer: null },
                },
              ],
            },
          ],
        },
      ],
    },
    {
      id: "page 1",
      content: [
        {
          id: "6",
          type: "texte",
          content: { text: "Questionnaire sur les chiens" },
        },
        {
          id: "7",
          type: "question textuelle",
          content: { text: "Votre nom ?", answer: null },
        },
        {
          id: "8",
          type: "groupe",
          content: [
            {
              id: "9",
              type: "question radio",
              content: {
                text: "Combien de chiens ?",
                options: [0, 1, 2, 3, 4],
                answer: null,
              },
            },
            {
              id: "10",
              type: "question textuelle",
              content: { text: "Couleur de vos chiens ?", answer: null },
            },
          ],
        },
      ],
    },
  ],
};

/* Permet de partager un même state aux enfants*/
const SurveyStore = ({ children }) => {
  const [state, dispatch] = useReducer(SurveyReducer, initialState); //CHANGER ICI !!!!!!!!!!!!!!!!!
  return (
    <SurveyContext.Provider value={[state, dispatch]}>
      {children}
    </SurveyContext.Provider>
  );
};

export const SurveyContext = createContext(initialState); // CHANGER ICI AUSSI !!!!!!!!!!!!!!!!!!
// export const SurveyContext = createContext(initialState);// CHANGER ICI AUSSI !!!!!!!!!!!!!!!!!!
export default SurveyStore;
