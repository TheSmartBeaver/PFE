import * as elementTypes from "../constants/ElementTypes";
import * as actionTypes from "../constants/ActionTypes";
import axios from "axios";

//Ce fichier contiendra les actions qui modifie à la fois sur le front et le survey sur le front

export function modifyObject() {}

export function putPosOnApi(id_element, order) {
  axios
    .put(
      "/api/v1/survey/edit/element/element_place/" +
        id_element,
      { showWithGroup: false, order: order }
    )
    .then((response) => {
      console.log(
        "Réussi à modifier pos sur API " + JSON.stringify(response.data)
      );
    })
    .catch((error) => {
      console.log(error);
    });
}

export async function postQuestionTarget(question_id) {
  console.log("On post pour obtenir target question " + question_id);
  return await axios({
    method: "post",
    url: "/api/v1/survey/edit/condition/target/question",
    data: { question_id: question_id },
  })
    .then((response) => {
      console.log(
        "Réussi à obtenir ID target question " + JSON.stringify(response.data)
      );
      return response.data.target.id;
    })
    .catch((error) => {
      console.log(error);
    });
}

export async function postConstTarget(const_val) {
  console.log("On post pour obtenir target const " + const_val);
  //let payload = { const: const_val };
  return await axios({
    method: "post",
    url: "/api/v1/survey/edit/condition/target/const",
    data: { const: const_val },
  })
    .then((response) => {
      console.log(
        "Réussi à obtenir ID target const " + JSON.stringify(response.data)
      );
      return response.data.target.id;
    })
    .catch((error) => {
      console.log(error);
    });
}

export async function postBasicCondition(id1, id2, op, isNot) {
  return await axios({
    method: "post",
    url: "/api/v1/survey/edit/condition/basic/",
    data: {
      target1_id: id1,
      target2_id: id2,
      isNot: isNot,
      comparisonType: op,
    },
  })
    .then((response) => {
      console.log(
        "Réussi à obtenir ID basic condition " + JSON.stringify(response.data)
      );
      return [response.data.condition.id, response.data.condition.childId];
    })
    .catch((error) => {
      console.log(error);
    });
}

export async function postComplexCondition(operator) {
  return await axios({
    method: "post",
    url: "/api/v1/survey/edit/condition/complex/",
    data: { complexType: operator },
  })
    .then((response) => {
      console.log(
        "Réussi à obtenir ID complex condition " + JSON.stringify(response.data)
      );
      return [response.data.condition.id, response.data.condition.childId];
    })
    .catch((error) => {
      console.log(error);
    });
}

export async function putBasicJoinComplexCondition(id_basic, id_complex) {
  await axios
    .put(
      "/api/v1/survey/edit/condition/basic/joinComplexCondition/" +
        id_basic,
      { complex_parent: { complex_condition_id: id_complex } }
    )
    .then((response) => {
      console.log("Réussi à join complex " + JSON.stringify(response.data));
    })
    .catch((error) => {
      console.log(error);
    });
}

export async function putComplexJoinComplexCondition(
  id_complex,
  id_complex_parent
) {
  await axios
    .put(
      "/api/v1/survey/edit/condition/complex/joinComplexCondition/" +
        id_complex,
      { complex_parent: { complex_condition_id: id_complex_parent } }
    )
    .then((response) => {
      console.log("Réussi à join complex " + JSON.stringify(response.data));
    })
    .catch((error) => {
      console.log(error);
    });
}

export async function putSetAsRoot(id_condition, id_caller) {
  await axios
    .put(
      "/api/v1/survey/edit/condition/complex/setAsRoot/" +
        id_condition,
      { element_attributes: { id: id_caller } }
    )
    .then((response) => {
      console.log("Réussi à setRoot " + JSON.stringify(response.data));
    })
    .catch((error) => {
      console.log(error);
    });
}

export async function postReply(question_id, answer) {
      console.log("postReplyyyyyyy "+question_id +" "+ answer)
      await axios({
        method: "post",
        url: "/api/v1/survey/reply/replyToQuestion/"+question_id,
        data: { response_data: answer }
        //{response_data: answer}
      })
      .then((response) => {
        console.log(
          "Réussi à replyToQuestion avec le replier "+ JSON.stringify(response.data)
        );
      })
      .catch((error) => {
        console.log(error);
        console.log(error.data);
      console.log(error.status);
      console.log(error.headers);
      console.log(error.request);
      console.log('Error', error.message);
      });
  console.log(
    "On reply à la question d'ID " +
      question_id +
      " avec la réponse " +
      JSON.stringify(answer)
  );
}


export async function postEndReply() {
  await axios({
    method: "post",
    url: "/api/v1/survey/edit/reply/endReply/",
  })
    .then((response) => {
      console.log("End reply survey" + JSON.stringify(response.data));
    })
    .catch((error) => {
      console.log(error);
    });
}

export async function askCanShowElement(id_element){
  return await axios({
    method: "get",
    url: "/api/v1/survey/reply/canShowElement/"+ id_element
  })
    .then((response) => {
      console.log("Peut-on afficher ??" + JSON.stringify(response.data));
      return response.data;
      //TODO: il faut renvoyer si on peut afficher ou non
    })
    .catch((error) => {
      console.log(error);
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
      console.log(error.request);
      console.log('Error', error.message);
    });
}

export async function startReply(id_survey){
  return await axios({
    method: "post",
    url: "/api/v1/survey/reply/startReply/"+ id_survey
  })
    .then((response) => {
      console.log("Start Reply " + JSON.stringify(response));
      return
      //TODO: il faut renvoyer si on peut afficher ou non
    })
    .catch((error) => {
      console.log(error);
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
      console.log(error.request);
      console.log('Error', error.message);
    });
}

export async function postSetSurveyPublic(id_survey){
  return await axios({
    method: "post",
    url: "/api/v1/survey/edit/setPublic/"+ id_survey
  })
    .then((response) => {
      console.log("Survey Public " + JSON.stringify(response));
      //TODO: il faut renvoyer si on peut afficher ou non
    })
    .catch((error) => {
      console.log(error);
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
      console.log(error.request);
      console.log('Error', error.message);
    });
}

export async function postCreateSurvey(surveyName){
  return await axios({
    method: "post",
    url: "/api/v1/survey/edit/survey/",
    data: {name: ("Survey "+surveyName)}
  })
    .then((response) => {
      console.log("Survey Create " + JSON.stringify(response));
      return response.data.id;
      //TODO: il faut renvoyer si on peut afficher ou non
    })
    .catch((error) => {
      console.log(error);
    });
}

export async function postStartEditSurvey(id_survey){
  return await axios({
    method: "post",
    url: "/api/v1/survey/edit/startEdit/"+id_survey
  })
    .then((response) => {
      console.log("Survey Edit" + JSON.stringify(response));
      //TODO: il faut renvoyer si on peut afficher ou non
    })
    .catch((error) => {
      console.log(error);
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
      console.log(error.request);
      console.log('Error', error.message);
    });
}

export async function getSurveyReply() {
  console.log("on récup le survey");
  let survey;
  return await axios
    .get(
      "/api/v1/survey/reply/show/?full=true&expendable=true"
    )
    .then((response) => {
      survey = response.data;
      //setSurveyState(survey);
      console.log(
        "Réussi à récup questionnaire " + JSON.stringify(response.data, null, 1)
      );
      return survey;
    })
    .catch((error) => {
      console.log(error);
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
      console.log(error.request);
      console.log('Error', error.message);
    });
}
///reply/nextPageOrder/:idDeTaPremièrePage

export async function nextPageOrder(id_page) {
  console.log("nextPageOrder ");
  let survey;
  return await axios
    .get(
      "/api/v1/survey/reply/nextPageOrder/"+id_page+"?full=true&expendable=true"
    )
    .then((response) => {
      survey = response.data;
      //setSurveyState(survey);
      console.log(
        "Réussi à nextPageOrder " + JSON.stringify(response.data)
      );
      return survey;
    })
    .catch((error) => {
      console.log(error);
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
      console.log(error.request);
      console.log('Error', error.message);
    });
}

export async function countSurvey(){

  return await axios
    .get("/api/v1/survey/edit/editables")
    .then(response => {
      console.log(response)
      return response.data.length;
    })
    .catch(error => {
      console.log(error);
    });
}

export async function getShareLink(id_survey){
  return await axios({
    method: "get",
    url: "/api/v1/survey/edit/generateLink/"+ id_survey
  })
    .then((response) => {
      console.log("generate link" + JSON.stringify(response.data));
      return response.data;
      //TODO: il faut renvoyer si on peut afficher ou non
    })
    .catch((error) => {
      console.log(error);
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
      console.log(error.request);
      console.log('Error', error.message);
    });
}

export async function getPropertiesOfQuestion(id_question){
  return await axios({
    method: "get",
    url: "/api/v1/survey/edit/element/show/"+ id_question+"/?full=true&expendable=true"
  })
    .then((response) => {
      console.log("GET QUESTION" + JSON.stringify(response.data));
      return response.data;
      //TODO: il faut renvoyer si on peut afficher ou non
    })
    .catch((error) => {
      console.log(error);
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
      console.log(error.request);
      console.log('Error', error.message);
    });
}