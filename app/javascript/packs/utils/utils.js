import * as elementTypes from "../constants/ElementTypes";
import * as actionTypes from "../constants/ActionTypes";

//Cette fonction cherche tous les objets de page actuelle et précédentes et les mets dans result !
export function getElementsOfCurrentPageAndPrevious(tab, pageIndex) {
  let result = []; //Résultat final : contiendra tous les éléments de chaque page
  let result2;
  for (let i = 0; i < pageIndex + 1; i++) {
    result2 = [];
    //On push tous les éléments d'une page
    result.push(iter3(tab[i].content, result2));
  }
  return result;
}

export function getVisibleElementsInPage(tab, pageIndex){
  console.log("getVisibleElementsInPage "+pageIndex+ " __ "+JSON.stringify(tab))
  let result = []; //Résultat final : contiendra tous les éléments de chaque page
  let result2;
    result2 = [];
    result.push(iter5(tab[pageIndex].content, result2));
  return result;
}

export function getPageOfElementOfId(tab, id) {
  let result;
  console.log("On va recherché une page d'élément hbuhezbu "+id+" "+JSON.stringify(tab))
  for (let i = 0; i < tab.length; i++) {
    result = iter4(tab[i].content, id);
    if (result == true){
      console.log("L'élément "+id+" appartient à indexPage "+i);
      return i;
    }
  }
}

//Cette fonction permet d'itérer pour récupérer tous les éléments d'une page
function iter3(tab, result) {
  for (let i = 0; i < tab.length; i++) {
    result.push(tab[i]);
    if (tab[i].type == "groupe") {
      iter3(tab[i].content, result);
    }
  }
  return result;
}

//Cette fonction permet d'itérer pour chercher page d'un élément
function iter4(tab, searched) {
  for (let i = 0; i < tab.length; i++) {
    console.log("page sssss" + tab[i].id[0] );
    if (tab[i].id[0] == searched) {
      console.log("OK")
      return true;
    }

    if (tab[i].type == "groupe") {
      if(iter4(tab[i].content, searched) == true)
        return true;
    }
  }
}

function iter5(tab, result) {
  for (let i = 0; i < tab.length; i++) {
    if (tab[i].type == "groupe") {
      iter3(tab[i].content, result);
    }
    else result.push(tab[i]);
  }
  return result;
}

function iter6(tab, searched) {
  for (let i = 0; i < tab.length; i++) {
    if (tab[i].pos == searched) {
      return tab[i];
    }
    if (tab[i].type == elementTypes.GROUP) {
      let indexSearched = iter6(tab[i].content, searched);
      if (indexSearched != undefined) {
        return indexSearched;
      }
    }
  }
}

export function get_object_at_pos_in_page(tab, pageIndex, pos_searched) {
    let result = iter6(tab[pageIndex].content, pos_searched);
    if (result != undefined) {
      return result;
    }
}

