class BasicCondition < ApplicationRecord
    has_one :condition, as: :conditionable, inverse_of: :conditionable
    belongs_to :target1, class_name: "Target", foreign_key: "target1_id"
    belongs_to :target2, class_name: "Target", foreign_key: "target2_id"

    accepts_nested_attributes_for :condition, :allow_destroy => true

    def as_json(options = {})
      if options["expendable"]
        {
          :target1 => self.target1.as_json(options),
          :target2 => self.target2.as_json(options),
          :comparisonType => comparisonType,
          :condition => self.condition.as_json_from_child(options),
        }
      else
        {
          :target1 => self.target1.id,
          :target2 => self.target2.id,
          :comparisonType => comparisonType,
          :condition => self.condition.as_json_from_child(options),
        }
      end
    end

    def as_json_from_parent(options = {})
      if options["expendable"]
        {
          :target1 => self.target1.as_json(options),
          :target2 => self.target2.as_json(options),
          :comparisonType => comparisonType,
        }
      else
        {
          :target1 => self.target1.id,
          :target2 => self.target2.id,
          :comparisonType => comparisonType,
        }
      end
    end

    def testCondition(replier, notShowedAreTrue = false)
      data1 = getTargetData(target1, replier)
      data2 = getTargetData(target2, replier)
      #check nil, empty but skip check false
      if data1.nil? || data2.nil?
        if notShowedAreTrue
          return false
        else
          if(not_showed_test(target1, replier))
            return true
          elsif (not_showed_test(target2, replier))
            return true
          else
            return false
          end
        end
      end
      if data1.kind_of?(Array) && data2.kind_of?(Array)
        #Check if they have the same count of data
        if data1.length != data2.length
          return false
        end
        results = []
        for val1 in data1 do
          tempTest = false
          for val2 in data2 do
            if(comparison(val1, val2))
              tempTest = true
              break
            end
          end
          results.push(tempTest)
        end
        #If one equality is false, the condition can't be true
        for result in results do
          unless result
            return false
          end
        end
        return true
      elsif data1.kind_of?(Array)
        for val1 in data1 do
          if comparison(val1, data2)
            return true
          end
        end
      elsif data2.kind_of?(Array)
        for val2 in data2 do
          if comparison(data1, val2)
            return true
          end
        end
      else
        return comparison(data1, data2)
      end
    end

    def comparison(val1, val2)
      case comparisonType
      when "equal"
        return val1 == val2
      when "gt"
        return val1 > val2
      when "lt"
        return val1 < val2
      else
        return false
      end
    end

    private
      def not_showed_test(target, replier)
        if(target.targetable_type == "QuestionTarget")
          return !ApplicationController.helpers.can_show_element(target.question.element, replier, false)
        else
          return false
        end
      end

      def getTargetData(target, replier)
        case target.targetable_type
        when "ConstTarget"
          return target.targetable.get_data(replier)
        when "QuestionTarget"
          response = target.targetable.get_data(replier)
          question = response.question
          if(question.getQuestionData.can_link_with_response)
            return response.getResponseData.linkWithQuestion(question.getQuestionData)
          else
            return response.getResponseData.response
          end
        else
          raise NotImplementedError, "No other target types implemented" 
        end
      end
end
