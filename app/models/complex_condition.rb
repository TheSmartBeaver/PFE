class ComplexCondition < ApplicationRecord
    has_one :condition, as: :conditionable, inverse_of: :conditionable
    has_many :conditions

    accepts_nested_attributes_for :condition, :allow_destroy => true

    def as_json(options = {})
        if options["expendable"]
          {
            :complexType => complexType,
            :condition => self.condition.as_json_from_child(options),
            :conditions => self.conditions.collect { |n| n.as_json(options) }
          }
        else
          {
            :complexType => complexType,
            :condition => self.condition.as_json_from_child(options),
            :conditions => self.conditions.collect { |n| n.id }
          }
        end
      end
  
      def as_json_from_parent(options = {})
        if options["expendable"]
          {
            :complexType => complexType,
            :conditions => self.conditions.collect { |n| n.as_json(options) }
          }
        else
          {
            :complexType => complexType,
            :conditions => self.conditions.collect { |n| n.id }
          }
        end
      end

      def testCondition(replier, notShowedAreTrue = false)
        case complexType
        when "and"
          for condition in conditions do
            unless condition.conditionable.testCondition(replier, notShowedAreTrue)
              return false
            end
          end
          return true
        when "or"
          for condition in conditions do
            if condition.conditionable.testCondition(replier, notShowedAreTrue)
              return true
            end
          end
          return false
        when "nand"
          for condition in conditions do
            if condition.conditionable.testCondition(replier, notShowedAreTrue)
              return false
            end
          end
          return true
        when "nor"
          for condition in conditions do
            unless condition.conditionable.testCondition(replier, notShowedAreTrue)
              return true
            end
          end
          return false
        else
            return false
        end
      end
end
