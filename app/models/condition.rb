class Condition < ApplicationRecord
    has_one :element, class_name: "Element", foreign_key: "condition_id"
    belongs_to :conditionable, polymorphic: true, dependent: :destroy

    def as_json(options = {})
      if options["expendable"]
        {
          :id => id,
          :childType => conditionable_type,
          :childId => conditionable_id,
          :treeParentId => complex_condition_id,
          :isRoot => isRoot,
          :element => optionalLinkedElement(element, options),
          :childCondition => self.conditionable.as_json_from_parent(options),
        }
      else
        {
          :id => id,
          :childType => conditionable_type,
          :treeParentId => complex_condition_id,
          :isRoot => isRoot,
          :element => optionalLinkedElement(element, options),
          :childId => self.conditionable.as_json_from_parent(options),
        }
      end
    end
    
      def as_json_from_child(options = {})
        if options["expendable"]
            {
              :id => id,
              :childType => conditionable_type,
              :treeParentId => complex_condition_id,
              :isRoot => isRoot,
              :element => optionalLinkedElement(element, options),
              :childId => conditionable_id,
            }
          else
            {
              :id => id,
              :childType => conditionable_type,
              :treeParentId => complex_condition_id,
              :isRoot => isRoot,
              :element => optionalLinkedElement(element, options),
              :childId => conditionable_id,
            }
          end
      end

      def optionalLinkedElement(element, options = {})
        if options["expendable"]
          element != nil ? element.as_json(options) : {}
        else
          element != nil ? element.id : {}
        end
      end

      def testCondition(replierId)
        raise NotImplementedError, "not implemented for Condition parent"
      end
end
