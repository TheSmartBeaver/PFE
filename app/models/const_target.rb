class ConstTarget < ApplicationRecord
    has_one :target, as: :targetable, inverse_of: :targetable

    accepts_nested_attributes_for :target, :allow_destroy => true

      def as_json(options = {})
            {
                  :const => const,
                  :target => self.target.as_json_from_child(options),
            }
      end
      
      def as_json_from_parent(options = {})
            {
                  :const => const,
            }
      end

      def get_data(replier = nil)
            return const
      end
end
