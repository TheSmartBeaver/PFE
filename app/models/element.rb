class Element < ApplicationRecord
  belongs_to :expendableElement, polymorphic: true, dependent: :destroy
  belongs_to :elementPlace, dependent: :destroy
  belongs_to :condition, optional: true, dependent: :destroy
  belongs_to :element_group, optional: true

  def as_json(options = {})
    if options["expendable"]
      {
        :id => id,
        :inGroup => element_group_id,
        :condition => condition_id,
        :elementPlace => self.elementPlace { |n| n.as_json(options) },
        :title => title,
        :childType => expendableElement_type,
        :childId => expendableElement_id,
        :childElement => self.expendableElement.as_json_from_parent(options),
      }
    else
      {
        :id => id,
        :inGroup => element_group_id,
        :condition => condition_id,
        :elementPlace => elementPlace_id,
        :title => title,
        :childType => expendableElement_type,
        :childId => expendableElement_id,
      }
    end
  end

  def as_json_from_child(options = {})
    if options["expendable"]
      {
        :id => id,
        :inGroup => element_group_id,
        :condition => condition_id,
        :title => title,
        :elementPlace => self.elementPlace { |n| n.as_json(options) },
        :childType => expendableElement_type,
        :childId => expendableElement_id,
      }
    else
      {
        :id => id,
        :inGroup => element_group_id,
        :condition => condition_id,
        :title => title,
        :elementPlace => elementPlace_id,
        :childType => expendableElement_type,
        :childId => expendableElement_id,
      }
    end
  end
end
