class ElementGroup < ApplicationRecord
    has_one :element, as: :expendableElement, inverse_of: :expendableElement
    has_many :elements, dependent: :destroy
    belongs_to :survey

    accepts_nested_attributes_for :element, :allow_destroy => true

    def as_json(options = {})
      if options["full"]
        {
          :isPage => isPage,
          :survey_id => survey.id,
          :element => self.element.as_json_from_child(options),
          :elements => self.elements.collect { |n| n.as_json(options) },
        }
      else
        {
          :isPage => isPage,
          :survey_id => survey.id,
          :element => self.element.as_json_from_child(options),
          :elements => self.elements.collect { |n| n.id },
        }
      end
    end

    def as_json_from_parent(options = {})
      if options["full"]
        {
          :isPage => isPage,
          :survey_id => survey.id,
          :elements => self.elements.collect { |n| n.as_json(options) },
        }
      else
        {
          :isPage => isPage,
          :survey_id => survey.id,
          :elements => self.elements.collect { |n| n.id },
        }
      end
    end
end
