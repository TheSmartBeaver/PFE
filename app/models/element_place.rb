class ElementPlace < ApplicationRecord
    has_one :element

    def as_json(options = {})
        {
          :id => id,
          :order => order,
          :showWithGroup => showWithGroup,
        }
    end
end