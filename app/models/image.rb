class Image < ApplicationRecord
    has_one :element, as: :expendableElement, inverse_of: :expendableElement

    accepts_nested_attributes_for :element, :allow_destroy => true

    def as_json(options = {})
      if options["full"]
        {
          :url => url,
          :alt => alt,
        }
      else
        {
          :id => self.element.id,
          :type => self.element.expendableElement_type,
          :typeId => id,
          :inGroup => self.element.element_group_id,
          :condition => self.elementcondition_id,
        }
      end
    end

    def as_json(options = {})
      if options["full"]
        {
          :url => url,
          :alt => alt,
          :element => self.element.as_json_from_child(options),
        }
      else
        {
          :element => self.element.as_json_from_child(options),
        }
      end
    end

    def as_json_from_parent(options = {})
      if options["full"]
        {
          :url => url,
          :alt => alt,
        }
      else
        {
        }
      end
    end
end
