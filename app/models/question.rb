class Question < ApplicationRecord
  
  has_one :element, as: :expendableElement, inverse_of: :expendableElement
  has_many :responses
  has_many :question_targets
  
  accepts_nested_attributes_for :element, :allow_destroy => true

  def as_json(options = {})
    if options["full"]
      {
        :questionType => questionType,
        :properties => self.getQuestionData,
        :element => self.element.as_json_from_child(options),
        :responses => self.responses.collect { |n| n.id },
      }
    else
      {
        :questionType => questionType,
        :element => self.element.as_json_from_child(options),
      }
    end
  end

  def as_json_from_parent(options = {})
    if options["full"]
      {
        :questionType => questionType,
        :properties => self.getQuestionData,
      }
    else
      {
      }
    end
  end

  def getQuestionData
    return ApplicationController.helpers.getQuestionData(id, questionType)
  end

  def get_response_of_replier(replier)
    response = responses.where(:replier_id => replier.id).first
    if response.blank?
      return nil
    end
    return response
  end
end