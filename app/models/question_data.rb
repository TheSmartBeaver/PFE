class QuestionData
    attr_accessor :dataKey, :title, :description

    def initialize(dataKey)
        @dataKey = dataKey
        load_if_exists()
    end

    def as_json(options={})
        {
            title: @title,
            description: @description,
            parameters: {
            }
        }
    end

    def to_json(*options)
        as_json(*options).to_json(*options)
    end

    def load_if_exists
        redis = Redis.new
        if redis.exists?(@dataKey)
            load_from_base()
            return 1
        else
            return 0
        end
    end

    def load_from_base
        redis = Redis.new
        data = redis.get(@dataKey)
        
        hash = JSON.parse data
        @title = hash["title"]
        @description = hash["description"]
    end

    def save_to_base
        redis = Redis.new
        redis.set(@dataKey, self.to_json)
    end

    def remove_from_base
        redis = Redis.new
        redis.del(@dataKey)
    end

    def addChoice(choice)
        raise "This method is not implemented for #{self.class}"
    end

    def removeChoice(index)
        raise "This method is not implemented for #{self.class}"
    end

    def setMaxCheck(maxCheck)
        raise "This method is not implemented for #{self.class}"
    end

    def setMinCheck(minCheck)
        raise "This method is not implemented for #{self.class}"
    end

    def can_link_with_response
        return false
    end

    def getChoices
        raise "This method is not implemented for #{self.class}"
    end
end