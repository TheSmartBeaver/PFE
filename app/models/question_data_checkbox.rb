class QuestionDataCheckbox < QuestionData
    attr_accessor :minCheck, :maxCheck, :choices

    def initialize(dataKey)
        @choices = []
        @minCheck = 0
        @maxCheck = 1
        super(dataKey)
    end

    def as_json(options={})
        {
            title: @title,
            description: @description,
            parameters: {
                minCheck: @minCheck,
                maxCheck: @maxCheck,
                choices: @choices
            }
        }
    end

    def addChoice(choice)
        @choices.push(choice)
        self.save_to_base
    end

    def removeChoice(index)
        @choices.delete_at(index)
        checkMaxCheck(@maxCheck)
        self.save_to_base
    end

    def setMaxCheck(maxCheck)
        checkMaxCheck(maxCheck)
        @maxCheck = maxCheck
        self.save_to_base
    end

    def setMinCheck(minCheck)
        checkMinCheck(minCheck)
        @minCheck = minCheck
        self.save_to_base
    end

    def checkMinCheck(minCheck)
        if minCheck < 0
            raise "minCheck must be >= 0"
        elsif minCheck > @maxCheck
            raise "minCheck must be <= maxCheck"
        end
        self.save_to_base
    end

    def checkMaxCheck(maxCheck)
        if maxCheck < @minCheck
            raise "maxCheck must be >= minCheck"
        elsif maxCheck > @choices.length
            raise "maxCheck must be inferior or equal to choice number"
        end
        self.save_to_base
    end

    def load_from_base
        redis = Redis.new
        data = redis.get(@dataKey)
        
        hash = JSON.parse data
        @title = hash["title"]
        @description = hash["description"]
        @minCheck = hash["parameters"]["minCheck"]
        @maxCheck = hash["parameters"]["maxCheck"]
        @choices = []
        if (hash["parameters"]["choices"] != nil)
            hash["parameters"]["choices"].each do |n| 
                addChoice(n)
            end
        end
    end

    def getChoices
        return choices
    end

    def can_link_with_response
        return true
    end
end