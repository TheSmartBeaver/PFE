class QuestionDataRadio < QuestionDataCheckbox
    def initialize(dataKey)
        super(dataKey)
    end

    def setMaxCheck(maxCheck)
        raise "It is impossible to edit maxCheck of a radio question."
    end

    def setMinCheck(minCheck)
        if(minCheck < 0 || minCheck > 1)
            raise "It is impossible to set a minCheck different of 0 or 1 for a radio question."
        else
            @minCheck = minCheck
        end
        self.save_to_base
    end

    def getChoices
        return choices
    end

    def can_link_with_response
        return true
    end
end