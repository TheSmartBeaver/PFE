class QuestionTarget < ApplicationRecord
    has_one :target, as: :targetable, inverse_of: :targetable
    belongs_to :question

    accepts_nested_attributes_for :target, :allow_destroy => true

    def as_json(options = {})
        if options["expendable"]
            {
                :question => self.question.as_json(options),
                :target => self.target.as_json_from_child(options),
            }
          else
            {
                :question => self.question.id,
                :target => self.target.as_json_from_child(options),
            }
          end
      end
  
      def as_json_from_parent(options = {})
        if options["expendable"]
            {
                :question => self.question { |n| n.as_json(options) },
            }
          else
            {
                :question => self.question.id,
            }
          end
      end

      def get_data(replier)
        response = question.responses.where(:replier_id => replier.id).first
        if response.blank?
          return nil
        end
        return response
      end
end
