class Replier < ApplicationRecord
    belongs_to :account, optional: true
    belongs_to :survey
    has_many :responses, dependent: :destroy
end
