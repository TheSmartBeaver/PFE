class Response < ApplicationRecord
    belongs_to :question, class_name: "Question", foreign_key: "question_id"
    belongs_to :replier, class_name: "Replier", foreign_key: "replier_id"

    def as_json(options = {})
        if options["full"]
          {
            :question => self.question.as_json(options),
            :replier => self.replier.as_json(options),
          }
        else
          {
            :question => question_id,
            :replier => replier_id,
          }
        end
      end

    def getResponseData
        return ApplicationController.helpers.getResponseData(id, question.questionType)
    end
end
