class ResponseData
    attr_accessor :dataKey, :response

    def initialize(dataKey)
        @dataKey = dataKey
        load_if_exists()
    end

    def as_json(options={})
        {
            response: @response
        }
    end

    def to_json(*options)
        as_json(*options).to_json(*options)
    end

    def load_if_exists
        redis = Redis.new
        if redis.exists?(@dataKey)
            load_from_base()
            return 1
        else
            return 0
        end
    end

    def load_from_base
        redis = Redis.new
        data = redis.get(@dataKey)
        
        hash = JSON.parse data
        @response = hash["response"]
    end

    def save_to_base
        redis = Redis.new
        redis.set(@dataKey, self.to_json)
    end

    def add_response(response)
        @response = response
        save_to_base()
    end

    def remove_response(responseIndex = 0)
        @response = nil
        save_to_base()
    end

    def remove_from_base
        redis = Redis.new
        redis.del(@dataKey)
    end

    def response_is_empty
        return response.blank?
    end

    def linkWithQuestion(questionData)
        raise Exception, "This response data doesn't have to be linked with Question"
    end
end