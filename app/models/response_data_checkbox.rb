class ResponseDataCheckbox < ResponseData
    attr_accessor :responses

    def initialize(dataKey)
        @responses = []
        super(dataKey)
    end

    def as_json(options={})
        {
            responses: @responses
        }
    end

    def add_response(response)
        if response.kind_of?(Integer)
            @responses.push(response)
            save_to_base()
        end
    end

    def remove_response(responseIndex)
        if responseIndex.kind_of?(Integer)
            @responses.delete(responseIndex)
            save_to_base()
        end
    end

    def load_from_base
        redis = Redis.new
        data = redis.get(@dataKey)
        
        hash = JSON.parse data
        @responses = []
        if (hash["responses"] != nil)
            hash["responses"].each do |n| 
                add_response(n)
            end
        end
    end

    def response_is_empty
        return responses.blank?
    end

    def linkWithQuestion(questionData)
        formatedResponse = []
        if @responses.empty? || questionData.getChoices.empty?
            return nil
        end
        for response in @responses do
            formatedResponse.push(questionData.getChoices[response])
        end
        return formatedResponse
    end
end