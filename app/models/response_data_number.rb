class ResponseDataNumber < ResponseData

    def initialize(dataKey)
        super(dataKey)
    end

    def add_response(response)
        if response.kind_of?(Integer)
            @response = response
            save_to_base()
        end
    end
end