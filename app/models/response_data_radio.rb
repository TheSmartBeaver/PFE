class ResponseDataRadio < ResponseData

    def initialize(dataKey)
        super(dataKey)
    end

    def add_response(response)
        if response.kind_of?(Integer)
            @response = response
            save_to_base()
        end
    end

    def linkWithQuestion(questionData)
        if @response == nil || questionData.getChoices.empty?
            return nil
        end
        return questionData.getChoices[@response]
    end
end