class Survey < ApplicationRecord
    has_many :elementGroups, dependent: :destroy
    has_many :repliers, dependent: :destroy

    def as_json(options = {})
        if options["expendable"]
            {
                :id => id,
                :name => name,
                :pages => self.elementGroups.select{|n| n.isPage}.collect { |n| n.as_json(options) },
            }
        else
            {
                :id => id,
                :name => name,
                :pages => self.elementGroups.select{|n| n.isPage}.collect { |n| n.id },
            }
        end
    end
end
