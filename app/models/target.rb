class Target < ApplicationRecord
    belongs_to :targetable, polymorphic: true, dependent: :destroy
    has_many :target1_basic_condition, class_name: "BasicCondition", foreign_key: "target1_id"
    has_many :target2_basic_condition, class_name: "BasicCondition", foreign_key: "target2_id"

    def as_json(options = {})
        if options["expendable"]
          {
            :id => id,
            :childType => targetable_type,
            :childId => targetable_id,
            :childTarget => self.targetable.as_json_from_parent(options),
          }
        else
          {
            :id => id,
            :childType => targetable_type,
            :childId => targetable_id,
          }
        end
      end
    
      def as_json_from_child(options = {})
        if options["expendable"]
            {
              :id => id,
              :childType => targetable_type,
              :childId => targetable_id,
            }
          else
            {
              :id => id,
              :childType => targetable_type,
              :childId => targetable_id,
            }
          end
      end

      def get_data(replier = nil)
        raise NotImplementedError, "not implemented for Target parent"
      end
end
