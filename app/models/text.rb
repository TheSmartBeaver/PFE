class Text < ApplicationRecord
    has_one :element, as: :expendableElement, inverse_of: :expendableElement

    accepts_nested_attributes_for :element, :allow_destroy => true

    def as_json(options = {})
      if options["full"]
        {
          :data => data,
          :element => self.element.as_json_from_child(options),
        }
      else
        {
          :element => self.element.as_json_from_child(options),
        }
      end
    end

    def as_json_from_parent(options = {})
      if options["full"]
        {
          :data => data,
        }
      else
        {
        }
      end
    end

end
