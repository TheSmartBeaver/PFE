Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  devise_for :accounts, :controllers => { :sessions => 'api/v1/accounts', :registrations => 'api/v1/signup'}

  authenticated :account do
    root :to => "page/v1/account#dashboard", :as => :authenticated_root
  end
  root 'page/v1/account#login'


  unauthenticated do
    namespace :page do
      namespace :v1 do
        scope :account do
          get "/login", to: "account#login"
          get "/signup", to: "account#signup"
          get "/dashboard", to: "account#login"
        end
        scope :survey do
          get "/show", to: "survey#show"
          # Can't edit without authentication -> redirect to login page
          get "/edit", to: "account#login"
        end
      end
    end
  end

  authenticated :account do
    namespace :page do
      namespace :v1 do
        scope :account do
          # No need to access login and signup page while authenticated -> redirect to panel page
          get "/login", to: "account#dashboard"
          get "/signup", to: "account#dashboard"
          get "/dashboard", to: "account#dashboard"
        end
        scope :survey do
          get "/edit", to: "survey#edit"
          get "/show", to: "survey#reply"
        end
      end
    end
  end

  #TESTS
  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :accounts, only: [:create, :destroy]
      devise_scope :account do
        post 'login', action: :create, controller: 'accounts'
        delete 'logout', action: :destroy, controller: 'accounts'
        post 'signup', action: :create, controller: 'signup'
      end

      scope :survey do

        scope :dashboard do
          scope :stats do
            get 'getReplierOnQuestion/:id', action: :getReplierOnQuestion, controller: 'stats'
            get 'getReponseOccOnQuestion/:id', action: :getReponseOccOnQuestion, controller: 'stats'
            get 'getReplierCount', action: :getReplierCount, controller: 'stats'
            get 'getEndedReplierCount', action: :getEndedReplierCount, controller: 'stats'
          end
        end

        scope :edit do
          resources :survey, only: [:create, :update, :destroy]
          get 'editables', action: :editableSurveys, controller: 'survey'
          get 'generateLink/:id', action: :generateLink, controller: 'survey'
          get 'show', action: :showEditSurvey, controller: 'survey'
          post 'startEdit/:id', action: :startEditSurvey, controller: 'survey'
          post 'endEdit', action: :endEditSurvey, controller: 'survey'
          post 'setSurveyPrivate/:id', action: :setSurveyPrivate, controller: 'survey'
          post 'setPublic/:id', action: :setPublic, controller: 'survey'


          
          namespace :element do

            resources :element_place, only: [:show, :update]

            resources :question, only: [:show, :create, :update, :destroy]
            scope :question do
              put "joinGroup/:id", action: :joinGroup, controller: "question"
              put "leaveGroup/:id", action: :leaveGroup, controller: "question"
            end

            namespace :question do
              scope :properties do
                get "show/:id", action: :show, controller: "properties"
                post "editTitle/:id", action: :editTitle, controller: "properties"
                post "editDescription/:id", action: :editDescription, controller: "properties"
                scope :params do
                  post "editMaxCheck/:id", action: :editMaxCheck, controller: "properties"
                  post "editMinCheck/:id", action: :editMinCheck, controller: "properties"
                  post "addChoice/:id", action: :addChoice, controller: "properties"
                  post "removeChoice/:id", action: :removeChoice, controller: "properties"
                end
              end
            end

            resources :group, only: [:show, :create, :update, :destroy]
            scope :group do
              put "joinGroup/:id", action: :joinGroup, controller: "group"
              put "leaveGroup/:id", action: :leaveGroup, controller: "group"
            end

            resources :text, only: [:show, :create, :update, :destroy]
            scope :text do
              put "joinGroup/:id", action: :joinGroup, controller: "text"
              put "leaveGroup/:id", action: :leaveGroup, controller: "text"
            end

            resources :image, only: [:show, :create, :update, :destroy]
            scope :image do
              put "joinGroup/:id", action: :joinGroup, controller: "image"
              put "leaveGroup/:id", action: :leaveGroup, controller: "image"
            end
          end

          namespace :condition do
            resources :basic , only: [:show, :create, :update, :destroy], controller: "basic_condition"
            scope :basic do
              put "joinComplexCondition/:id", action: :joinComplexCondition, controller: "basic_condition"
              put "leaveComplexCondition/:id", action: :leaveComplexCondition, controller: "basic_condition"
              put "setAsRoot/:id", action: :setAsRoot, controller: "basic_condition"
              put "unroot/:id", action: :unroot, controller: "basic_condition"
            end

            resources :complex , only: [:show, :create, :update, :destroy], controller: "complex_condition"
            scope :complex do
              put "joinComplexCondition/:id", action: :joinComplexCondition, controller: "complex_condition"
              put "leaveComplexCondition/:id", action: :leaveComplexCondition, controller: "complex_condition"
              put "setAsRoot/:id", action: :setAsRoot, controller: "complex_condition"
              put "unroot/:id", action: :unroot, controller: "complex_condition"
            end

            namespace :target do
              resources :const , only: [:show, :create, :update, :destroy], controller: "const_target"
              resources :question , only: [:show, :create, :update, :destroy], controller: "question_target"
            end
          end
        end

        scope :reply do
          get 'replyables', action: :replyableSurveys, controller: 'survey'
          post 'startReply/:id', action: :startReplySurvey, controller: 'survey'
          post 'pauseReply', action: :pauseReplySurvey, controller: 'survey'
          post 'endReply', action: :endReplySurvey, controller: 'survey'
        end

        namespace :reply do

          get 'showElement/:id', action: :showElement, controller: 'replying'
          get 'canShowElement/:id', action: :canShowElement, controller: 'replying'
          get 'getPercentage/:id', action: :getPercentage, controller: 'replying'
          get 'show', action: :show, controller: 'replying'
          post 'replyToQuestion/:id', action: :replyToQuestion, controller: 'replying'
          post 'removeReplyToQuestion/:id', action: :removeReplyToQuestion, controller: 'replying'
          post 'nextPageOrder/:id', action: :nextPageOrder, controller: 'replying'
          post 'canGoNextPage/:id', action: :canGoNextPage, controller: 'replying'

          resources :replier , only: [:show, :create, :update, :destroy], controller: "replier"
          resources :response , only: [:show, :create, :update, :destroy], controller: "response"
          namespace :response do
            scope :properties do
              get 'show/:id', action: :show, controller: 'properties'
              post 'addResponse/:id', action: :addResponse, controller: 'properties'
              post 'removeResponse/:id', action: :removeResponse, controller: 'properties'
            end
          end
        end
      end
    end
  end
  mount Sidekiq::Web => "/sidekiq" if defined?(Sidekiq) && defined?(Sidekiq::Web)
  mount LetterOpenerWeb::Engine, at: "/letter_opener" if Rails.env.development?

  #match ':controller(/:action(/:id')), :via => :get

end
