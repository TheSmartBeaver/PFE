class CreateSurveys < ActiveRecord::Migration[6.1]
  def change
    create_table :surveys do |t|
      t.belongs_to :account, index: true, foreign_key: true
      t.string :name
      t.boolean :editable, default: true
      t.boolean :isPublic, default: false

      t.timestamps
    end
  end
end
