class CreateRepliers < ActiveRecord::Migration[6.1]
  def change
    create_table :repliers do |t|
      t.belongs_to :account, foreign_key: true
      t.belongs_to :survey, foreign_key: true
      t.boolean :ended, default: false

      t.timestamps
    end
  end
end
