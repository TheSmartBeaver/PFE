class CreateConditions < ActiveRecord::Migration[6.1]
  def change
    create_table :conditions do |t|
      t.references :conditionable, polymorphic: true
      t.boolean :isRoot, :default => false

      t.timestamps
    end
  end
end
