class CreateElementPlaces < ActiveRecord::Migration[6.1]
  def change
    create_table :element_places do |t|
      t.boolean :showWithGroup
      t.integer :order

      t.timestamps
    end
  end
end
