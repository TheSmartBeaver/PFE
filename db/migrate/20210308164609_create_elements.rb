class CreateElements < ActiveRecord::Migration[6.1]
  def change
    create_table :elements do |t|
      t.references :expendableElement, polymorphic: true
      t.belongs_to :condition, class_name: "Condition", foreign_key: "condition_id"
      t.belongs_to :elementPlace, class_name: "ElementPlace", index: { unique: true }, foreign_key: {to_table: :element_places}

      t.string :title

      t.timestamps
    end
  end
end
