class CreateElementGroups < ActiveRecord::Migration[6.1]
  def change
    create_table :element_groups do |t|
      t.boolean :isPage
      t.belongs_to :survey, index: true, foreign_key: true, null: true

      t.timestamps
    end
  end
end
