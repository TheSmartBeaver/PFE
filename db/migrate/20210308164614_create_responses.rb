class CreateResponses < ActiveRecord::Migration[6.1]
  def change
    create_table :responses do |t|
      t.belongs_to :question, index: true, foreign_key: true
      t.belongs_to :replier, index: true, foreign_key: true

      t.timestamps
    end
  end
end
