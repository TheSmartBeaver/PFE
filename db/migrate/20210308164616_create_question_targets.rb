class CreateQuestionTargets < ActiveRecord::Migration[6.1]
  def change
    create_table :question_targets do |t|
      t.belongs_to :question, index: true, foreign_key: true
      
      t.timestamps
    end
  end
end
