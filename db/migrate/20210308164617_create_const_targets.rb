class CreateConstTargets < ActiveRecord::Migration[6.1]
  def change
    create_table :const_targets do |t|
      t.string :const

      t.timestamps
    end
  end
end
