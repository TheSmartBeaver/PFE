class CreateBasicConditions < ActiveRecord::Migration[6.1]
  def change
    create_table :basic_conditions do |t|
      t.references :target1, null: false
      t.references :target2, null: false
      
      t.string :comparisonType

      t.timestamps
    end
  end
end
