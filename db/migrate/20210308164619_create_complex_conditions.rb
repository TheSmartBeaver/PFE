class CreateComplexConditions < ActiveRecord::Migration[6.1]
  def change
    create_table :complex_conditions do |t|
      t.string :complexType

      t.timestamps
    end
  end
end
