class AddElementGroupToElements < ActiveRecord::Migration[6.1]
  def change
    add_belongs_to :elements, :element_group, class_name:"ElementGroup", null: true, foreign_key: {to_table: :element_groups}
  end
end
