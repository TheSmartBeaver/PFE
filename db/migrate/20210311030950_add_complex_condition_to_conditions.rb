class AddComplexConditionToConditions < ActiveRecord::Migration[6.1]
  def change
    add_reference :conditions, :complex_condition, null: true, foreign_key: {to_table: :complex_conditions}
  end
end
