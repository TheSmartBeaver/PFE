# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_03_14_180805) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.string "email"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "authentication_token", limit: 30
    t.index ["authentication_token"], name: "index_accounts_on_authentication_token", unique: true
    t.index ["email"], name: "index_accounts_on_email", unique: true
    t.index ["reset_password_token"], name: "index_accounts_on_reset_password_token", unique: true
  end

  create_table "basic_conditions", force: :cascade do |t|
    t.bigint "target1_id", null: false
    t.bigint "target2_id", null: false
    t.string "comparisonType"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["target1_id"], name: "index_basic_conditions_on_target1_id"
    t.index ["target2_id"], name: "index_basic_conditions_on_target2_id"
  end

  create_table "complex_conditions", force: :cascade do |t|
    t.string "complexType"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "conditions", force: :cascade do |t|
    t.string "conditionable_type"
    t.bigint "conditionable_id"
    t.boolean "isRoot", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "complex_condition_id"
    t.index ["complex_condition_id"], name: "index_conditions_on_complex_condition_id"
    t.index ["conditionable_type", "conditionable_id"], name: "index_conditions_on_conditionable"
  end

  create_table "const_targets", force: :cascade do |t|
    t.string "const"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "element_groups", force: :cascade do |t|
    t.boolean "isPage"
    t.bigint "survey_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["survey_id"], name: "index_element_groups_on_survey_id"
  end

  create_table "element_places", force: :cascade do |t|
    t.boolean "showWithGroup"
    t.integer "order"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "elements", force: :cascade do |t|
    t.string "expendableElement_type"
    t.bigint "expendableElement_id"
    t.bigint "condition_id"
    t.bigint "elementPlace_id"
    t.string "title"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "element_group_id"
    t.index ["condition_id"], name: "index_elements_on_condition_id"
    t.index ["elementPlace_id"], name: "index_elements_on_elementPlace_id", unique: true
    t.index ["element_group_id"], name: "index_elements_on_element_group_id"
    t.index ["expendableElement_type", "expendableElement_id"], name: "index_elements_on_expendableElement"
  end

  create_table "images", force: :cascade do |t|
    t.string "url"
    t.string "alt"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "question_targets", force: :cascade do |t|
    t.bigint "question_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["question_id"], name: "index_question_targets_on_question_id"
  end

  create_table "questions", force: :cascade do |t|
    t.string "questionType"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "repliers", force: :cascade do |t|
    t.bigint "account_id"
    t.bigint "survey_id"
    t.boolean "ended", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["account_id"], name: "index_repliers_on_account_id"
    t.index ["survey_id"], name: "index_repliers_on_survey_id"
  end

  create_table "responses", force: :cascade do |t|
    t.bigint "question_id"
    t.bigint "replier_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["question_id"], name: "index_responses_on_question_id"
    t.index ["replier_id"], name: "index_responses_on_replier_id"
  end

  create_table "surveys", force: :cascade do |t|
    t.bigint "account_id"
    t.string "name"
    t.boolean "editable", default: true
    t.boolean "isPublic", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["account_id"], name: "index_surveys_on_account_id"
  end

  create_table "targets", force: :cascade do |t|
    t.string "targetable_type"
    t.bigint "targetable_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["targetable_type", "targetable_id"], name: "index_targets_on_targetable"
  end

  create_table "texts", force: :cascade do |t|
    t.string "data"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "conditions", "complex_conditions"
  add_foreign_key "element_groups", "surveys"
  add_foreign_key "elements", "conditions"
  add_foreign_key "elements", "element_groups"
  add_foreign_key "elements", "element_places", column: "elementPlace_id"
  add_foreign_key "question_targets", "questions"
  add_foreign_key "repliers", "accounts"
  add_foreign_key "repliers", "surveys"
  add_foreign_key "responses", "questions"
  add_foreign_key "responses", "repliers"
  add_foreign_key "surveys", "accounts"
end
