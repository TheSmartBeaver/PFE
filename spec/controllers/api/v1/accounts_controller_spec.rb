require "rails_helper"

RSpec.describe Api::V1::AccountsController, type: :controller do
    login_account
    
    # OLD
    # describe "GET index -" do
    #     it "return a 200" do
    #         get :index
    #         expect(response).to have_http_status(:ok)
    #     end
    # end

    describe "POST create -" do
        it "return a 200" do
            post :create, :params => {:account=>{:email=>"test@test.test", :password=>"password"}}
            expect(response).to have_http_status(:found)
        end
    end

    describe "DELETE destroy -" do
        it "return a 200" do
            account = create(:account)
            delete :destroy, :params => {:id=>account.id}
            expect(response).to have_http_status(:found)
        end
    end
end