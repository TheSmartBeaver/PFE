require "rails_helper"
include ConditionHelper

RSpec.describe Api::V1::Condition::BasicConditionController, type: :controller do
    login_account

    before(:each) do
        @account = create(:account)
        @survey = create(:survey)
        @survey.update(account_id:@account.id)
        @survey.update(editable:true)
        @survey.save
    end
    
    # describe "GET index -" do
    #     it "return a 200" do
    #         get :index
    #         expect(response).to have_http_status(:ok)
    #     end
    # end

    describe "GET show -" do
        it "return a 200" do
            condition = create(:condition, :basic)
            get :show, :params => {:id=>condition.conditionable.id, :full=>true, :expendable=>true}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "POST create -" do
        it "return a 200" do
            target1 = create(:target, :const)
            target2 = create(:target, :const)
            post :create, :params => {:basic_condition => {:target1_id=>target1.id, :target2_id=>target2.id, :comparisonType=>"equal"}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "PUT update -" do
        it "return a 200" do
            condition = create(:condition, :basic)
            target1 = create(:target, :const)
            target2 = create(:target, :const)
            put :update, :params => {:id=>condition.conditionable.id, :basic_condition => {:target1_id=>target1.id, :target2_id=>target2.id, :comparisonType=>"equal"}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "DELETE destroy -" do
        it "return a 200" do
            condition = create(:condition, :basic)
            delete :destroy, :params => {:id=>condition.conditionable.id}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "PUT joinComplexCondition -" do
        it "return a 200" do
            basic = create(:condition, :basic)
            cplx = create(:condition, :complex)
            put :joinComplexCondition, :params => {:id=>basic.conditionable.id, :complex_parent=>{:complex_condition_id=>cplx.conditionable.id}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end
    
    describe "PUT leaveComplexCondition -" do
        it "return a 200" do
            basic = create(:condition, :basic)
            cplx = create(:condition, :complex)
            ConditionHelper.joinComplexCondition(basic.conditionable.condition, cplx.conditionable)
            put :leaveComplexCondition, :params => {:id=>basic.conditionable.id}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "PUT setAsRoot -" do
        it "return a 200" do
            basic = create(:condition, :basic)
            element = create(:element)
            put :setAsRoot, :params => {:id=>basic.conditionable.id, :element_attributes=>{:id=>element.id}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "PUT unroot -" do
        it "return a 200" do
            basic = create(:condition, :basic)
            element = create(:element)
            basic.update(:isRoot => true)
            element.update(:condition_id => basic.id)
            put :unroot, :params => {:id=>basic.conditionable.id, :element_attributes=>{:id=>element.id}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400" do
            basic = create(:condition, :basic)
            element = create(:element)
            put :unroot, :params => {:id=>basic.conditionable.id, :element_attributes=>{:id=>element.id}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end
end