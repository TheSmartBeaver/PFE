require "rails_helper"

RSpec.describe Api::V1::Condition::ComplexConditionController, type: :controller do
    login_account

    before(:each) do
        @account = create(:account)
        @survey = create(:survey)
        @survey.update(account_id:@account.id)
        @survey.update(editable:true)
        @survey.save
    end
    
    # describe "GET index -" do
    #     it "return a 200" do
    #         get :index
    #         expect(response).to have_http_status(:ok)
    #     end
    # end

    describe "GET show -" do
        it "return a 200" do
            condition = create(:condition, :complex)
            get :show, :params => {:id=>condition.conditionable.id, :full=>true, :expendable=>true}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "POST create -" do
        it "return a 200" do
            post :create, :params => {:complex_condition => {:complexType=>"or"}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "PUT update -" do
        it "return a 200" do
            condition = create(:condition, :complex)
            put :update, :params => {:id=>condition.conditionable.id, :complex_condition => {:complexType=>"and"}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "DELETE destroy -" do
        it "return a 200" do
            condition = create(:condition, :complex)
            delete :destroy, :params => {:id=>condition.conditionable.id}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "PUT joinComplexCondition -" do
        it "return a 200" do
            cplx1 = create(:condition, :complex)
            cplx2 = create(:condition, :complex)
            put :joinComplexCondition, :params => {:id=>cplx1.conditionable.id, :complex_parent=>{:complex_condition_id=>cplx2.conditionable.id}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end
    

    describe "PUT leaveComplexCondition -" do
        it "return a 200" do
            cplx1 = create(:condition, :complex)
            cplx2 = create(:condition, :complex)
            ConditionHelper.joinComplexCondition(cplx1.conditionable.condition, cplx2.conditionable)
            put :leaveComplexCondition, :params => {:id=>cplx1.conditionable.id}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "PUT setAsRoot -" do
        it "return a 200" do
            cplx = create(:condition, :complex)
            element = create(:element)
            put :setAsRoot, :params => {:id=>cplx.conditionable.id, :element_attributes=>{:id=>element.id}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "PUT unroot -" do
        it "return a 200" do
            cplx = create(:condition, :complex)
            element = create(:element)
            cplx.update(:isRoot => true)
            element.update(:condition_id => cplx.id)
            put :unroot, :params => {:id=>cplx.conditionable.id, :element_attributes=>{:id=>element.id}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400" do
            cplx = create(:condition, :complex)
            element = create(:element)
            put :unroot, :params => {:id=>cplx.conditionable.id, :element_attributes=>{:id=>element.id}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end
end