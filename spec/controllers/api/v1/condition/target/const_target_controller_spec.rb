require "rails_helper"

RSpec.describe Api::V1::Condition::Target::ConstTargetController, type: :controller do
    login_account

    before(:each) do
        @account = create(:account)
        @survey = create(:survey)
        @survey.update(account_id:@account.id)
        @survey.update(editable:true)
        @survey.save
    end
    
    # describe "GET index -" do
    #     it "return a 200" do
    #         get :index
    #         expect(response).to have_http_status(:ok)
    #     end
    # end

    describe "GET show -" do
        it "return a 200" do
            target = create(:target, :const)
            get :show, :params => {:id=>target.targetable.id, :full=>true, :expendable=>true}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "POST create -" do
        it "return a 200" do
            post :create, :params => {:const_target=>{:const=>nil}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "POST update -" do
        it "return a 200" do
            target = create(:target, :const)
            post :update, :params => {:id=>target.targetable.id, :const_target=>{:const=>nil}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "DELETE destroy -" do
        it "return a 200" do
            target = create(:target, :const)
            delete :destroy, :params => {:id=>target.targetable.id}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end
end