require "rails_helper"

RSpec.describe Api::V1::Condition::Target::QuestionTargetController, type: :controller do
    login_account

    before(:each) do
        @account = create(:account)
        @survey = create(:survey)
        @survey.update(account_id:@account.id)
        @survey.update(editable:true)
        @survey.save
    end
    
    # describe "GET index -" do
    #     it "return a 200" do
    #         get :index
    #         expect(response).to have_http_status(:ok)
    #     end
    # end

    describe "GET show -" do
        it "return a 200" do
            target = create(:target, :question)
            get :show, :params => {:id=>target.targetable.id, :full=>true, :expendable=>true}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "POST create -" do
        it "return a 200" do
            q = create(:question, :text)
            post :create, :params => {:question_target=>{:question_id=>q.id}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "POST update -" do
        it "return a 200" do
            target = create(:target, :question)
            q = create(:question, :text)
            post :update, :params => {:id=>target.targetable.id, :question_target=>{:question_id=>q.id}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "DELETE destroy -" do
        it "return a 200" do
            target = create(:target, :question)
            delete :destroy, :params => {:id=>target.targetable.id}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

end