require "rails_helper"

RSpec.describe Api::V1::Element::ElementPlaceController, type: :controller do
    login_account

    before(:each) do
        @account = create(:account)
        @survey = create(:survey)
        @survey.update(account_id:@account.id)
        @survey.update(editable:true)
        @survey.save
    end
    
    describe "GET show -" do
        it "return a 200" do
            elem = create(:element)
            get :show, params:{id:elem.id, full:true, expendable:true}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "PUT update -" do
        it "return a 200" do
            elem = create(:element)
            put :update, params:{id:elem.id, full:true, expendable:true, element_place:{order:0, showWithGroup:false}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end
end