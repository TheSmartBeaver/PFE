require "rails_helper"

RSpec.describe Api::V1::Element::GroupController, type: :controller do
    login_account

    before(:each) do
        @account = create(:account)
        @survey = create(:survey)
        @survey.update(account_id:@account.id)
        @survey.update(editable:true)
        @survey.save
    end
    
    # describe "GET index -" do
    #     it "return a 200" do
    #         get :index
    #         expect(response).to have_http_status(:ok)
    #     end
    # end

    describe "GET show -" do
        it "returns error when looking for an unsaved text" do
            get :show, :params => {:id => -1, :full=>true, :expendable=>true}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:bad_request)
        end

        it "return a 200" do
            group = create(:element, :group)
            get :show, :params => {:id => group.expendableElement.id, :full=>true, :expendable=>true}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "POST create -" do
        it "return a 200" do
            post :create, :params => {:group=>{:isPage=>true, :survey_id=>create(:survey).id, :element_attributes=>{:title=>"TITLE"}}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "PUT update -" do
        it "return a 200" do
            group = create(:element, :group)
            put :update, :params => {:id=>group.expendableElement.id, :group=>{:isPage=>true, :survey_id=>group.expendableElement.survey.id}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400 Unable to get ElementGroup" do
            group = create(:element, :group)
            put :update, :params => {:id=>-1, :group=>{:isPage=>true, :survey_id=>group.expendableElement.survey.id}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "DELETE destroy" do
        it "return a 200" do
            group = create(:element, :group)
            delete :destroy, :params => {:id=>group.expendableElement.id}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400 Unable to get ElementGroup" do
            delete :destroy, :params => {:id=>-1}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "PUT joinGroup" do
        it "return a 200" do
            group = create(:element, :group)
            group2 = create(:element, :group)
            group2.expendableElement.update(survey:group.expendableElement.survey)
            put :joinGroup, :params => {:id=>group.expendableElement.id, :element=>{:element_group_id=>group2.expendableElement.id}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400" do
            group = create(:element, :group)
            group.expendableElement.update(isPage:true)
            group2 = create(:element, :group)
            group2.expendableElement.update(survey:group.expendableElement.survey)
            put :joinGroup, :params => {:id=>group.expendableElement.id, :element=>{:element_group_id=>group2.expendableElement.id}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:bad_request)
        end

        it "return a 400 Unable to get ElementGroup" do
            group = create(:element, :group)
            group.expendableElement.update(isPage:true)
            group2 = create(:element, :group)
            group2.expendableElement.update(survey:group.expendableElement.survey)
            put :joinGroup, :params => {:id=>-1, :element=>{:element_group_id=>group2.expendableElement.id}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "PUT leaveGroup" do
        it "return a 200" do
            group = create(:element, :group)
            group2 = create(:element, :group)
            group2.expendableElement.update(survey:group.expendableElement.survey)
            group.update(:element_group_id => group2.expendableElement.id)
            put :leaveGroup, :params => {:id=>group.expendableElement.id}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400" do
            group = create(:element, :group)
            group.expendableElement.update(isPage:true)
            group2 = create(:element, :group)
            group2.expendableElement.update(survey:group.expendableElement.survey)
            put :leaveGroup, :params => {:id=>group.expendableElement.id, :element=>{:element_group_id=>group2.expendableElement.id}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:bad_request)
        end

        it "return a 400 Unable to get ElementGroup" do
            group = create(:element, :group)
            group2 = create(:element, :group)
            group2.expendableElement.update(survey:group.expendableElement.survey)
            put :leaveGroup, :params => {:id=>-1, :element=>{:element_group_id=>group2.expendableElement.id}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end
end