require "rails_helper"

RSpec.describe Api::V1::Element::ImageController, type: :controller do
    login_account

    before(:each) do
        @account = create(:account)
        @survey = create(:survey)
        @survey.update(account_id:@account.id)
        @survey.update(editable:true)
        @survey.save
    end
    
    #describe "GET index -" do
    #    it "return a 200" do
    #        get :index
    #        expect(response).to have_http_status(:ok)
    #    end
    #end

    describe "GET show -" do
        it "returns error when looking for an unsaved text" do
            get :show, :params => {:id => -1, :full=>true, :expendable=>true}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:bad_request)
        end

        it "return a 200" do
            image = create(:element, :image)
            get :show, :params => {:id => image.expendableElement.id, :full=>true, :expendable=>true}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "POST create -" do
        it "return a 200" do
            post :create, :params => {:image=>{:url=>"URL", :alt=>"ALT", :element_attributes=>{:title=>"TITLE"}}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "PUT update -" do
        it "return a 200" do
            image = create(:element, :image)
            put :update, :params => {:id=>image.expendableElement.id, :image=>{:url=>"URL_EDIT", :alt=>"ALT_EDIT"}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400" do
            put :update, :params => {:id=>-1, :image=>{:url=>"URL_EDIT", :alt=>"ALT_EDIT"}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "DELETE destroy" do
        it "return a 200" do
            image = create(:element, :image)
            delete :destroy, :params => {:id=>image.expendableElement.id}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400" do
            delete :destroy, :params => {:id=>-1}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "PUT joinGroup" do
        it "return a 200" do
            image = create(:element, :image)
            group = create(:element, :group)

            put :joinGroup, :params => {:id=>image.expendableElement.id, :element=>{:element_group_id=>group.expendableElement.id}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400 no image" do
            group = create(:element, :group)

            put :joinGroup, :params => {:id=>-1, :element=>{:element_group_id=>group.expendableElement.id}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "PUT leaveGroup" do
        it "return a 200" do
            image = create(:image, :element)
            group = create(:element, :group)
            image.element.update(element_group_id:group.expendableElement.id)

            put :leaveGroup, :params => {:id=>image.id}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400 no image" do
            put :leaveGroup, :params => {:id=>-1}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end
end