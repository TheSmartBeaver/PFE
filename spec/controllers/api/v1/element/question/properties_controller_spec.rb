require "rails_helper"
include QuestionDataHelper

RSpec.describe Api::V1::Element::Question::PropertiesController, type: :controller do
    login_account

    before(:each) do
        @account = create(:account)
        @survey = create(:survey)
        @survey.update(account_id:@account.id)
        @survey.update(editable:true)
        @survey.save
    end
    
    describe "GET show -" do
        it "return a 200" do
            q = create(:question, :text)
            get :show, :params => {:id => q.id}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end

        # it "return a 400" do
        #     q = create(:question, :text)
        #     get :show, :params => {:id => q.id}
        #     expect(response).to have_http_status(:bad_request)
        # end
    end

    describe "POST editTitle -" do
        it "return a 200" do
            q = create(:question, :text)
            post :editTitle, :params => {:id => q.id, :title => "TITLE"}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "POST editDescription -" do
        it "return a 200" do
            q = create(:question, :text)
            post :editDescription, :params => {:id => q.id, :description => "DESCRIPTION"}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "POST editMaxCheck -" do
        it "return a 200" do
            q = create(:question, :checkbox)
            post :editMaxCheck, :params => {:id => q.id, :maxCheck => 0}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400" do
            q = create(:question, :text)
            post :editMaxCheck, :params => {:id => q.id, :maxCheck => 0}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "POST editMinCheck -" do
        it "return a 200" do
            q = create(:question, :checkbox)
            post :editMinCheck, :params => {:id => q.id, :minCheck => 0}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400" do
            q = create(:question, :text)
            post :editMinCheck, :params => {:id => q.id, :minCheck => 0}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "POST addChoice -" do
        it "return a 200" do
            q = create(:question, :checkbox)
            post :addChoice, :params => {:id => q.id, :choice => "CHOICE"}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "POST removeChoice -" do
        it "return a 200" do

            question = create(:question, :checkbox)

            properties = QuestionDataHelper.getQuestionData(question.id, question.questionType)
            properties.addChoice(2)
            properties.setMaxCheck(0)

            post :removeChoice, :params => {:id => question.id, :choiceId => 0}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end
end