require "rails_helper"

RSpec.describe Api::V1::Element::QuestionController, type: :controller do
    login_account

    before(:each) do
        @account = create(:account)
        @survey = create(:survey)
        @survey.update(account_id:@account.id)
        @survey.update(editable:true)
        @survey.save
    end
    
    # describe "GET index -" do
    #     it "return a 200" do
    #         get :index
    #         expect(response).to have_http_status(:ok)
    #     end
    # end

    describe "GET show -" do
        it "returns error when looking for an unsaved text" do
            get :show, :params => {:id => -1, :full=>true, :expendable=>true}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:bad_request)
        end

        it "return a 200" do
            question = create(:question, :element)
            get :show, :params => {:id => question.id, :full=>true, :expendable=>true}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "POST create -" do
        it "return a 200" do
            post :create, :params => {:question=>{:questionType=>"text", :element_attributes=>{:title=>"TITLE"}}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "PUT update data -" do
        it "return a 200" do
            question = create(:question, :element)
            put :update, :params => {:id=>question.id, :question=>{:questionType=>"number"}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400 Unable to get Question" do
            put :update, :params => {:id=>-1, :question=>{:questionType=>"number"}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "DELETE destroy" do
        it "return a 200" do
            question = create(:question, :element)
            delete :destroy, :params => {:id=>question.id}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400 Unable to get Question" do
            delete :destroy, :params => {:id=>-1}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "PUT joinGroup" do
        it "return a 200" do
            question = create(:question, :element)
            group = create(:element, :group)

            put :joinGroup, :params => {:id=>question.id, :element=>{:element_group_id=>group.expendableElement.id}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400 Unable to get Question" do
            group = create(:element, :group)

            put :joinGroup, :params => {:id=>-1, :element=>{:element_group_id=>group.expendableElement.id}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "PUT leaveGroup" do
        it "return a 200" do
            question = create(:question, :element)
            group = create(:element, :group)
            question.element.update(element_group_id:group.expendableElement.id)

            put :leaveGroup, :params => {:id=>question.id}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400 Unable to get Question" do
            group = create(:element, :group)

            put :leaveGroup, :params => {:id=>-1, :element=>{:element_group_id=>group.expendableElement.id}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end
end