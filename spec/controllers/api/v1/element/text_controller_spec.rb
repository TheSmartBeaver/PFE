require "rails_helper"

RSpec.describe Api::V1::Element::TextController, type: :controller do
    login_account

    before(:each) do
        @account = create(:account)
        @survey = create(:survey)
        @survey.update(account_id:@account.id)
        @survey.update(editable:true)
        @survey.save
    end
    
    #describe "GET index -" do
    #    it "return a 200" do
    #        get :index
    #        expect(response).to have_http_status(:ok)
    #    end
    #end

    describe "GET show -" do
        it "returns error when looking for an unsaved text" do
            get :show, :params => {:id => -1, :showParams=>{:full=>true,:expendable=>true}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:bad_request)
        end

        it "return a 200" do
            text = create(:element, :text)
            get :show, :params => {:id => text.expendableElement.id, :full=>true, :expendable=>true}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "POST create -" do
        it "return a 200" do
            post :create, :params => {:text=>{:data=>"DATA", :element_attributes=>{:title=>"TITLE"}}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "PUT update data -" do
        it "return a 200" do
            text = create(:element, :text)
            put :update, :params => {:id=>text.expendableElement.id, :text=>{:data=>"DATA_EDIT"}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400" do
            put :update, :params => {:id=>-1, :text=>{:data=>"DATA_EDIT"}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "DELETE destroy" do
        it "return a 200" do
            text = create(:element, :text)
            delete :destroy, :params => {:id=>text.expendableElement.id}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400" do
            delete :destroy, :params => {:id=>-1}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "PUT joinGroup" do
        it "return a 200" do
            text = create(:element, :text)
            group = create(:element, :group)

            put :joinGroup, :params => {:id=>text.expendableElement.id, :element=>{:element_group_id=>group.expendableElement.id}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400 no text" do
            group = create(:element, :group)

            put :joinGroup, :params => {:id=>-1, :element=>{:element_group_id=>group.expendableElement.id}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "PUT leaveGroup" do
        it "return a 200" do
            text = create(:text, :element)
            group = create(:element, :group)
            text.element.update(element_group_id:group.expendableElement.id)

            put :leaveGroup, :params => {:id=>text.id}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400 no text" do
            put :leaveGroup, :params => {:id=>-1}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end
end