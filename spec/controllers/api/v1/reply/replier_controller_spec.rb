require "rails_helper"

RSpec.describe Api::V1::Reply::ReplierController, type: :controller do
    login_account

    before(:each) do
        @account = create(:account)
        @survey = create(:survey)
        @survey.update(account_id:@account.id)
        @survey.update(editable:true)
        @survey.save
    end

    # describe "GET index -" do
    #     it "return a 200" do
    #         get :index
    #         expect(response).to have_http_status(:ok)
    #     end
    # end

    describe "GET show -" do
        it "return a 200" do
            replier = create(:replier)
            get :show, :params => {:id=>replier.id, :full=>true, :expendable=>true}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "POST create -" do
        it "return a 200" do
            account = create(:account)
            survey = create(:survey)
            post :create, :params => {:replier=>{:account_id=>account.id, :survey_id=>survey.id}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "PUT update -" do
        it "return a 200" do
            replier = create(:replier)
            account = create(:account)
            survey = create(:survey)
            put :update, :params => {:id=>replier.id, :replier=>{:account_id=>account.id, :survey_id=>survey.id}}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "DELETE destroy -" do
        it "return a 200" do
            replier = create(:replier)
            delete :destroy, :params => {:id=>replier.id}, session:{account_id:@account.id, edit_survey_id:@survey.id}
            expect(response).to have_http_status(:ok)
        end
    end
end