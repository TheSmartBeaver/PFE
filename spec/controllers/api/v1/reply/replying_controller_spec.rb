require "rails_helper"
include ResponseDataHelper

RSpec.describe Api::V1::Reply::ReplyingController, type: :controller do
    
    describe "GLOBAL" do
        it "return a 400 You can\'t reply on this survey" do
            element = create(:element, :group)
            replier = create(:replier)
            survey = create(:survey)
            get :showElement, params:{id:element.id}, session:{replier_id:replier.id, reply_survey_id:survey.id}
            expect(response).to have_http_status(:bad_request)
        end

        it "return a 400 You can\'t reply on this survey" do
            element = create(:element, :group)
            replier = create(:replier)
            get :showElement, params:{id:element.id}, session:{replier_id:-1, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:bad_request)
        end

        it "return a 400 You can\'t reply on this survey" do
            element = create(:element, :group)
            replier = create(:replier)
            get :showElement, params:{id:element.id}, session:{replier_id:replier.id, reply_survey_id:-1}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "GET showElement -" do
        it "return a 200" do
            replier = create(:replier)
            element = create(:element, :group)
            replier.survey = element.expendableElement.survey
            replier.save
            get :showElement, :params => {:id=>element.id, :full=>true, :expendable=>true}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400 This element is not in this survey." do
            replier = create(:replier)
            element = create(:element, :group)
            get :showElement, :params => {:id=>element.id, :full=>true, :expendable=>true}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:bad_request)
        end

        it "return a 400 Unable to get Element" do
            replier = create(:replier)
            get :showElement, :params => {:id=>-1, :full=>true, :expendable=>true}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "GET canShowElement -" do
        it "return a 200" do
            replier = create(:replier)
            element = create(:element, :group)
            replier.survey = element.expendableElement.survey
            replier.save
            get :canShowElement, :params => {:id=>element.id, :full=>true, :expendable=>true}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400 This element is not in this survey." do
            replier = create(:replier)
            element = create(:element, :group)
            get :canShowElement, :params => {:id=>element.id, :full=>true, :expendable=>true}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:bad_request)
        end

        it "return a 400 Unable to get Element" do
            replier = create(:replier)
            get :canShowElement, :params => {:id=>-1, :full=>true, :expendable=>true}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "GET canGoNextPage" do
        it "return a 200" do
            page = create(:element, :groupPage)

            question = create(:element, :questionOther)
            question.update(:element_group_id => page.expendableElement.id)
            question.save

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            rsp = create(:response, :other)
            rsp.question = question.expendableElement
            rsp.replier = replier
            rsp.save
            
            get :canGoNextPage, params:{id:page.expendableElement.id}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400 Unable to get Group" do
            replier = create(:replier)
            
            get :canGoNextPage, params:{id:-1}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:bad_request)
        end

        it "return a 400 This is not a page" do
            page = create(:element, :group)

            question = create(:element, :questionOther)
            question.update(:element_group_id => page.expendableElement.id)
            question.save

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            rsp = create(:response, :other)
            rsp.question = question.expendableElement
            rsp.replier = replier
            rsp.save
            
            get :canGoNextPage, params:{id:page.expendableElement.id}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:bad_request)
        end

        it "return a 400 This element is not in this survey." do
            page = create(:element, :groupPage)

            question = create(:element, :questionOther)
            question.update(:element_group_id => page.expendableElement.id)
            question.save

            replier = create(:replier)

            rsp = create(:response, :other)
            rsp.question = question.expendableElement
            rsp.replier = replier
            rsp.save
            
            get :canGoNextPage, params:{id:page.expendableElement.id}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "GET nextPageOrder" do
        it "return a 200" do
            page = create(:element, :groupPage)
            page.elementPlace.update(order:0)
            page.elementPlace.save

            question = create(:element, :questionOther)
            question.update(:element_group_id => page.expendableElement.id)
            question.save

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            rsp = create(:response, :other)
            rsp.question = question.expendableElement
            rsp.replier = replier
            rsp.save
            
            get :nextPageOrder, params:{id:page.expendableElement.id}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400 Unable to get Group" do
            replier = create(:replier)
            
            get :nextPageOrder, params:{id:-1}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:bad_request)
        end

        it "return a 400 This is not a page" do
            page = create(:element, :group)

            question = create(:element, :questionOther)
            question.update(:element_group_id => page.expendableElement.id)
            question.save

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            rsp = create(:response, :other)
            rsp.question = question.expendableElement
            rsp.replier = replier
            rsp.save
            
            get :nextPageOrder, params:{id:page.expendableElement.id}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:bad_request)
        end

        it "return a 400 This element is not in this survey." do
            page = create(:element, :groupPage)

            question = create(:element, :questionOther)
            question.update(:element_group_id => page.expendableElement.id)
            question.save

            replier = create(:replier)

            rsp = create(:response, :other)
            rsp.question = question.expendableElement
            rsp.replier = replier
            rsp.save
            
            get :nextPageOrder, params:{id:page.expendableElement.id}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "GET show -" do
        it "return a 200" do
            replier = create(:replier)
            get :show, :params => {:full=>true, :expendable=>true}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "GET getPercentage -" do
        it "return a 200" do
            elemGrp = create(:element_group)
            elemGrp.isPage=true
            elemGrp.save
            replier = create(:replier)
            get :getPercentage, :params => {id: elemGrp.id, :full=>true, :expendable=>true}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400 This is not a page" do
            elemGrp = create(:element_group)
            replier = create(:replier)
            get :getPercentage, :params => {id: elemGrp.id, :full=>true, :expendable=>true}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:bad_request)
        end

        it "return a 400 Page doesn\'t exist." do
            replier = create(:replier)
            get :getPercentage, :params => {id: -1, :full=>true, :expendable=>true}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "POST replyToQuestion -" do
        it "return a 200 with response" do
            page = create(:element, :groupPage)

            question = create(:element, :question)
            question.update(:element_group_id => page.expendableElement.id)
            question.save

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            rsp = create(:response, :text)
            rsp.question = question.expendableElement
            rsp.replier = replier
            rsp.save

            post :replyToQuestion, params:{id:question.expendableElement.id, response_data:"RESPONSE", response:{question_id:question.expendableElement.id, replier_id:replier.id}, full: true, expendable: true}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 200 without response" do
            page = create(:element, :groupPage)

            question = create(:element, :question)
            question.update(:element_group_id => page.expendableElement.id)
            question.save

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            post :replyToQuestion, params:{id:question.expendableElement.id, response_data:2, response:{question_id:question.expendableElement.id, replier_id:replier.id}, full: true, expendable: true}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400 with response" do
            page = create(:element, :groupPage)

            question = create(:element, :question)
            question.update(:element_group_id => page.expendableElement.id)
            question.save

            replier = create(:replier)

            rsp = create(:response, :text)
            rsp.question = question.expendableElement
            rsp.replier = replier
            rsp.save

            post :replyToQuestion, params:{id:question.expendableElement.id, response_data:"RESPONSE", response:{question_id:question.expendableElement.id, replier_id:replier.id}, full: true, expendable: true}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:bad_request)
        end

        it "return a 400 with response other" do
            page = create(:element, :groupPage)

            question = create(:element, :questionOther)
            question.update(:element_group_id => page.expendableElement.id)
            question.save

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            rsp = create(:response, :other)
            rsp.question = question.expendableElement
            rsp.replier = replier
            rsp.save

            post :replyToQuestion, params:{id:question.expendableElement.id, response_data:"RESPONSE", response:{question_id:question.expendableElement.id, replier_id:replier.id}, full: true, expendable: true}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "POST removeReplyToQuestion -" do
        it "return a 200 questionCheckbox" do
            page = create(:element, :groupPage)

            question = create(:element, :questionCheckbox)
            question.update(:element_group_id => page.expendableElement.id)
            question.save

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            rsp = create(:response, :checkbox)
            rsp.question = question.expendableElement
            rsp.replier = replier
            rsp.save

            properties = ResponseDataHelper.getResponseData(rsp.id, rsp.question.questionType)
            properties.add_response(2)

            post :removeReplyToQuestion, params:{id:question.expendableElement.id, response_data:0, response:{question_id:question.expendableElement.id, replier_id:replier.id}, full: true, expendable: true}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 200 questionText" do
            page = create(:element, :groupPage)

            question = create(:element, :question)
            question.update(:element_group_id => page.expendableElement.id)
            question.save

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            rsp = create(:response, :text)
            rsp.question = question.expendableElement
            rsp.replier = replier
            rsp.save

            properties = ResponseDataHelper.getResponseData(rsp.id, rsp.question.questionType)
            properties.add_response("RESPONSE")

            post :removeReplyToQuestion, params:{id:question.expendableElement.id, response_data:0, response:{question_id:question.expendableElement.id, replier_id:replier.id}, full: true, expendable: true}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400" do
            page = create(:element, :groupPage)

            question = create(:element, :questionCheckbox)
            question.update(:element_group_id => page.expendableElement.id)
            question.save

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            post :removeReplyToQuestion, params:{id:question.expendableElement.id, response_data:0, response:{question_id:question.expendableElement.id, replier_id:replier.id}, full: true, expendable: true}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:bad_request)
        end

        it "return a 400" do
            page = create(:element, :groupPage)

            question = create(:element, :questionCheckbox)

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            rsp = create(:response, :checkbox)
            rsp.question = question.expendableElement
            rsp.replier = replier
            rsp.save

            properties = ResponseDataHelper.getResponseData(rsp.id, rsp.question.questionType)
            properties.add_response(2)

            post :removeReplyToQuestion, params:{id:question.expendableElement.id, response_data:0, response:{question_id:question.expendableElement.id, replier_id:replier.id}, full: true, expendable: true}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:bad_request)
        end

        it "return a 400" do
            page = create(:element, :groupPage)

            question = create(:element, :questionOther)
            question.update(:element_group_id => page.expendableElement.id)
            question.save

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            rsp = create(:response, :other)
            rsp.question = question.expendableElement
            rsp.replier = replier
            rsp.save

            post :removeReplyToQuestion, params:{id:question.expendableElement.id, response_data:0, response:{question_id:question.expendableElement.id, replier_id:replier.id}, full: true, expendable: true}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end
end