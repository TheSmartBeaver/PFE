require "rails_helper"
include ResponseDataHelper

RSpec.describe Api::V1::Reply::Response::PropertiesController, type: :controller do

    describe "GLOBAL" do
        it "return a 400 You can\'t reply on this survey" do
            rsp = create(:response, :text)
            replier = create(:replier)
            survey = create(:survey)
            get :show, params:{id:rsp.id}, session:{replier_id:replier.id, reply_survey_id:survey.id}
            expect(response).to have_http_status(:bad_request)
        end

        it "return a 400 You can\'t reply on this survey" do
            rsp = create(:response, :text)
            replier = create(:replier)
            get :show, params:{id:rsp.id}, session:{replier_id:-1, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:bad_request)
        end

        it "return a 400 You can\'t reply on this survey" do
            rsp = create(:response, :text)
            replier = create(:replier)
            get :show, params:{id:rsp.id}, session:{replier_id:replier.id, reply_survey_id:-1}
            expect(response).to have_http_status(:bad_request)
        end
    end
  
    describe "GET show -" do
        it "return a 200" do
            rsp = create(:response, :text)
            replier = create(:replier)
            get :show, params:{id:rsp.id}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400 Unable to get Response" do
            replier = create(:replier)
            get :show, params:{id:-1}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "GET addResponse -" do
        it "return a 200" do
            page = create(:element, :groupPage)

            question = create(:element, :question)
            question.update(:element_group_id => page.expendableElement.id)
            question.save

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            rsp = create(:response, :text)
            rsp.question = question.expendableElement
            rsp.replier = replier
            rsp.save

            get :addResponse, params:{id:rsp.id, response:"RESPONSE"}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400 Unable to get Response" do
            replier = create(:replier)

            get :addResponse, params:{id:-1, response:"RESPONSE"}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:bad_request)
        end

        it "return a 400 This targeted question is not in allowed survey" do
            page = create(:element, :groupPage)

            question = create(:element, :question)
            question.update(:element_group_id => page.expendableElement.id)
            question.save

            replier = create(:replier)
            replier.save

            rsp = create(:response, :text)
            rsp.question = question.expendableElement
            rsp.replier = replier
            rsp.save

            get :addResponse, params:{id:rsp.id, response:"RESPONSE"}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "GET removeResponse -" do
        it "return a 200" do
            page = create(:element, :groupPage)

            question = create(:element, :questionCheckbox)
            question.update(:element_group_id => page.expendableElement.id)
            question.save

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            rsp = create(:response, :checkbox)
            rsp.question = question.expendableElement
            rsp.replier = replier
            rsp.save

            properties = ResponseDataHelper.getResponseData(rsp.id, rsp.question.questionType)
            properties.add_response(2)

            get :removeResponse, params:{id:rsp.id, response:0}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400 Unable to get Response" do
            replier = create(:replier)

            get :removeResponse, params:{id:-1, response:0}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:bad_request)
        end

        it "return a 400 This targeted question is not in allowed survey" do
            page = create(:element, :groupPage)

            question = create(:element, :question)
            question.update(:element_group_id => page.expendableElement.id)
            question.save

            replier = create(:replier)
            replier.save

            rsp = create(:response, :text)
            rsp.question = question.expendableElement
            rsp.replier = replier
            rsp.save

            get :removeResponse, params:{id:rsp.id, response:"RESPONSE"}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end
end