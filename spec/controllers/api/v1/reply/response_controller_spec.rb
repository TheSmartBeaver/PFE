require "rails_helper"

RSpec.describe Api::V1::Reply::ResponseController, type: :controller do
  
    # describe "GET index -" do
    #     it "return a 200" do
    #         get :index
    #         expect(response).to have_http_status(:ok)
    #     end
    # end

    describe "GET show -" do
        it "return a 200" do
            page = create(:element, :groupPage)

            question = create(:element, :question)
            question.update(:element_group_id => page.expendableElement.id)
            question.save

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            rsp = create(:response, :text)
            rsp.question = question.expendableElement
            rsp.replier = replier
            rsp.save

            get :show, :params => {:id=>rsp.id, :full=>true, :expendable=>true}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "POST create -" do
        it "return a 200" do
            question = create(:question)
            replier = create(:replier)
            post :create, :params => {:response=>{:question_id=>question.id, :replier_id=>replier.id}}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "PUT update -" do
        it "return a 200" do
            page = create(:element, :groupPage)

            question = create(:element, :question)
            question.update(:element_group_id => page.expendableElement.id)
            question.save

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            rsp = create(:response, :text)
            rsp.question = question.expendableElement
            rsp.replier = replier
            rsp.save

            put :update, :params => {:id=>rsp.id, :response=>{:question_id=>question.expendableElement.id, :replier_id=>replier.id}}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "DELETE destroy -" do
        it "return a 200" do
            rep = create(:response)
            replier = create(:replier)
            rep.replier = replier
            rep.save
            delete :destroy, :params => {:id=>rep.id}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:ok)
        end
    end
end