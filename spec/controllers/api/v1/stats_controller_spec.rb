require "rails_helper"

RSpec.describe Api::V1::StatsController, type: :controller do
    login_account

    describe "GET getReplierOnQuestion -" do
        it "return a 200" do
            account = create(:account)
            survey = create(:survey)
            survey.update(account_id:account.id)
            survey.update(editable:true)
            survey.save
            question = create(:question, :element)
            get :getReplierOnQuestion, params:{id:question.id}, session:{account_id:account.id, edit_survey_id:survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400" do
            account = create(:account)
            survey = create(:survey)
            survey.update(account_id:account.id)
            survey.update(editable:true)
            survey.save
            get :getReplierOnQuestion, params:{id:-1}, session:{account_id:account.id, edit_survey_id:survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "GET getReponseOccOnQuestion -" do
        it "return a 200" do
            account = create(:account)
            survey = create(:survey)
            survey.update(account_id:account.id)
            survey.update(editable:true)
            survey.save
            question = create(:question, :element)
            get :getReponseOccOnQuestion, params:{id:question.id}, session:{account_id:account.id, edit_survey_id:survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400" do
            account = create(:account)
            survey = create(:survey)
            survey.update(account_id:account.id)
            survey.update(editable:true)
            survey.save
            get :getReponseOccOnQuestion, params:{id:-1}, session:{account_id:account.id, edit_survey_id:survey.id}
            expect(response).to have_http_status(:bad_request)
        end
    end
end