require "rails_helper"

RSpec.describe Api::V1::SurveyController, type: :controller do
    login_account
  
    # describe "GET index -" do
    #     it "return a 200" do
    #         get :index
    #         expect(response).to have_http_status(:ok)
    #     end
    # end

    describe "GET editableSurveys -" do
        it "return a 200" do
            account = create(:account)
            get :editableSurveys, params: {account_id:account.id}, session: {account_id:account.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400" do
            account = create(:account)
            get :editableSurveys, params: {account_id:account.id}, session: {account_id:nil}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "GET replyableSurveys -" do
        it "return a 400" do
            get :replyableSurveys, params: {}
            expect(response).to have_http_status(:bad_request)
        end
    end

    # OLD
    # describe "GET show -" do
    #     it "return a 200" do
    #         survey = create(:survey)
    #         get :show, :params => {:id=>survey.id, :full=>true, :expendable=>true}
    #         expect(response).to have_http_status(:ok)
    #     end

    #     it "return a 400" do
    #         get :show, :params => {:id=>-1, :full=>true, :expendable=>true}
    #         expect(response).to have_http_status(:bad_request)
    #     end
    # end

    describe "GET showEditSurvey -" do
        it "return a 200" do
            survey = create(:survey)
            get :showEditSurvey, :params => {:full=>true, :expendable=>true},session: {edit_survey_id:survey.id}
            expect(response).to have_http_status(:ok)
        end
        
        it "return a 400" do
            get :showEditSurvey, :params => {:full=>true, :expendable=>true},session: {edit_survey_id:-1}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "POST create -" do
        it "return a 200" do
            post :create, :params => {:survey=>{:name=>"SURVEY NAME"}}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "PUT update -" do
        it "return a 200" do
            survey = create(:survey)
            put :update, :params => {:id=>survey.id, :survey=>{:name=>"SURVEY NAME UPDATE"}}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400" do
            put :update, :params => {:id=>-1, :survey=>{:name=>"SURVEY NAME UPDATE"}}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "DELETE destroy -" do
        it "return a 200" do
            survey = create(:survey)
            delete :destroy, :params => {:id=>survey.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 200" do
            delete :destroy, :params => {:id=>-1}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "POST startEditSurvey -" do
        it "return a 200" do
            account = create(:account)
            survey = create(:survey)
            survey.account_id=account.id
            survey.editable = true
            survey.save
            post :startEditSurvey, params: {id:survey.id}, session: {account_id:survey.account_id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400" do
            account = create(:account)
            survey = create(:survey)
            survey.account_id=account.id
            survey.editable = false
            survey.save
            post :startEditSurvey, params: {id:survey.id}, session: {account_id:survey.account_id}
            expect(response).to have_http_status(:bad_request)
        end

        it "return a 400" do
            account = create(:account)
            post :startEditSurvey, params: {id:-1}, session: {account_id:account.id}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "POST startReplySurvey -" do
        it "return a 200 with isPublic=true" do
            account = create(:account)
            survey = create(:survey)
            survey.isPublic = true
            survey.save
            post :startReplySurvey, params: {id:survey.id}, session: {account_id:account.id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400 with isPublic=false" do
            account = create(:account)
            survey = create(:survey)
            survey.isPublic = false
            survey.save
            replier = create(:replier)
            post :startReplySurvey, params: {id:survey.id, survey:{replier_id:replier.id}}, session: {account_id:account.id}
            expect(response).to have_http_status(:bad_request)
        end

        it "return a 400" do
            account = create(:account)
            post :startReplySurvey, params: {id:-1}, session: {account_id:account.id}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "POST endEditSurvey -" do
        it "return a 200" do
            post :endEditSurvey, params: {}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "POST endReplySurvey -" do
        it "return a 200" do
            replier = create(:replier)
            post :endReplySurvey, params: {}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "POST pauseReplySurvey -" do
        it "return a 200" do
            replier = create(:replier)
            post :pauseReplySurvey, params: {}, session:{replier_id:replier.id, reply_survey_id:replier.survey.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "POST setPublic -" do
        it "return a 200" do
            account = create(:account)
            survey = create(:survey)
            survey.account_id=account.id
            survey.save
            post :setPublic, params: {id: survey.id}, session: {account_id:survey.account_id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400 Unable to get Survey" do
            account = create(:account)
            post :setPublic, params: {id:-1}, session: {account_id:account.id}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "POST setSurveyPrivate -" do
        it "return a 200" do
            account = create(:account)
            survey = create(:survey)
            survey.account_id=account.id
            survey.save
            post :setSurveyPrivate, params: {id: survey.id}, session: {account_id:survey.account_id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400 Unable to get Survey" do
            account = create(:account)
            post :setSurveyPrivate, params: {id:-1}, session: {account_id:account.id}
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe "GET generateLink -" do
        it "return a 200" do
            account = create(:account)
            survey = create(:survey)
            survey.account_id=account.id
            survey.save
            get :generateLink, params: {id: survey.id}, session: {account_id:survey.account_id}
            expect(response).to have_http_status(:ok)
        end

        it "return a 400" do
            account = create(:account)
            post :generateLink, params: {id:-1}, session: {account_id:account.id}
            expect(response).to have_http_status(:bad_request)
        end

        it "return a 400 can't edit survey" do
            account = create(:account)
            survey = create(:survey)
            post :generateLink, params: {id: survey.id}, session: {account_id:account.id}
            expect(response).to have_http_status(:bad_request)
        end
    end
end