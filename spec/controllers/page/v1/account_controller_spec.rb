require "rails_helper"

RSpec.describe Page::V1::AccountController, type: :controller do
  
    describe "GET login -" do
        it "return a 200" do
            get :login
            expect(response).to have_http_status(:ok)
        end
    end

    describe "GET signup -" do
        it "return a 200" do
            get :signup
            expect(response).to have_http_status(:ok)
        end
    end

    describe "GET dashboard -" do
        it "return a 200" do
            get :dashboard
            expect(response).to have_http_status(:ok)
        end
    end
end