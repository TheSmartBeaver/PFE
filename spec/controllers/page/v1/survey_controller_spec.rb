require "rails_helper"

RSpec.describe Page::V1::SurveyController, type: :controller do
  
    describe "GET edit -" do
        it "return a 200" do
            get :edit
            expect(response).to have_http_status(:ok)
        end
    end
end