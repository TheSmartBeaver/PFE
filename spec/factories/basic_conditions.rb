FactoryBot.define do
  factory :basic_condition do
    association :target1, factory: :target
    association :target2, factory: :target

    trait :equal do
      comparisonType {"equal"}
    end

    trait :gt do
      comparisonType {"gt"}
    end

    trait :lt do
      comparisonType {"lt"}
    end

    trait :other do
      comparisonType {"other"}
    end

    trait :target1Zero do
      association :target1, factory: [:target, :constZero]
    end

    trait :target1One do
      association :target1, factory: [:target, :constOne]
    end

    trait :target1Nil do
      association :target1, factory: [:target, :constNil]
    end

    trait :target2Zero do
      association :target2, factory: [:target, :constZero]
    end

    trait :target2One do
      association :target2, factory: [:target, :constOne]
    end

    trait :target2Nil do
      association :target2, factory: [:target, :constNil]
    end
  end
end
