FactoryBot.define do
  factory :complex_condition do
    
    trait :and do
      complexType {"and"}
    end

    trait :or do
      complexType {"or"}
    end

    trait :nand do
      complexType {"nand"}
    end

    trait :nor do
      complexType {"nor"}
    end

    trait :other do
      complexType {"other"}
    end
  end
end
