FactoryBot.define do
  factory :condition do
    association :conditionable, factory: :basic_condition

    trait :basic do
      association :conditionable, factory: :basic_condition
    end

    trait :basicFirstNil do
      association :conditionable, factory: [:basic_condition, :equal, :target1Nil, :target2Zero]
    end

    trait :basicSecondNil do
      association :conditionable, factory: [:basic_condition, :equal, :target1Zero, :target2Nil]
    end

    trait :basicEqualTrue do
      association :conditionable, factory: [:basic_condition, :equal, :target1Zero, :target2Zero]
    end

    trait :basicEqualFalse do
      association :conditionable, factory: [:basic_condition, :equal, :target1Zero, :target2One]
    end

    trait :basicGtTrue do
      association :conditionable, factory: [:basic_condition, :gt, :target1One, :target2Zero]
    end

    trait :basicGtFalse do
      association :conditionable, factory: [:basic_condition, :gt, :target1Zero, :target2One]
    end

    trait :basicLtTrue do
      association :conditionable, factory: [:basic_condition, :lt, :target1Zero, :target2One]
    end

    trait :basicLtFalse do
      association :conditionable, factory: [:basic_condition, :lt, :target1One, :target2Zero]
    end
    
    trait :basicOther do
      association :conditionable, factory: [:basic_condition, :other]
    end

    trait :basicEqual do
      association :conditionable, factory: [:basic_condition, :equal]
    end

    trait :complex do
      association :conditionable, factory: :complex_condition
    end

    trait :complexAnd do
      association :conditionable, factory: [:complex_condition, :and]
    end

    trait :complexOr do
      association :conditionable, factory: [:complex_condition, :or]
    end

    trait :complexNand do
      association :conditionable, factory: [:complex_condition, :nand]
    end

    trait :complexNor do
      association :conditionable, factory: [:complex_condition, :nor]
    end

    trait :complexOther do
      association :conditionable, factory: [:complex_condition, :other]
    end
  end
end
