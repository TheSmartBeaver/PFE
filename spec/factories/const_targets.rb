FactoryBot.define do
  factory :const_target do
    const {0}

    trait :zero do
      const {0}
    end

    trait :one do
      const {1}
    end

    trait :nil do
      const {nil}
    end

    trait :text do
      const {"text"}
    end

    trait :choix2 do
      const {"Choix 2"}
    end
  end
end
