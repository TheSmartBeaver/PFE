FactoryBot.define do
  factory :element do
    association :expendableElement, factory: :text
    association :elementPlace, factory: :element_place
    
    trait :text do
      association :expendableElement, factory: :text
    end

    trait :image do
      association :expendableElement, factory: :image
    end

    trait :group do
      association :expendableElement, factory: :element_group
      expendableElement_type {"ElementGroup"}
    end

    trait :groupPage do
      association :expendableElement, factory: [:element_group, :page]
      expendableElement_type {"ElementGroup"}
    end

    trait :question do
      association :expendableElement, factory: :question
      expendableElement_type {"Question"}
    end

    trait :questionOther do
      association :expendableElement, factory: [:question, :other]
      expendableElement_type {"Question"}
    end
    
    trait :questionCheckbox do
      association :expendableElement, factory: [:question, :checkbox]
      expendableElement_type {"Question"}
    end
  end
end
