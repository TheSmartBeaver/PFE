FactoryBot.define do
  factory :question_target do
    association :question, factory: [:question, :element]
    
    trait :text do
      association :question, factory: [:question, :text]
    end
    
    trait :number do
      association :question, factory: [:question, :number]
    end
    
    trait :checkbox do
      association :question, factory: [:question, :checkbox]
    end
    
    trait :radio do
      association :question, factory: [:question, :radio]
    end
  end
end
