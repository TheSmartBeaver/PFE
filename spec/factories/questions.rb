FactoryBot.define do
  factory :question do
    questionType {'text'}
    
    trait :text do
      questionType {'text'}
    end
    
    trait :number do
      questionType {'number'}  
    end
    
    trait :checkbox do
      questionType {'checkbox'}  
    end
    
    trait :radio do
      questionType {'radio'}  
    end
    
    trait :other do
      questionType {'other'}  
    end

    trait :element do
      element
    end

  end
end
