FactoryBot.define do
  factory :response do

    association :question, factory: [:question, :text]
    association :replier, factory: :replier

    trait :text do
      association :question, factory: [:question, :text]  
    end
    
    trait :number do
      association :question, factory: [:question, :number]  
    end
    
    trait :checkbox do
      association :question, factory: [:question, :checkbox]  
    end
    
    trait :radio do
      association :question, factory: [:question, :radio]  
    end
    
    trait :other do
      association :question, factory: [:question, :other]  
    end
    
  end
end
