FactoryBot.define do
  factory :target do
    association :targetable, factory: :const_target
    
    trait :const do
      association :targetable, factory: :const_target
    end

    trait :constText do
      association :targetable, factory: [:const_target, :text]
    end

    trait :constChoix2 do
      association :targetable, factory: [:const_target, :choix2]
    end

    trait :constZero do
      association :targetable, factory: [:const_target, :zero]
    end

    trait :constOne do
      association :targetable, factory: [:const_target, :one]
    end

    trait :constNil do
      association :targetable, factory: [:const_target, :nil]
    end

    trait :question do
      association :targetable, factory: :question_target
    end

    trait :questionText do
      association :targetable, factory: [:question_target, :text]
    end

    trait :questionCheckbox do
      association :targetable, factory: [:question_target, :checkbox]
    end
  end
end
