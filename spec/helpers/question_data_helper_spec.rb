require 'rails_helper'

RSpec.describe QuestionDataHelper, type: :helper do
    before(:example) do
        @question = create(:question)
    end

    it "Create a text questionData" do
        qd = QuestionDataHelper.getQuestionData(@question.id, "text")
        expect(qd).to be_an_instance_of QuestionDataText
    end

    it "Create a number questionData" do
        qd = QuestionDataHelper.getQuestionData(@question.id, "number")
        expect(qd).to be_an_instance_of QuestionDataNumber
    end

    it "Create a checkbox questionData" do
        qd = QuestionDataHelper.getQuestionData(@question.id, "checkbox")
        expect(qd).to be_an_instance_of QuestionDataCheckbox
    end

    it "Create a radio questionData" do
        qd = QuestionDataHelper.getQuestionData(@question.id, "radio")
        expect(qd).to be_an_instance_of QuestionDataRadio
    end

    it "Raise error when type is not supported" do
        expect {QuestionDataHelper.getQuestionData(@question.id, "notSupportedType")}.to raise_error("@type 'notSupportedType' is not supported")
    end

    it "Raise error when dataKey is null" do
        expect {QuestionDataHelper.getQuestionData(nil, "text")}.to raise_error("dataKey is null")
    end
end
