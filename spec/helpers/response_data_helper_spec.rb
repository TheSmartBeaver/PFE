require 'rails_helper'

RSpec.describe ResponseDataHelper, type: :helper do
    before(:example) do
        @response = create(:response)
    end

    it "Create a text responseData" do
        rd = ResponseDataHelper.getResponseData(@response.id, "text")
        expect(rd).to be_an_instance_of ResponseDataText
    end

    it "Create a number responseData" do
        rd = ResponseDataHelper.getResponseData(@response.id, "number")
        expect(rd).to be_an_instance_of ResponseDataNumber
    end

    it "Create a checkbox responseData" do
        rd = ResponseDataHelper.getResponseData(@response.id, "checkbox")
        expect(rd).to be_an_instance_of ResponseDataCheckbox
    end

    it "Create a radio responseData" do
        rd = ResponseDataHelper.getResponseData(@response.id, "radio")
        expect(rd).to be_an_instance_of ResponseDataRadio
    end

    it "Raise error when type is not supported" do
        expect {ResponseDataHelper.getResponseData(@response.id, "notSupportedType")}.to raise_error("@type 'notSupportedType' is not supported")
    end

    it "Raise error when dataKey is null" do
        expect {ResponseDataHelper.getResponseData(nil, "text")}.to raise_error("dataKey is null")
    end
end
