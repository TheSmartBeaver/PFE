require 'rails_helper'

RSpec.describe SurveyReplierHelper, type: :helper do

    describe "can_show_element - " do
        it "basic true" do
            replier = create(:replier)
            element = create(:element, :group)
            replier.survey = element.expendableElement.survey
            replier.save
            expect(can_show_element(element, replier)).to be true
        end

        it "with group" do
            page = create(:element, :groupPage)
            page.elementPlace.update(order:0)
            page.elementPlace.save

            question = create(:element, :question)
            question.update(:element_group_id => page.expendableElement.id)
            question.save

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            expect(can_show_element(question, replier)).to be true
        end

        it "true with condition" do
            page = create(:element, :groupPage)
            page.elementPlace.update(order:0)
            page.elementPlace.save

            cond = create(:condition, :basicEqualTrue)

            question = create(:element, :question)
            question.condition = cond
            question.save

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            expect(can_show_element(question, replier)).to be true
        end

        it "false with condition" do
            page = create(:element, :groupPage)
            page.elementPlace.update(order:0)
            page.elementPlace.save

            cond = create(:condition, :basicEqualFalse)

            question = create(:element, :question)
            question.condition = cond
            question.save

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            expect(can_show_element(question, replier)).to be false
        end

        it "with group and condition" do
            page = create(:element, :groupPage)
            page.elementPlace.update(order:0)
            page.elementPlace.save

            cond = create(:condition, :basicEqualTrue)

            question = create(:element, :question)
            question.update(:element_group_id => page.expendableElement.id)
            question.condition = cond
            question.save

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            expect(can_show_element(question, replier)).to be true
        end
    end

    describe "can_go_next_page - " do
        it "true" do
            page = create(:element, :groupPage)

            question = create(:element, :questionOther)
            question.update(:element_group_id => page.expendableElement.id)
            question.save

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            rsp = create(:response, :other)
            rsp.question = question.expendableElement
            rsp.replier = replier
            rsp.save

            expect(can_go_next_page(page.expendableElement, replier)).to be true
        end

        it "ArgumentError, This is not a page" do
            page = create(:element, :group)

            question = create(:element, :questionOther)
            question.update(:element_group_id => page.expendableElement.id)
            question.save

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            expect{can_go_next_page(page.expendableElement, replier)}.to raise_error(ArgumentError, "This is not a page")
        end

        it "true with group childElement" do
            page = create(:element, :groupPage)

            group = create(:element, :group)
            group.update(:element_group_id => page.expendableElement.id)
            group.save

            question = create(:element, :questionOther)
            question.update(:element_group_id => group.expendableElement.id)
            question.save

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            rsp = create(:response, :other)
            rsp.question = question.expendableElement
            rsp.replier = replier
            rsp.save

            expect(can_go_next_page(page.expendableElement, replier)).to be true
        end

        it "false with group childElement" do
            page = create(:element, :groupPage)

            group = create(:element, :group)
            group.update(:element_group_id => page.expendableElement.id)
            group.save

            question = create(:element, :questionOther)
            question.update(:element_group_id => group.expendableElement.id)
            question.save

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            expect(can_go_next_page(page.expendableElement, replier)).to be false
        end
    end

    describe "next_page_order - " do
        it "currentPage is only page" do
            page = create(:element, :groupPage)

            group = create(:element, :group)
            group.update(:element_group_id => page.expendableElement.id)
            group.save

            question = create(:element, :questionOther)
            question.update(:element_group_id => group.expendableElement.id)
            question.save

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            expect(next_page_order(page.expendableElement.survey, page.expendableElement, replier)).to eq page.expendableElement.id
        end

        it "n+1 page" do
            page = create(:element, :groupPage)
            page.elementPlace.update(order:0)
            page.elementPlace.save

            group = create(:element, :group)
            group.update(:element_group_id => page.expendableElement.id)
            group.save

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            cond = create(:condition, :basicEqualTrue)

            page2 = create(:element, :groupPage)
            page2.elementPlace.update(order:1)
            page2.expendableElement.survey = page.expendableElement.survey
            page2.condition = cond
            page2.elementPlace.save

            page.expendableElement.survey.elementGroups.push(page2.expendableElement)

            expect(next_page_order(page.expendableElement.survey, page.expendableElement, replier)).to eq page2.expendableElement.id
        end

        it "currentPage, n+1 page not showable" do
            page = create(:element, :groupPage)
            page.elementPlace.update(order:0)
            page.elementPlace.save

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            cond = create(:condition, :basicEqualFalse)

            page2 = create(:element, :groupPage)
            page2.elementPlace.update(order:1)
            page2.expendableElement.survey = page.expendableElement.survey
            page2.condition = cond
            page2.elementPlace.save
            page2.save

            page.expendableElement.survey.elementGroups.push(page2.expendableElement)

            expect(next_page_order(page.expendableElement.survey, page.expendableElement, replier)).to eq page.expendableElement.id
        end

        it "n+2, n+1 page not showable" do
            page = create(:element, :groupPage)
            page.elementPlace.update(order:0)
            page.elementPlace.save

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            cond = create(:condition, :basicEqualFalse)
            page2 = create(:element, :groupPage)
            page2.elementPlace.update(order:1)
            page2.expendableElement.survey = page.expendableElement.survey
            page2.condition = cond
            page2.expendableElement.save
            page2.elementPlace.save
            page2.save

            cond2 = create(:condition, :basicEqualTrue)
            page3 = create(:element, :groupPage)
            page3.elementPlace.update(order:2)
            page3.expendableElement.survey = page.expendableElement.survey
            page3.condition = cond2
            page3.expendableElement.save
            page3.elementPlace.save
            page3.save

            page.expendableElement.survey.elementGroups.push(page2.expendableElement)

            expect(next_page_order(page.expendableElement.survey, page.expendableElement, replier)).to eq page3.expendableElement.id
        end


        it "ArgumentError, This is not a page" do
            page = create(:element, :group)

            question = create(:element, :questionOther)
            question.update(:element_group_id => page.expendableElement.id)
            question.save

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            expect{next_page_order(page.expendableElement.survey, page.expendableElement, replier)}.to raise_error(ArgumentError, "This is not a page")
        end
    end
end
