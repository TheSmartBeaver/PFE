require 'rails_helper'
include ResponseDataHelper

RSpec.describe SurveyStatsHelper, type: :helper do

    describe "response_question_count" do
        it "count responses" do
            page = create(:element, :groupPage)

            question = create(:element, :question)
            question.update(:element_group_id => page.expendableElement.id)
            question.save

            replier = create(:replier)
            replier.survey = page.expendableElement.survey
            replier.save

            # Ajoute des réponses à la question
            rsp1 = create(:response, :text)
            rsp1.question = question.expendableElement
            rep1 = create(:replier)
            rep1.survey = page.expendableElement.survey
            rsp1.replier = rep1
            rsp1.save
            prop1 = ResponseDataHelper.getResponseData(rsp1.id, rsp1.question.questionType)
            prop1.add_response("Response1")

            rsp2 = create(:response, :text)
            rsp2.question = question.expendableElement
            rep2 = create(:replier)
            rep2.survey = page.expendableElement.survey
            rsp2.replier = rep2
            rsp2.save
            prop2 = ResponseDataHelper.getResponseData(rsp2.id, rsp2.question.questionType)
            prop2.add_response("Response2")

            rsp3 = create(:response, :text)
            rsp3.question = question.expendableElement
            rep3 = create(:replier)
            rep3.survey = page.expendableElement.survey
            rsp3.replier = rep3
            rsp3.save
            prop3 = ResponseDataHelper.getResponseData(rsp3.id, rsp3.question.questionType)
            prop3.add_response("Response3")

            expect(response_question_count(question.expendableElement)).to eq 3
        end
    end

    describe "occurence_of_responses" do
        it "count responses text" do
            page = create(:element, :groupPage)

            question = create(:element, :question)
            question.update(:element_group_id => page.expendableElement.id)
            question.save

            survey = page.expendableElement.survey

            # Ajoute des réponses à la question
            rsp1 = create(:response, :text)
            rsp1.question = question.expendableElement
            rep1 = create(:replier)
            rep1.survey = survey
            survey.repliers.push(rep1)
            survey.save
            rsp1.replier = rep1
            rsp1.save
            prop1 = ResponseDataHelper.getResponseData(rsp1.id, rsp1.question.questionType)
            prop1.add_response("Response1")

            rsp2 = create(:response, :text)
            rsp2.question = question.expendableElement
            rep2 = create(:replier)
            rep2.survey = survey
            survey.repliers.push(rep2)
            survey.save
            rsp2.replier = rep2
            rsp2.save
            prop2 = ResponseDataHelper.getResponseData(rsp2.id, rsp2.question.questionType)
            prop2.add_response("Response2")

            rsp3 = create(:response, :text)
            rsp3.question = question.expendableElement
            rep3 = create(:replier)
            rep3.survey = survey
            survey.repliers.push(rep3)
            survey.save
            rsp3.replier = rep3
            rsp3.save
            prop3 = ResponseDataHelper.getResponseData(rsp3.id, rsp3.question.questionType)
            prop3.add_response("Response1")

            occ = Hash.new
            occ.default = 0
            occ['Response1'] = 2
            occ['Response2'] = 1

            expect(occurence_of_responses(question.expendableElement)).to eq occ
        end

        it "count responses number" do
            page = create(:element, :groupPage)

            question = create(:element, :question)
            question.update(:element_group_id => page.expendableElement.id)
            question.save

            survey = page.expendableElement.survey

            # Ajoute des réponses à la question
            rsp1 = create(:response, :number)
            rsp1.question = question.expendableElement
            rep1 = create(:replier)
            rep1.survey = survey
            survey.repliers.push(rep1)
            survey.save
            rsp1.replier = rep1
            rsp1.save
            prop1 = ResponseDataHelper.getResponseData(rsp1.id, rsp1.question.questionType)
            prop1.add_response(1)

            rsp2 = create(:response, :number)
            rsp2.question = question.expendableElement
            rep2 = create(:replier)
            rep2.survey = survey
            survey.repliers.push(rep2)
            survey.save
            rsp2.replier = rep2
            rsp2.save
            prop2 = ResponseDataHelper.getResponseData(rsp2.id, rsp2.question.questionType)
            prop2.add_response(2)

            rsp3 = create(:response, :number)
            rsp3.question = question.expendableElement
            rep3 = create(:replier)
            rep3.survey = survey
            survey.repliers.push(rep3)
            survey.save
            rsp3.replier = rep3
            rsp3.save
            prop3 = ResponseDataHelper.getResponseData(rsp3.id, rsp3.question.questionType)
            prop3.add_response(1)

            occ = Hash.new
            occ.default = 0
            occ[1] = 2
            occ[2] = 1

            expect(occurence_of_responses(question.expendableElement)).to eq occ
        end
    end

    describe "replier_survey_count" do
        it "count responses" do
            page = create(:element, :groupPage)

            question = create(:element, :question)
            question.update(:element_group_id => page.expendableElement.id)
            question.save

            survey = page.expendableElement.survey

            # Ajoute des réponses à la question
            rsp1 = create(:response, :text)
            rsp1.question = question.expendableElement
            rep1 = create(:replier)
            rep1.survey = survey
            survey.repliers.push(rep1)
            survey.save
            rsp1.replier = rep1
            rsp1.save
            prop1 = ResponseDataHelper.getResponseData(rsp1.id, rsp1.question.questionType)
            prop1.add_response("Response1")

            rsp2 = create(:response, :text)
            rsp2.question = question.expendableElement
            rep2 = create(:replier)
            rep2.survey = survey
            survey.repliers.push(rep2)
            survey.save
            rsp2.replier = rep2
            rsp2.save
            prop2 = ResponseDataHelper.getResponseData(rsp2.id, rsp2.question.questionType)
            prop2.add_response("Response2")

            rsp3 = create(:response, :text)
            rsp3.question = question.expendableElement
            rep3 = create(:replier)
            rep3.survey = survey
            survey.repliers.push(rep3)
            survey.save
            rsp3.replier = rep3
            rsp3.save
            prop3 = ResponseDataHelper.getResponseData(rsp3.id, rsp3.question.questionType)
            prop3.add_response("Response3")

            expect(replier_survey_count(survey)).to eq 3
        end
    end

    describe "replier_ended_survey_count" do
        it "count responses" do
            page = create(:element, :groupPage)

            question = create(:element, :question)
            question.update(:element_group_id => page.expendableElement.id)
            question.save

            survey = page.expendableElement.survey

            # Ajoute des réponses à la question
            rsp1 = create(:response, :text)
            rsp1.question = question.expendableElement
            rep1 = create(:replier)
            rep1.survey = survey
            rep1.update(ended:true)
            rep1.save
            survey.repliers.push(rep1)
            survey.save
            rsp1.replier = rep1
            rsp1.save
            prop1 = ResponseDataHelper.getResponseData(rsp1.id, rsp1.question.questionType)
            prop1.add_response("Response1")

            rsp2 = create(:response, :text)
            rsp2.question = question.expendableElement
            rep2 = create(:replier)
            rep2.survey = survey
            survey.repliers.push(rep2)
            survey.save
            rsp2.replier = rep2
            rsp2.save
            prop2 = ResponseDataHelper.getResponseData(rsp2.id, rsp2.question.questionType)
            prop2.add_response("Response2")

            rsp3 = create(:response, :text)
            rsp3.question = question.expendableElement
            rep3 = create(:replier)
            rep3.survey = survey
            rep3.update(ended:true)
            rep3.save
            survey.repliers.push(rep3)
            survey.save
            rsp3.replier = rep3
            rsp3.save
            prop3 = ResponseDataHelper.getResponseData(rsp3.id, rsp3.question.questionType)
            prop3.add_response("Response3")

            expect(replier_ended_survey_count(survey)).to eq 2
        end
    end
end
