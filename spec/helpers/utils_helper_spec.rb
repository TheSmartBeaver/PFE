require 'rails_helper'

RSpec.describe UtilsHelper, type: :helper do
    describe "to_bool" do
        it "true" do
            expect(to_bool("true")).to be true
        end

        it "TRUE" do
            expect(to_bool("TRUE")).to be true
        end

        it "false" do
            expect(to_bool("false")).to be false
        end

        it "FALSE" do
            expect(to_bool("FALSE")).to be false
        end

        it "other" do
            expect(to_bool("other")).to be false
        end
    end
end
