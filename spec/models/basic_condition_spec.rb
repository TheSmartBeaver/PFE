require 'rails_helper'

RSpec.describe BasicCondition, type: :model do

    describe "null data -" do
        it "data1 in null" do
            cond = create(:condition, :basicFirstNil)
            replier = create(:replier)
            expect(cond.conditionable.testCondition(replier.id)).to be false
        end

        it "data2 is null" do
            cond = create(:condition, :basicSecondNil)
            replier = create(:replier)
            expect(cond.conditionable.testCondition(replier.id)).to be false
        end
    end

    describe "equal -" do
        it "is true" do
            cond = create(:condition, :basicEqualTrue)
            replier = create(:replier)
            expect(cond.conditionable.testCondition(replier.id)).to be true
        end

        it "is false" do
            cond = create(:condition, :basicEqualFalse)
            replier = create(:replier)
            expect(cond.conditionable.testCondition(replier.id)).to be false
        end

        it "is true with text questionData" do
            cond = create(:condition, :basicEqual)
            rsp = create(:response, :text)
            rsp.getResponseData.add_response("text")
            target = create(:target, :questionText)
            rsp.question = target.targetable.question
            target.targetable.question.responses.push(rsp)
            cond.conditionable.target1 = create(:target, :constText)
            cond.conditionable.target2 = target
            expect(cond.conditionable.testCondition(rsp.replier)).to be true
        end

        it "is true with const and checkbox target" do
            cond = create(:condition, :basicEqual)
            rsp = create(:response, :checkbox)
            rsp.getResponseData.add_response(1)
            target = create(:target, :questionCheckbox)
            rsp.question = target.targetable.question
            target.targetable.question.responses.push(rsp)
            target.targetable.question.getQuestionData.addChoice("Choix 1")
            target.targetable.question.getQuestionData.addChoice("Choix 2")
            target.targetable.question.getQuestionData.addChoice("Choix 3")
            cond.conditionable.target1 = create(:target, :constChoix2)
            cond.conditionable.target2 = target
            expect(cond.conditionable.testCondition(rsp.replier)).to be true
        end

        it "is true with checkbox target and const" do
            cond = create(:condition, :basicEqual)
            rsp = create(:response, :checkbox)
            rsp.getResponseData.add_response(1)
            target = create(:target, :questionCheckbox)
            rsp.question = target.targetable.question
            target.targetable.question.responses.push(rsp)
            target.targetable.question.getQuestionData.addChoice("Choix 1")
            target.targetable.question.getQuestionData.addChoice("Choix 2")
            target.targetable.question.getQuestionData.addChoice("Choix 3")
            cond.conditionable.target2 = create(:target, :constChoix2)
            cond.conditionable.target1 = target
            expect(cond.conditionable.testCondition(rsp.replier)).to be true
        end

        it "is true with two checkbox target" do
            cond = create(:condition, :basicEqual)

            rsp1 = create(:response, :checkbox)
            rsp1.getResponseData.add_response(1)
            target1 = create(:target, :questionCheckbox)
            rsp1.question = target1.targetable.question
            target1.targetable.question.responses.push(rsp1)
            target1.targetable.question.getQuestionData.addChoice("Choix 1")
            target1.targetable.question.getQuestionData.addChoice("Choix 2")
            target1.targetable.question.getQuestionData.addChoice("Choix 3")

            rsp2 = create(:response, :checkbox)
            rsp2.replier = rsp1.replier
            rsp2.getResponseData.add_response(1)
            target2 = create(:target, :questionCheckbox)
            rsp2.question = target2.targetable.question
            target2.targetable.question.responses.push(rsp2)
            target2.targetable.question.getQuestionData.addChoice("Choix 1")
            target2.targetable.question.getQuestionData.addChoice("Choix 2")
            target2.targetable.question.getQuestionData.addChoice("Choix 3")

            cond.conditionable.target1 = target1
            cond.conditionable.target2 = target2
            expect(cond.conditionable.testCondition(rsp1.replier)).to be true
        end

        it "is false with two checkbox target different size" do
            cond = create(:condition, :basicEqual)

            rsp1 = create(:response, :checkbox)
            rsp1.getResponseData.add_response(1)
            rsp1.getResponseData.add_response(0)
            target1 = create(:target, :questionCheckbox)
            rsp1.question = target1.targetable.question
            target1.targetable.question.responses.push(rsp1)
            target1.targetable.question.getQuestionData.addChoice("Choix 1")
            target1.targetable.question.getQuestionData.addChoice("Choix 2")
            target1.targetable.question.getQuestionData.addChoice("Choix 3")

            rsp2 = create(:response, :checkbox)
            rsp2.replier = rsp1.replier
            rsp2.getResponseData.add_response(1)
            target2 = create(:target, :questionCheckbox)
            rsp2.question = target2.targetable.question
            target2.targetable.question.responses.push(rsp2)
            target2.targetable.question.getQuestionData.addChoice("Choix 1")
            target2.targetable.question.getQuestionData.addChoice("Choix 2")
            target2.targetable.question.getQuestionData.addChoice("Choix 3")

            cond.conditionable.target1 = target1
            cond.conditionable.target2 = target2
            expect(cond.conditionable.testCondition(rsp1.replier)).to be false
        end

        it "is false with two different checkbox target" do
            cond = create(:condition, :basicEqual)

            rsp1 = create(:response, :checkbox)
            rsp1.getResponseData.add_response(0)
            target1 = create(:target, :questionCheckbox)
            rsp1.question = target1.targetable.question
            target1.targetable.question.responses.push(rsp1)
            target1.targetable.question.getQuestionData.addChoice("Choix 1")
            target1.targetable.question.getQuestionData.addChoice("Choix 2")
            target1.targetable.question.getQuestionData.addChoice("Choix 3")

            rsp2 = create(:response, :checkbox)
            rsp2.replier = rsp1.replier
            rsp2.getResponseData.add_response(1)
            target2 = create(:target, :questionCheckbox)
            rsp2.question = target2.targetable.question
            target2.targetable.question.responses.push(rsp2)
            target2.targetable.question.getQuestionData.addChoice("Choix 1")
            target2.targetable.question.getQuestionData.addChoice("Choix 2")
            target2.targetable.question.getQuestionData.addChoice("Choix 3")

            cond.conditionable.target1 = target1
            cond.conditionable.target2 = target2
            expect(cond.conditionable.testCondition(rsp1.replier)).to be false
        end
    end

    describe "gt -" do
        it "is true" do
            cond = create(:condition, :basicGtTrue)
            replier = create(:replier)
            expect(cond.conditionable.testCondition(replier.id)).to be true
        end

        it "is false" do
            cond = create(:condition, :basicGtFalse)
            replier = create(:replier)
            expect(cond.conditionable.testCondition(replier.id)).to be false
        end
    end

    describe "lt -" do
        it "is true" do
            cond = create(:condition, :basicLtTrue)
            replier = create(:replier)
            expect(cond.conditionable.testCondition(replier.id)).to be true
        end

        it "is false" do
            cond = create(:condition, :basicLtFalse)
            replier = create(:replier)
            expect(cond.conditionable.testCondition(replier.id)).to be false
        end
    end

    describe "other -" do
        it "is false" do
            cond = create(:condition, :basicOther)
            replier = create(:replier)
            expect(cond.conditionable.testCondition(replier.id)).to be false
        end
    end
end
