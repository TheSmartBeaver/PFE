require 'rails_helper'

RSpec.describe ComplexCondition, type: :model do
    describe "and -" do
        it "is true" do
            cond = create(:condition, :complexAnd)
            cond.conditionable.conditions = [create(:condition, :basicEqualTrue), create(:condition, :basicEqualTrue)]
            replier = create(:replier)
            expect(cond.conditionable.testCondition(replier.id)).to be true
        end

        it "is false" do
            cond = create(:condition, :complexAnd)
            cond.conditionable.conditions = [create(:condition, :basicEqualTrue), create(:condition, :basicEqualFalse)]
            replier = create(:replier)
            expect(cond.conditionable.testCondition(replier.id)).to be false
        end
    end

    describe "or -" do
        it "is true with two true" do
            cond = create(:condition, :complexOr)
            cond.conditionable.conditions = [create(:condition, :basicEqualTrue), create(:condition, :basicEqualTrue)]
            replier = create(:replier)
            expect(cond.conditionable.testCondition(replier.id)).to be true
        end

        it "is true with one true and one false" do
            cond = create(:condition, :complexOr)
            cond.conditionable.conditions = [create(:condition, :basicEqualTrue), create(:condition, :basicEqualFalse)]
            replier = create(:replier)
            expect(cond.conditionable.testCondition(replier.id)).to be true
        end

        it "is false with two false" do
            cond = create(:condition, :complexOr)
            cond.conditionable.conditions = [create(:condition, :basicEqualFalse), create(:condition, :basicEqualFalse)]
            replier = create(:replier)
            expect(cond.conditionable.testCondition(replier.id)).to be false
        end
    end

    describe "nand -" do
        it "is false with two true" do
            cond = create(:condition, :complexNand)
            cond.conditionable.conditions = [create(:condition, :basicEqualTrue), create(:condition, :basicEqualTrue)]
            replier = create(:replier)
            expect(cond.conditionable.testCondition(replier.id)).to be false
        end

        it "is false with one true and one false" do
            cond = create(:condition, :complexNand)
            cond.conditionable.conditions = [create(:condition, :basicEqualTrue), create(:condition, :basicEqualFalse)]
            replier = create(:replier)
            expect(cond.conditionable.testCondition(replier.id)).to be false
        end

        it "is true with two false" do
            cond = create(:condition, :complexNand)
            cond.conditionable.conditions = [create(:condition, :basicEqualFalse), create(:condition, :basicEqualFalse)]
            replier = create(:replier)
            expect(cond.conditionable.testCondition(replier.id)).to be true
        end
    end

    describe "nor -" do
        it "is false with two true" do
            cond = create(:condition, :complexNor)
            cond.conditionable.conditions = [create(:condition, :basicEqualTrue), create(:condition, :basicEqualTrue)]
            replier = create(:replier)
            expect(cond.conditionable.testCondition(replier.id)).to be false
        end

        it "is true with one true and one false" do
            cond = create(:condition, :complexNor)
            cond.conditionable.conditions = [create(:condition, :basicEqualTrue), create(:condition, :basicEqualFalse)]
            replier = create(:replier)
            expect(cond.conditionable.testCondition(replier.id)).to be true
        end

        it "is true with two false" do
            cond = create(:condition, :complexNor)
            cond.conditionable.conditions = [create(:condition, :basicEqualFalse), create(:condition, :basicEqualFalse)]
            replier = create(:replier)
            expect(cond.conditionable.testCondition(replier.id)).to be true
        end
    end

    describe "other -" do
        it "is false" do
            cond = create(:condition, :complexOther)
            replier = create(:replier)
            expect(cond.conditionable.testCondition(replier.id)).to be false
        end
    end
end
