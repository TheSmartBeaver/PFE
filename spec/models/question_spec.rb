require 'rails_helper'
include QuestionDataHelper

RSpec.describe Question, type: :model do

  it "Create a Question" do
    q = create(:question)
    expect(q).to be_an_instance_of Question
  end

  context "TEXT DATA : " do
    before(:example) do
      @qtext = create(:question, :text)
    end

    it "Create a text question" do
      expect(@qtext.questionType).to eq "text"
    end

    it "Check json of a text questionData with no data" do
      qd = QuestionDataHelper.getQuestionData(@qtext.id, @qtext.questionType)
      expect(qd.to_json).to eq "{\"title\":null,\"description\":null,\"parameters\":{}}" 
    end

    it "Check json of a text questionData with title" do
      qd = QuestionDataHelper.getQuestionData(@qtext.id, @qtext.questionType)
      qd.title = "Titre de la question"
      expect(qd.to_json).to eq "{\"title\":\"Titre de la question\",\"description\":null,\"parameters\":{}}" 
    end

    it "Check json of a text questionData with description" do
      qd = QuestionDataHelper.getQuestionData(@qtext.id, @qtext.questionType)
      qd.description = "Description de la question."
      expect(qd.to_json).to eq "{\"title\":null,\"description\":\"Description de la question.\",\"parameters\":{}}" 
    end

    it "Check json of a text questionData with title and description" do
      qd = QuestionDataHelper.getQuestionData(@qtext.id, @qtext.questionType)
      qd.title = "Titre de la question"
      qd.description = "Description de la question."
      expect(qd.to_json).to eq "{\"title\":\"Titre de la question\",\"description\":\"Description de la question.\",\"parameters\":{}}" 
    end

    it "Save a text questionData" do
      qd = QuestionDataHelper.getQuestionData(@qtext.id, @qtext.questionType)
      qd.title = "Titre de la question"
      qd.description = "Description de la question."
      expect {qd.save_to_base}.not_to raise_error
    end

    it "Load a text questionData from base" do
      qd = QuestionDataHelper.getQuestionData(@qtext.id, @qtext.questionType)
      qd.title = "Titre de la question"
      qd.description = "Description de la question."
      qd.save_to_base

      qdLoad = QuestionDataHelper.getQuestionData(@qtext.id, @qtext.questionType)
      expect(qdLoad.dataKey).to eq qd.dataKey
      expect(qdLoad.title).to eq qd.title
      expect(qdLoad.description).to eq qd.description
    end

    it "Can't add a choice for a text questionData" do
      qd = QuestionDataHelper.getQuestionData(@qtext.id, @qtext.questionType)
      expect {qd.addChoice("Choix 1")}.to raise_error("This method is not implemented for QuestionDataText")
    end

    it "Can't remove a choice for a text questionData" do
      qd = QuestionDataHelper.getQuestionData(@qtext.id, @qtext.questionType)
      expect {qd.removeChoice(0)}.to raise_error("This method is not implemented for QuestionDataText")
    end

    it "Can't set maxCheck for a text questionData" do
      qd = QuestionDataHelper.getQuestionData(@qtext.id, @qtext.questionType)
      expect {qd.setMaxCheck(0)}.to raise_error("This method is not implemented for QuestionDataText")
    end

    it "Can't set minCheck for a text questionData" do
      qd = QuestionDataHelper.getQuestionData(@qtext.id, @qtext.questionType)
      expect {qd.setMinCheck(0)}.to raise_error("This method is not implemented for QuestionDataText")
    end
  end

  context "NUMBER DATA : " do
    before(:example) do
      @qnum = create(:question, :number)
    end

    it "Create a number question" do
      expect(@qnum.questionType).to eq "number"
    end

    it "Check json of a number questionData with no data" do
      qd = QuestionDataHelper.getQuestionData(@qnum.id, @qnum.questionType)
      expect(qd.to_json).to eq "{\"title\":null,\"description\":null,\"parameters\":{}}" 
    end

    it "Check json of a number questionData with title" do
      qd = QuestionDataHelper.getQuestionData(@qnum.id, @qnum.questionType)
      qd.title = "Titre de la question"
      expect(qd.to_json).to eq "{\"title\":\"Titre de la question\",\"description\":null,\"parameters\":{}}" 
    end

    it "Check json of a number questionData with description" do
      qd = QuestionDataHelper.getQuestionData(@qnum.id, @qnum.questionType)
      qd.description = "Description de la question."
      expect(qd.to_json).to eq "{\"title\":null,\"description\":\"Description de la question.\",\"parameters\":{}}" 
    end

    it "Check json of a number questionData with title and description" do
      qd = QuestionDataHelper.getQuestionData(@qnum.id, @qnum.questionType)
      qd.title = "Titre de la question"
      qd.description = "Description de la question."
      expect(qd.to_json).to eq "{\"title\":\"Titre de la question\",\"description\":\"Description de la question.\",\"parameters\":{}}" 
    end

    it "Save a number questionData" do
      qd = QuestionDataHelper.getQuestionData(@qnum.id, @qnum.questionType)
      qd.title = "Titre de la question"
      qd.description = "Description de la question."
      expect {qd.save_to_base}.not_to raise_error
    end

    it "Load a number questionData from base" do
      qd = QuestionDataHelper.getQuestionData(@qnum.id, @qnum.questionType)
      qd.title = "Titre de la question"
      qd.description = "Description de la question."
      qd.save_to_base

      qdLoad = QuestionDataHelper.getQuestionData(@qnum.id, @qnum.questionType)
      expect(qdLoad.dataKey).to eq qd.dataKey
      expect(qdLoad.title).to eq qd.title
      expect(qdLoad.description).to eq qd.description
    end

    it "Can't add a choice for a number questionData" do
      qd = QuestionDataHelper.getQuestionData(@qnum.id, @qnum.questionType)
      expect {qd.addChoice("Choix 1")}.to raise_error("This method is not implemented for QuestionDataNumber")
    end

    it "Can't remove a choice for a number questionData" do
      qd = QuestionDataHelper.getQuestionData(@qnum.id, @qnum.questionType)
      expect {qd.removeChoice(0)}.to raise_error("This method is not implemented for QuestionDataNumber")
    end

    it "Can't set maxCheck for a number questionData" do
      qd = QuestionDataHelper.getQuestionData(@qnum.id, @qnum.questionType)
      expect {qd.setMaxCheck(0)}.to raise_error("This method is not implemented for QuestionDataNumber")
    end

    it "Can't set minCheck for a number questionData" do
      qd = QuestionDataHelper.getQuestionData(@qnum.id, @qnum.questionType)
      expect {qd.setMinCheck(0)}.to raise_error("This method is not implemented for QuestionDataNumber")
    end
  end

  context "CHECKBOX DATA : " do
    before(:example) do
      @qcb = create(:question, :checkbox)
    end

    it "Create a checkbox question" do
      expect(@qcb.questionType).to eq "checkbox"
    end

    it "Create a checkbox questionData with choices" do
      qd = QuestionDataHelper.getQuestionData(@qcb.id, @qcb.questionType)
      qd.addChoice("Choix 1")
      qd.addChoice("Choix 2")
      qd.addChoice("Choix 3")
      expect(qd.choices).to match_array(["Choix 1", "Choix 2", "Choix 3"])
    end

    it "Remove a choice from a checkbox questionData" do
      qd = QuestionDataHelper.getQuestionData(@qcb.id, @qcb.questionType)
      qd.addChoice("Choix 1")
      qd.addChoice("Choix 2")
      qd.addChoice("Choix 3")
      qd.removeChoice(1)
      expect(qd.choices).to match_array(["Choix 1", "Choix 3"])
    end

    it "Create a checkbox questionData and set maxCheck" do
      qd = QuestionDataHelper.getQuestionData(@qcb.id, @qcb.questionType)
      expect(qd.maxCheck).to eq 1
      qd.addChoice("Choix 1")
      qd.addChoice("Choix 2")
      qd.addChoice("Choix 3")
      qd.setMaxCheck(2)
      expect(qd.maxCheck).to eq 2
    end

    it "Create a checkbox questionData and set minCheck" do
      qd = QuestionDataHelper.getQuestionData(@qcb.id, @qcb.questionType)
      expect(qd.minCheck).to eq 0
      qd.addChoice("Choix 1")
      qd.addChoice("Choix 2")
      qd.addChoice("Choix 3")
      qd.setMaxCheck(2)
      qd.setMinCheck(1)
      expect(qd.minCheck).to eq 1
    end

    it "Can't set an outbound value of minCheck" do
      qd = QuestionDataHelper.getQuestionData(@qcb.id, @qcb.questionType)
      expect {qd.setMinCheck(-1)}.to raise_error("minCheck must be >= 0")
      expect {qd.setMinCheck(2)}.to raise_error("minCheck must be <= maxCheck")
    end

    it "Can't set an outbound value of maxCheck" do
      qd = QuestionDataHelper.getQuestionData(@qcb.id, @qcb.questionType)
      expect {qd.setMaxCheck(-1)}.to raise_error("maxCheck must be >= minCheck")
      expect {qd.setMaxCheck(2)}.to raise_error("maxCheck must be inferior or equal to choice number")
    end

    it "Check json of a checkbox questionData with no data" do
      qd = QuestionDataHelper.getQuestionData(@qcb.id, @qcb.questionType)
      expect(qd.to_json).to eq "{\"title\":null,\"description\":null,\"parameters\":{\"minCheck\":0,\"maxCheck\":1,\"choices\":[]}}" 
    end

    it "Check json of a checkbox questionData with title" do
      qd = QuestionDataHelper.getQuestionData(@qcb.id, @qcb.questionType)
      qd.title = "Titre de la question"
      expect(qd.to_json).to eq "{\"title\":\"Titre de la question\",\"description\":null,\"parameters\":{\"minCheck\":0,\"maxCheck\":1,\"choices\":[]}}" 
    end

    it "Check json of a checkbox questionData with description" do
      qd = QuestionDataHelper.getQuestionData(@qcb.id, @qcb.questionType)
      qd.description = "Description de la question."
      expect(qd.to_json).to eq "{\"title\":null,\"description\":\"Description de la question.\",\"parameters\":{\"minCheck\":0,\"maxCheck\":1,\"choices\":[]}}" 
    end

    it "Check json of a checkbox questionData with choices" do
      qd = QuestionDataHelper.getQuestionData(@qcb.id, @qcb.questionType)
      qd.addChoice("Choix 1")
      qd.addChoice("Choix 2")
      qd.addChoice("Choix 3")
      expect(qd.to_json).to eq "{\"title\":null,\"description\":null,\"parameters\":{\"minCheck\":0,\"maxCheck\":1,\"choices\":[\"Choix 1\",\"Choix 2\",\"Choix 3\"]}}" 
    end

    it "Check json of a checkbox questionData with title, description and choices" do
      qd = QuestionDataHelper.getQuestionData(@qcb.id, @qcb.questionType)
      qd.title = "Titre de la question"
      qd.description = "Description de la question."
      qd.addChoice("Choix 1")
      qd.addChoice("Choix 2")
      qd.addChoice("Choix 3")
      expect(qd.to_json).to eq "{\"title\":\"Titre de la question\",\"description\":\"Description de la question.\",\"parameters\":{\"minCheck\":0,\"maxCheck\":1,\"choices\":[\"Choix 1\",\"Choix 2\",\"Choix 3\"]}}" 
    end

    it "Save a checkbox questionData" do
      qd = QuestionDataHelper.getQuestionData(@qcb.id, @qcb.questionType)
      qd.title = "Titre de la question"
      qd.description = "Description de la question."
      expect {qd.save_to_base}.not_to raise_error
    end

    it "Load a checkbox questionData from base" do
      qd = QuestionDataHelper.getQuestionData(@qcb.id, @qcb.questionType)
      qd.title = "Titre de la question"
      qd.description = "Description de la question."
      qd.addChoice("Choix 1")
      qd.addChoice("Choix 2")
      qd.addChoice("Choix 3")
      qd.addChoice("Choix 4")
      qd.setMaxCheck(2)
      qd.setMinCheck(1)
      qd.save_to_base

      qdLoad = QuestionDataHelper.getQuestionData(@qcb.id, @qcb.questionType)
      expect(qdLoad.dataKey).to eq qd.dataKey
      expect(qdLoad.title).to eq qd.title
      expect(qdLoad.description).to eq qd.description
      expect(qdLoad.minCheck).to eq qd.minCheck
      expect(qdLoad.maxCheck).to eq qd.maxCheck
      expect(qdLoad.choices).to match_array(qd.choices)
    end
  end

  context "RADIO DATA : " do
    before(:example) do
      @qradio = create(:question, :radio)
    end

    it "Create a radio question" do
      expect(@qradio.questionType).to eq "radio"
    end

    it "Create a radio questionData with choices" do
      qd = QuestionDataHelper.getQuestionData(@qradio.id, @qradio.questionType)
      qd.addChoice("Choix 1")
      qd.addChoice("Choix 2")
      qd.addChoice("Choix 3")
      expect(qd.choices).to match_array(["Choix 1", "Choix 2", "Choix 3"])
    end

    it "Create a radio questionData with maxCheck" do
      qd = QuestionDataHelper.getQuestionData(@qradio.id, @qradio.questionType)
      expect(qd.maxCheck).to eq 1
    end

    it "Can't edit a radio questionData maxCheck" do
      qd = QuestionDataHelper.getQuestionData(@qradio.id, @qradio.questionType)
      expect {qd.setMaxCheck(2)}.to raise_error("It is impossible to edit maxCheck of a radio question.")
    end

    it "Can't set a radio questionData minCheck different of 0 or 1" do
      qd = QuestionDataHelper.getQuestionData(@qradio.id, @qradio.questionType)
      expect {qd.setMinCheck(-1)}.to raise_error("It is impossible to set a minCheck different of 0 or 1 for a radio question.")
      expect {qd.setMinCheck(2)}.to raise_error("It is impossible to set a minCheck different of 0 or 1 for a radio question.")
    end

    it "Check json of a radio questionData with no data" do
      qd = QuestionDataHelper.getQuestionData(@qradio.id, @qradio.questionType)
      expect(qd.to_json).to eq "{\"title\":null,\"description\":null,\"parameters\":{\"minCheck\":0,\"maxCheck\":1,\"choices\":[]}}" 
    end

    it "Check json of a radio questionData with title" do
      qd = QuestionDataHelper.getQuestionData(@qradio.id, @qradio.questionType)
      qd.title = "Titre de la question"
      expect(qd.to_json).to eq "{\"title\":\"Titre de la question\",\"description\":null,\"parameters\":{\"minCheck\":0,\"maxCheck\":1,\"choices\":[]}}" 
    end

    it "Check json of a radio questionData with description" do
      qd = QuestionDataHelper.getQuestionData(@qradio.id, @qradio.questionType)
      qd.description = "Description de la question."
      expect(qd.to_json).to eq "{\"title\":null,\"description\":\"Description de la question.\",\"parameters\":{\"minCheck\":0,\"maxCheck\":1,\"choices\":[]}}" 
    end

    it "Check json of a radio questionData with choices" do
      qd = QuestionDataHelper.getQuestionData(@qradio.id, @qradio.questionType)
      qd.addChoice("Choix 1")
      qd.addChoice("Choix 2")
      qd.addChoice("Choix 3")
      expect(qd.to_json).to eq "{\"title\":null,\"description\":null,\"parameters\":{\"minCheck\":0,\"maxCheck\":1,\"choices\":[\"Choix 1\",\"Choix 2\",\"Choix 3\"]}}" 
    end

    it "Check json of a radio questionData with title, description and choices" do
      qd = QuestionDataHelper.getQuestionData(@qradio.id, @qradio.questionType)
      qd.title = "Titre de la question"
      qd.description = "Description de la question."
      qd.addChoice("Choix 1")
      qd.addChoice("Choix 2")
      qd.addChoice("Choix 3")
      qd.setMinCheck(1)
      expect(qd.to_json).to eq "{\"title\":\"Titre de la question\",\"description\":\"Description de la question.\",\"parameters\":{\"minCheck\":1,\"maxCheck\":1,\"choices\":[\"Choix 1\",\"Choix 2\",\"Choix 3\"]}}" 
    end

    it "Save a radio questionData" do
      qd = QuestionDataHelper.getQuestionData(@qradio.id, @qradio.questionType)
      qd.title = "Titre de la question"
      qd.description = "Description de la question."
      expect {qd.save_to_base}.not_to raise_error
    end

    it "Load a radio questionData from base" do
      qd = QuestionDataHelper.getQuestionData(@qradio.id, @qradio.questionType)
      qd.title = "Titre de la question"
      qd.description = "Description de la question."
      qd.addChoice("Choix 1")
      qd.addChoice("Choix 2")
      qd.addChoice("Choix 3")
      qd.addChoice("Choix 4")
      qd.setMinCheck(1)
      qd.save_to_base

      qdLoad = QuestionDataHelper.getQuestionData(@qradio.id, @qradio.questionType)
      expect(qdLoad.dataKey).to eq qd.dataKey
      expect(qdLoad.title).to eq qd.title
      expect(qdLoad.description).to eq qd.description
      expect(qdLoad.minCheck).to eq 1
      expect(qdLoad.maxCheck).to eq 1
      expect(qdLoad.choices).to match_array(qd.choices)
    end
  end

end