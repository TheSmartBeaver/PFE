require 'rails_helper'
include ResponseDataHelper

RSpec.describe Response, type: :model do
  it "Create a Response" do
    r = create(:response)
    expect(r).to be_an_instance_of Response
  end

  context "TEXT DATA : " do
    before(:example) do
      @response = create(:response, :text)
      @responseData = ResponseDataHelper.getResponseData(@response.id, @response.question.questionType)
    end

    it "Create a Response with a text Question" do
      expect(@response.question.questionType).to eq "text"
    end

    it "Check json of a text responseData with no data" do
      expect(@responseData.to_json).to eq "{\"response\":null}" 
    end

    it "Check json of a text responseData with data" do
      @responseData.response = "Réponse à la question"
      expect(@responseData.to_json).to eq "{\"response\":\"Réponse à la question\"}" 
    end

    it "Save a text responseData" do
      @responseData.response = "Réponse à la question"
      expect {@responseData.save_to_base}.not_to raise_error
    end

    it "Load a text responseData from base" do
      @responseData.response = "Réponse à la question"
      @responseData.save_to_base

      responseDataLoad = ResponseDataHelper.getResponseData(@response.id, @response.question.questionType)
      expect(responseDataLoad.dataKey).to eq @responseData.dataKey
      expect(responseDataLoad.response).to eq @responseData.response
    end
  end

  context "NUMBER DATA : " do
    before(:example) do
      @response = create(:response, :number)
      @responseData = ResponseDataHelper.getResponseData(@response.id, @response.question.questionType)
    end

    it "Create a Response with a number Question" do
      expect(@response.question.questionType).to eq "number"
    end

    it "Check json of a number responseData with no data" do
      expect(@responseData.to_json).to eq "{\"response\":null}" 
    end

    it "Check json of a number responseData with data" do
      @responseData.response = 42
      expect(@responseData.to_json).to eq "{\"response\":42}" 
    end

    it "Save a number responseData" do
      @responseData.response = 42
      expect {@responseData.save_to_base}.not_to raise_error
    end

    it "Load a number responseData from base" do
      @responseData.response = 42
      @responseData.save_to_base

      responseDataLoad = ResponseDataHelper.getResponseData(@response.id, @response.question.questionType)
      expect(responseDataLoad.dataKey).to eq @responseData.dataKey
      expect(responseDataLoad.response).to eq @responseData.response
    end
  end

  context "CHECKBOX DATA : " do
    before(:example) do
      @response = create(:response, :checkbox)
      @responseData = ResponseDataHelper.getResponseData(@response.id, @response.question.questionType)
    end

    it "Create a Response with a checkbox Question" do
      expect(@response.question.questionType).to eq "checkbox"
    end

    it "Check json of a checkbox responseData with no data" do
      expect(@responseData.to_json).to eq "{\"responses\":[]}" 
    end

    it "Check json of a checkbox responseData with data" do
      @responseData.add_response(3)
      @responseData.add_response(5)
      expect(@responseData.to_json).to eq "{\"responses\":[3,5]}" 
    end

    it "Save a checkbox responseData" do
      @responseData.add_response(3)
      @responseData.add_response(5)
      expect {@responseData.save_to_base}.not_to raise_error
    end

    it "Load a checkbox responseData from base" do
      @responseData.add_response(3)
      @responseData.add_response(5)
      @responseData.save_to_base

      responseDataLoad = ResponseDataHelper.getResponseData(@response.id, @response.question.questionType)
      expect(responseDataLoad.dataKey).to eq @responseData.dataKey
      expect(responseDataLoad.responses).to eq @responseData.responses
    end
  end

  context "RADIO DATA : " do
    before(:example) do
      @response = create(:response, :radio)
      @responseData = ResponseDataHelper.getResponseData(@response.id, @response.question.questionType)
    end

    it "Create a Response with a radio Question" do
      expect(@response.question.questionType).to eq "radio"
    end

    it "Check json of a radio responseData with no data" do
      expect(@responseData.to_json).to eq "{\"response\":null}" 
    end

    it "Check json of a radio responseData with data" do
      @responseData.response = 2
      expect(@responseData.to_json).to eq "{\"response\":2}" 
    end

    it "Save a radio responseData" do
      @responseData.response = 2
      expect {@responseData.save_to_base}.not_to raise_error
    end

    it "Load a radio responseData from base" do
      @responseData.response = 2
      @responseData.save_to_base

      responseDataLoad = ResponseDataHelper.getResponseData(@response.id, @response.question.questionType)
      expect(responseDataLoad.dataKey).to eq @responseData.dataKey
      expect(responseDataLoad.response).to eq @responseData.response
    end
  end

end
