module ControllerMacros
    def login_admin
        before(:each) do
            @request.env["devise.mapping"] = Devise.mappings[:admin]
            sign_in FactoryBot.create(:admin)
        end
    end

    def login_account
        before(:each) do
            @request.env["devise.mapping"] = Devise.mappings[:account]
            account = FactoryBot.create(:account)
            sign_in account
        end
    end
end